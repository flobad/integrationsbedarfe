﻿using MethodenunterstuetzungV2.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MethodenunterstuetzungV2.Views
{
    /// <summary>
    /// Interaktionslogik für EditAnalysisPage.xaml
    /// </summary>
    public partial class EditAnalysisPage : Page
    {
        public EditAnalysisPage()
        {
            InitializeComponent();
        }

        private string Analysis { get; set; }
        private bool Scenario { get; set; }
        private bool InterAwSTAsGroupedTargetListBox_SourceEvent { get; set; }

        private static TransactionAutomationDegrees _requiredAutomation = TransactionAutomationDegrees.nicht_gesetzt;

        public EditAnalysisPage(string data, bool scenario) : this()
        {
            InitializeComponent();

            Title = "Bearbeite " + data;

            Analysis = data;
            Scenario = scenario;
            vorhandeneBOlistBox.Items.Clear();
            sendingComboBox.Items.Clear();
            receivingComboBox.Items.Clear();


            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                ArrayList bos = new ArrayList();
                // TODO Beim Szenario muss die (1.1) Zuordnung der AT vor der (0.2) SOM-GP-Modell Struktursicht durchgeführt werden - Reiter tauschen!
                if (scenario == true)
                {
                    // Load the scenario
                    Scenario scenarioDS = ctx.Scenarios
                        .Where(b => b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                            && b.ScenarioName == data)
                        .OrderBy(b => b.ScenarioName)
                        .FirstOrDefault();

                    //Test.Content = "Meins " + data + " Szenario? " + scenario + " ASDF " + scenarioDS.ScenarioName;
                    SollkonzeptTabPage.Visibility = Visibility.Visible;
                    //GroupInterAwS.Visibility = Visibility.Visible;
                    GroupInterAwS.Header = "(2.1) Gruppierung von Transaktionen mit Inter-AwS-Kommunikationskanälen";
                    //PictureSollkonzeptTabPage.Visibility = Visibility.Visible;
                    PictureSollkonzeptTabPage.Header = "(2.3) Sollkonzept (IAS)";
                    IntegrationskonzeptTabPage.Visibility = Visibility.Visible;

                    foreach (BusinessObject bo in scenarioDS.BusinessObjects)
                    {
                        bos.Add(bo.BusinessObjectName);

                        // Fülle die Combo-Boxen zur Festlegung sendender und empfangender betrieblicher Objekte
                        sendingComboBox.Items.Add(bo.BusinessObjectName);
                        receivingComboBox.Items.Add(bo.BusinessObjectName);
                    }
                }
                else
                {
                    // Load the as-is analysis
                    AsIsAnalysis asIsAnalysis = ctx.AsIsAnalysis
                        .Where(b => b.AsIsAnalysisName == data)
                        .FirstOrDefault();

                    // TODO Register Sollkonzept müsste in abgeschwächter Form (ohne SOM-Unternehmensarchitektur) auch bei der Istanalyse eingebunden werden, um die Automatisierung festzuhalten.
                    //Test.Content = "Meins " + data + " Szenario? " + scenario + " ASDF " + asIsAnalysis.AsIsAnalysisName;
                    SollkonzeptTabPage.Visibility = Visibility.Collapsed;
                    //GroupInterAwS.Visibility = Visibility.Collapsed;
                    GroupInterAwS.Header = "(1.2) Gruppierung von Transaktionen mit Inter-AwS-Kommunikationskanälen";
                    //PictureSollkonzeptTabPage.Visibility = Visibility.Collapsed;
                    PictureSollkonzeptTabPage.Header = "(1.3) Istzustand (IAS)";
                    IntegrationskonzeptTabPage.Visibility = Visibility.Collapsed;

                    foreach (BusinessObject bo in asIsAnalysis.BusinessObjects)
                    {
                        bos.Add(bo.BusinessObjectName);

                        // Fülle die Combo-Boxen zur Festlegung sendender und empfangender betrieblicher Objekte
                        sendingComboBox.Items.Add(bo.BusinessObjectName);
                        receivingComboBox.Items.Add(bo.BusinessObjectName);
                    }
                }

                // Sortiere die betriebl. Objekte
                bos.Sort();
                foreach (string bo in bos)
                {
                    vorhandeneBOlistBox.Items.Add(bo);
                }

                if (!vorhandeneBOlistBox.Items.IsEmpty)
                {
                    // Selektiere das Erste Element in der Liste der vorhandenen betriebl. Objekte vor
                    vorhandeneBOlistBox.SelectedIndex = 0;
                }
                //fillTasListBox();
            }
        }

        #region Wechsel der Tabs
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //throw new NotImplementedException("Text in der Box wurde geändert! Es wurde eingegeben: " + KostenTextBox.Text.ToString());

            //do work when tab is changed
            if (e.Source is TabControl)
            {
                //if (SomGpModellIAStabPage.IsSelected)
                //{

                //}
                if (PictureSomGpModellIAStabPage.IsSelected)
                {
                    //string linkZumModell = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace("ü", "ue").Replace(" ", "_") + "/" + Analysis.Replace(" ", "_") + "/02_IAS.png");
                    string linkZumModell = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/" + Analysis.Replace(" ", "_") + "/02_IAS.png");
                    ImageSomGpModellIAStabPage.Source = new BitmapImage(new Uri(linkZumModell));
                }
                else if (GroupInterAwS.IsSelected)
                {
                    // Aufgabenobjekte dürfen nicht für alle Transaktionen einer Gruppe gleich sein, da sonst die AOs par. TAs gleich gemacht werden, obwohl bei jeder TA unterschiedliche AOs übertragen werden können. So würde es dann aussehen, dass alle par. TAs die gleichen AOs übertragen.
                    // Im Gegensatz dazu sind bei der Selektion einer TA-Gruppe alle AOs der in der Gruppe enthaltenen Transaktionen aufzulisten (alle AO aller TAs einer Gruppe).

                    //groupBox1.Visible = false;
                    //groupBox2.Visible = false;

                    TAgroupsListBox.Items.Clear();
                    InterAwSTAsGroupedListBox.Items.Clear();
                    InterAwSTAsUngroupedListBox.Items.Clear();
                    using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                    {
                        // Fülle die List-Box mit den Transaktionen die über Inter-AwS-Kommunikationskanäle laufen
                        var transactions = from b in ctx.Transactions
                                               //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                           where b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                                             && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                             && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                           orderby b.SendingObject.ZugeordnetesAwS.AufgabentraegerName, b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                           select b;
                        // Farbwerte willkürlich erstellen lassen um potenziell zu gruppierende TAs in der gleichen Farbe darzustellen.
                        Random rnd = new Random();
                        //int i = 0, red = rnd.Next(0, 255), green = rnd.Next(0, 255), blue = rnd.Next(0, 255);
                        int red = 0, green = 0, blue = 0;
                        string sendingObjectName = null, receivingObjectName = null;
                        foreach (Transaction transaction in transactions)
                        {
                            // Entweder die Strings sind im initialen Zustand ...
                            if (string.IsNullOrEmpty(sendingObjectName) && string.IsNullOrEmpty(receivingObjectName)
                                // ... oder sendendes und emfpangendes BO haben sich geändert, ...
                                || !string.IsNullOrEmpty(sendingObjectName) && !string.IsNullOrEmpty(receivingObjectName)
                                && (!sendingObjectName.Equals(transaction.SendingObject.BusinessObjectName) || !receivingObjectName.Equals(transaction.ReceivingObject.BusinessObjectName)))
                                //&& (!sendingObjectName.Equals(transaction.SendingObject.BusinessObjectName) && receivingObjectName.Equals(transaction.ReceivingObject.BusinessObjectName)
                                //    || sendingObjectName.Equals(transaction.SendingObject.BusinessObjectName) && !receivingObjectName.Equals(transaction.ReceivingObject.BusinessObjectName)
                                //    || !sendingObjectName.Equals(transaction.SendingObject.BusinessObjectName) && !receivingObjectName.Equals(transaction.ReceivingObject.BusinessObjectName)))
                            {
                                // ... dann ändere die BO-Namen und die Farbe des Eintrags
                                sendingObjectName = transaction.SendingObject.BusinessObjectName;
                                receivingObjectName = transaction.ReceivingObject.BusinessObjectName;
                                //InterAwSTAsUngroupedListBox.Items.Add(transaction.TransactionName);
                                ListBoxItem lbi = new ListBoxItem();
                                //lbi.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#ffaacc"));
                                //lbi.Background = Brushes.SeaGreen;
                                //lbi.Foreground = Brushes.DarkKhaki;
                                //lbi.Foreground = Brushes.Indigo;
                                lbi.Foreground = new SolidColorBrush(Color.FromRgb(Convert.ToByte(red = rnd.Next(0, 255)), Convert.ToByte(green = rnd.Next(0, 255)), Convert.ToByte(blue = rnd.Next(0, 255))));
                                lbi.Content = transaction.TransactionName;
                                InterAwSTAsUngroupedListBox.Items.Add(lbi);
                            }
                            else
                            {
                                //InterAwSTAsUngroupedListBox.Items.Add(transaction.TransactionName);
                                ListBoxItem lbi = new ListBoxItem();
                                //lbi.Foreground = Brushes.OliveDrab;
                                //lbi.Foreground = Brushes.SeaGreen;
                                lbi.Foreground = new SolidColorBrush(Color.FromRgb(Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue)));
                                lbi.Content = transaction.TransactionName;
                                InterAwSTAsUngroupedListBox.Items.Add(lbi);
                            }
                            //if (i == 0)
                            //{
                            //    // Selektiere das erste Element der Liste vor
                            //    InterAwSTAsUngroupedListBox.SelectedItem = transaction.TransactionName;

                            //    // TODO Aufrufen der zugehörigen fill-Methode (aus interAwSTAsListBox_SelectedIndexChanged)!
                            //    //sendingBOlabel.Text = transaction.SendingObject.BusinessObjectName;
                            //    //receivingBOlabel.Text = transaction.ReceivingObject.BusinessObjectName;

                            //    //// Fülle die Liste überlappender Aufgabenobjekte
                            //    //aoListBox.Items.Clear();
                            //    //foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                            //    //{
                            //    //    aoListBox.Items.Add(aufgabenobjekt.TaskObjectName);
                            //    //}
                            //    // istAutomatisierungComboBox
                            //    // sollAutomatisierungComboBox
                            //}
                            //i++;
                        }

                        if (!InterAwSTAsUngroupedListBox.Items.IsEmpty)
                        {
                            InterAwSTAsUngroupedListBox.SelectedIndex = 0;
                        }


                        // Fülle die List-Box mit den gruppierten Transaktionen
                        var transactionsWithoutGroups = from b in ctx.Transactions
                                               //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                           where b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                                             && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                             && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                             && b.TransactionGroup == null
                                           orderby b.TransactionName
                                           select b;
                        ArrayList taGroups = new ArrayList();
                        foreach (Transaction ta in transactionsWithoutGroups)
                        {
                            taGroups.Add(ta.TransactionName);
                        }

                        // oder not null und dann aber das Erste Element - Linq with order by and group by (Quelle: https://stackoverflow.com/questions/15571878/entity-framework-linq-query-with-order-by-and-group-by)
                        var transactionsWithGroups = ctx.Transactions
                            .Where(b => b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                                && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                && b.TransactionGroup != null)
                            .OrderBy(b => b.TransactionName)
                            .GroupBy(b => b.TransactionGroup)
                            .ToList();
                            //.FirstOrDefault();
                        foreach (var group in transactionsWithGroups)
                        {
                            taGroups.Add(group.FirstOrDefault().TransactionGroup);
                        }
                        taGroups.Sort();
                        foreach (string ta in taGroups)
                        {
                            TAgroupsListBox.Items.Add(ta);
                        }
                    }
                }
                else if (ZuordnungATtabPage.IsSelected)
                {
                    vorhandeneBOlistBoxZuordnung.Items.Clear();
                    foreach (var bo in vorhandeneBOlistBox.Items)
                    {
                        vorhandeneBOlistBoxZuordnung.Items.Add(bo);
                    }

                    if (!vorhandeneBOlistBoxZuordnung.Items.IsEmpty)
                    {
                        // Selektiere das erste Element vor
                        vorhandeneBOlistBoxZuordnung.SelectedIndex = 0;

                        fillZuordnungATtabPage();
                    }
                    //vorhandeneBOlistBoxZuordnung = vorhandeneBOlistBox;

                    fillATListBox();
                }
                //else if (PictureZuordnungATtabPage.IsSelected)
                //{
                //    // TODO
                //}
                else if (SollkonzeptTabPage.IsSelected)
                {
                    InterAwSTAsGroupedTargetListBox.Items.Clear();
                    InterAwSTAsListBox.Items.Clear();
                    using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                    {
                        // Fülle die List-Box mit den gruppierten Transaktionen
                        var transactionsWithoutGroups = from b in ctx.Transactions
                                                            //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                                        where b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                                                          && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                                          && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                                          && b.TransactionGroup == null
                                                        orderby b.TransactionName
                                                        select b;
                        ArrayList interAwSTas = new ArrayList();
                        foreach (Transaction ta in transactionsWithoutGroups)
                        {
                            interAwSTas.Add(ta.TransactionName);
                        }

                        // oder not null und dann aber das Erste Element
                        var transactionsWithGroups = ctx.Transactions
                            .Where(b => b.SendingObject.ZugeordnetesAwS != null &&  b.ReceivingObject.ZugeordnetesAwS != null
                                && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                                && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                && b.TransactionGroup != null)
                            .OrderBy(b => b.TransactionName)
                            .GroupBy(b => b.TransactionGroup)
                            .ToList();
                        //.FirstOrDefault();
                        foreach (var group in transactionsWithGroups)
                        {
                            interAwSTas.Add(group.FirstOrDefault().TransactionGroup);
                        }
                        interAwSTas.Sort();
                        foreach (string interAwSTa in interAwSTas)
                        {
                            InterAwSTAsGroupedTargetListBox.Items.Add(interAwSTa);
                        }


                        if (!InterAwSTAsGroupedTargetListBox.Items.IsEmpty)
                        {
                            InterAwSTAsGroupedTargetListBox.SelectedIndex = 0;
                        }
                    }
                }
                else if (PictureSollkonzeptTabPage.IsSelected)
                {
                    //string linkZumModell = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace("ü", "ue").Replace(" ", "_") + "/" + Analysis.Replace(" ", "_") + "/22_IAS.png");
                    string linkZumModell = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/" + Analysis.Replace(" ", "_") + "/22_IAS.png");
                    ImageSollkonzeptTabPage.Source = new BitmapImage(new Uri(linkZumModell));
                }
                else if (IntegrationskonzeptTabPage.IsSelected)
                {
                    // Blende die Boxen zur Bearbeitung der betrieblichen Objekte aus
                    //groupBox1.Visible = false;
                    //groupBox2.Visible = false;

                    DeltaTAsListBox.Items.Clear();
                    using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                    {
                        // Fülle die List-Box mit den gruppierten Transaktionen
                        var transactionsWithoutGroups = from b in ctx.Transactions
                                                            //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                                        where b.RequiredAutomation != b.ActualAutomation
                                                          && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                                          && b.TransactionGroup == null
                                                        orderby b.TransactionName
                                                        select b;
                        ArrayList deltaTas = new ArrayList();
                        foreach (Transaction ta in transactionsWithoutGroups)
                        {
                            deltaTas.Add(ta.TransactionName);
                        }

                        // oder not null und dann aber das Erste Element
                        var transactionsWithGroups = ctx.Transactions
                            .Where(b => b.RequiredAutomation != b.ActualAutomation
                                && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                && b.TransactionGroup != null)
                            .OrderBy(b => b.TransactionName)
                            .GroupBy(b => b.TransactionGroup)
                            .ToList();
                        //.FirstOrDefault();
                        foreach (var group in transactionsWithGroups)
                        {
                            deltaTas.Add(group.FirstOrDefault().TransactionGroup);
                        }
                        // Sortiere die Liste mit den Delta-TAs
                        deltaTas.Sort();
                        foreach (string ta in deltaTas)
                        {
                            DeltaTAsListBox.Items.Add(ta);
                        }

                        // Wenn in der Liste Transaktionen enthalten sind, dann selektiere das erste Element vor.
                        if (!DeltaTAsListBox.Items.IsEmpty)
                        {
                            DeltaTAsListBox.SelectedIndex = 0;
                            calculateIntegrationskonzept();
                        }
                        else
                        {
                            DeltaTAsListBox.Items.Add("Es besteht für keine Transaktion ein Handlungsbedarf!");
                        }

                        // Fülle die List-Box mit den Transaktionen, deren Ist-Automatisierung nicht mit der Soll-Automatisierung übereinstimmen.
                        //var transactions = from b in ctx.Transactions
                        //                   where b.RequiredAutomation != b.ActualAutomation
                        //                     && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                        //                     //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                        //                   select b;

                        //bool isFirstSelected = false;
                        //foreach (Transaction transaction in transactions)
                        //{
                        //    DeltaTAsListBox.Items.Add(transaction.TransactionName);
                        //    if (isFirstSelected == false)
                        //    {
                        //        DeltaTAsListBox.SelectedItem = transaction.TransactionName;
                        //    }
                        //    isFirstSelected = true;
                        //}
                        //if (DeltaTAsListBox.Items.Count == 0)
                    }

                    //if (!DeltaTAsListBox.Items.IsEmpty && !DeltaTAsListBox.Items[0].ToString().Equals("Es besteht für keine Transaktion ein Handlungsbedarf!"))
                    //{
                    //    DeltaTAsListBox.SelectedIndex = 0;
                    //    calculateIntegrationskonzept();
                    //}
                }
            }
        }

        private void fillATListBox()
        {
            AwsListBox.Items.Clear();
            PatListBox.Items.Clear();
            //UnterstuetzendeBOlistBox.Items.Clear();

            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                // Bereich für die AwS
                var awse = from b in ctx.Anwendungssysteme
                           where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                           orderby b.AufgabentraegerName
                           select b;
                foreach (Anwendungssystem aws in awse)
                {
                    bool zugeordnet = false;

                    // Falls ein betriebliches Objekt selektiert ist, selektiere das unterstützende AwS und die vom AwS anderen unterstützten BOs
                    if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
                    {
                        foreach (BusinessObject bo in aws.UnterstuetzendeBOs)
                        {
                            if (bo.ZugeordneteIstanalyse != null && bo.ZugeordneteIstanalyse.AsIsAnalysisName.Equals(Analysis)
                                || bo.ZugeordnetesSzenario != null && bo.ZugeordnetesSzenario.ScenarioName.Equals(Analysis))
                            {
                                // AwS ist einem BO des aktuellen Szenarios zugeordnet
                                zugeordnet = true;
                            }
                            if (bo.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString()))
                            {
                                AwsListBox.SelectedItem = aws.AufgabentraegerName;
                            }
                        }
                    }

                    if (zugeordnet)
                    {
                        AwsListBox.Items.Add(aws.AufgabentraegerName);
                    }
                }

                // Bereich für die pAT
                var pATs = from b in ctx.PersonelleAufgabentraeger
                           where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                           orderby b.AufgabentraegerName
                           select b;
                foreach (PersonellerAufgabentraeger pAT in pATs)
                {
                    bool zugeordnet = false;

                    // Falls ein betriebliches Objekt selektiert ist, selektiere den unterstützenden pAT und die vom pAT anderen unterstützten BOs
                    if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
                    {

                        foreach (BusinessObject bo in pAT.UnterstuetzendeBOs)
                        {
                            if (bo.ZugeordneteIstanalyse != null && bo.ZugeordneteIstanalyse.AsIsAnalysisName.Equals(Analysis)
                                || bo.ZugeordnetesSzenario != null && bo.ZugeordnetesSzenario.ScenarioName.Equals(Analysis))
                            {
                                // pAT ist einem BO des aktuellen Szenarios zugeordnet
                                zugeordnet = true;
                            }
                            if (bo.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString()))
                            {
                                PatListBox.SelectedItem = pAT.AufgabentraegerName;
                            }
                        }
                    }

                    if (zugeordnet)
                    {
                        PatListBox.Items.Add(pAT.AufgabentraegerName);
                    }

                }
            }
        }

        private void fillZuordnungATtabPage()
        {
            if (vorhandeneBOlistBoxZuordnung.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()
                        // FIXME hier kann auch mehr als ein BO gefunden werden, wenn nämlich das BO sowohl in der Istanalyse als auch in den Szenarien verwendet wird!
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Fülle die zugeordnete Stelle
                    if (bo.ZugeordneteStelle != null)
                    {
                        label3.Content = bo.ZugeordneteStelle.AufgabentraegerName;
                        namePATtextBox.Text = bo.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        label3.Content = "bislang wurde noch kein personeller AT zugeordnet";
                        namePATtextBox.Clear();
                    }

                    // Fülle das zugeordnete AwS
                    if (bo.ZugeordnetesAwS != null)
                    {
                        label4.Content = bo.ZugeordnetesAwS.AufgabentraegerName;
                        nameAwStextBox.Text = bo.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        label4.Content = "bislang wurde noch kein AwS zugeordnet";
                        nameAwStextBox.Clear();
                    }
                }
            }
        }

        private void fillTasListBox()
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                tasListBox.Items.Clear();
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Fülle die List-Box mit den Transaktionen, die zu einem betrieblichen Objekt gehören.
                    var transactions = from b in ctx.Transactions
                                       where b.SendingObject.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString())
                                         && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                         //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                         || b.ReceivingObject.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString())
                                         && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                       orderby b.TransactionName
                                         //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                       select b;
                    int i = 0;
                    foreach (Transaction transaction in transactions)
                    {
                        tasListBox.Items.Add(transaction.TransactionName);
                        if (i == 0)
                        {
                            tasListBox.SelectedItem = transaction.TransactionName;
                        }
                        i++;
                    }
                }
            }
        }
        #endregion

        #region betr. Obj. und Transaktionen
        private void HinzufuegenBetrObjButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(newBetrObjTextBox.Text))
            {
                //betrieblichesObjekt bo = new betrieblichesObjekt();
                //bo.nameBO = textBox1.Text;
                //proxy.create(bo);

                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    BusinessObject bo = new BusinessObject() { BusinessObjectName = newBetrObjTextBox.Text };

                    // Ordne das Betriebliche Objekt dem Mandanten zu
                    if (Scenario == true)
                    {
                        // Load the scenario
                        Scenario scenarioDS = ctx.Scenarios
                            .Where(b => b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                && b.ScenarioName == Analysis)
                            .FirstOrDefault();

                        scenarioDS.BusinessObjects.Add(bo);
                        bo.ZugeordnetesSzenario = scenarioDS;
                    }
                    else
                    {
                        // Load the as-is analysis
                        AsIsAnalysis asIsAnalysis = ctx.AsIsAnalysis
                            .Where(b => b.AsIsAnalysisName == Analysis)
                            .FirstOrDefault();

                        asIsAnalysis.BusinessObjects.Add(bo);
                        bo.ZugeordneteIstanalyse = asIsAnalysis;
                    }

                    ctx.BusinessObjects.Add(bo);
                    ctx.SaveChanges();
                }

                //toolStripStatusLabel1.Text = new BusinessObjectDAO().CreateNewBusinessObject(new BusinessObject(textBox1.Text));
                vorhandeneBOlistBox.Items.Add(newBetrObjTextBox.Text);

                // Fülle die Combo-Boxen zur Festlegung sendender und empfangender betrieblicher Objekte
                sendingComboBox.Items.Add(newBetrObjTextBox.Text);
                receivingComboBox.Items.Add(newBetrObjTextBox.Text);

                newBetrObjTextBox.Clear();
            }
        }

        private void DeleteBetrObjButton_Click(object sender, RoutedEventArgs e)
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                    .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                    .FirstOrDefault();

                    ctx.BusinessObjects.Remove(bo);
                    ctx.SaveChanges();
                }

                sendingComboBox.Items.Remove(vorhandeneBOlistBox.SelectedItem.ToString());
                receivingComboBox.Items.Add(vorhandeneBOlistBox.SelectedItem.ToString());

                //toolStripStatusLabel1.Text = new BusinessObjectDAO().DeleteBusinessObjectByName(listBox1.SelectedItem.ToString());
                vorhandeneBOlistBox.Items.Remove(vorhandeneBOlistBox.SelectedItem);
            }
        }

        private void AddTAbutton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(taTextBox.Text) && vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == taTextBox.Text
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                            //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    if (transaction == null)
                    {
                        transaction = new Transaction() { TransactionName = taTextBox.Text };

                        // AIM
                        //transaction.Reihenfolgebeziehung = false;
                        //transaction.PartielleGleichheit = false;
                        //transaction.PartielleIdentitaet = false;
                        //transaction.UeberlappendeLoesungsverfahren = false;

                        // SOM-Unternehmensarchitektur
                        // U-Plan
                        //transaction.Strategie = false;
                        //transaction.Vorgabe = false;
                        //transaction.Interdependenzen = false;
                        //transaction.Normen = false;
                        //transaction.Datenschutz = false;
                        //transaction.Kosten = false;

                        // Ressourcenebene
                        //transaction.Interoberability = false;
                        //transaction.KnowHow = false;
                        //transaction.Einfachheit = false;

                        if (Scenario == true)
                        {
                            transaction.ZugeordnetesSzenario = bo.ZugeordnetesSzenario;
                        }
                        else
                        {
                            transaction.ZugeordneteIstanalyse = bo.ZugeordneteIstanalyse;
                        }

                        transaction.SendingObject = bo;
                        transaction.ReceivingObject = bo;
                        ctx.Transactions.Add(transaction);
                        ctx.SaveChanges();
                    }
                    bo.ZugeordneteTAs.Add(transaction);
                    ctx.SaveChanges();
                }
                tasListBox.Items.Add(taTextBox.Text);
            }
            taTextBox.Clear();
        }

        private void DeleteTAbutton_Click(object sender, RoutedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    Transaction aws = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    if (aws == null)
                    {
                        aws = new Transaction() { TransactionName = taTextBox.Text };
                        ctx.Transactions.Add(aws);
                        ctx.SaveChanges();
                    }
                    ctx.Transactions.Remove(aws);
                    ctx.SaveChanges();
                }
                tasListBox.Items.Remove(tasListBox.SelectedItem);
            }
        }

        private void vorhandeneBOlistBox_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            fillTasListBox();
        }

        private void tasListBox_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Clearing Combo-Boxes
                    //sendingComboBox.Items.Clear();
                    //receivingComboBox.Items.Clear();


                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    //sendingComboBox.Items.Add(bo.SendingObject.BusinessObjectName);
                    //receivingComboBox.Items.Add(bo.ReceivingObject.BusinessObjectName);

                    // Fülle die Combo-Boxen
                    //var bos = from b in ctx.BusinessObjects
                    //          select b;
                    //foreach (BusinessObject ba in bos)
                    //{
                    //    sendingComboBox.Items.Add(ba.BusinessObjectName);
                    //    receivingComboBox.Items.Add(ba.BusinessObjectName);
                    //}

                    // Lege den selektierten Eintrag in der Combo-Box fest
                    string sendingText = "Kein sendendes betr. Obj. ausgewählt";
                    if (transaction.SendingObject != null)
                    {
                        sendingComboBox.Items.Remove(sendingText);
                        sendingComboBox.SelectedItem = transaction.SendingObject.BusinessObjectName;
                    }
                    else
                    {
                        sendingComboBox.Items.Add(sendingText);
                        sendingComboBox.SelectedItem = sendingText;
                    }

                    string receivingText = "Kein empfangendes betr. Obj. ausgewählt";
                    if (transaction.ReceivingObject != null)
                    {
                        //receivingComboBox.Items.Contains(receivingComboBox);
                        receivingComboBox.Items.Remove(receivingText);
                        receivingComboBox.SelectedItem = transaction.ReceivingObject.BusinessObjectName;
                    }
                    else
                    {
                        receivingComboBox.Items.Add(receivingText);
                        receivingComboBox.SelectedItem = receivingText;
                    }

                    // Fülle die Liste überlappender Aufgabenobjekte
                    ArrayList aos = new ArrayList();
                    aoListBox.Items.Clear();
                    foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                    {
                        aos.Add(aufgabenobjekt.TaskObjectName);
                    }
                    aos.Sort();
                    foreach (string ao in aos)
                    {
                        aoListBox.Items.Add(ao);
                    }

                    // AIM
                    ReihenfolgebezCheckBox.IsChecked = transaction.Reihenfolgebeziehung;
                    GleichheitAOcheckBox.IsChecked = transaction.PartielleGleichheit;
                    IdentityAOcheckBox.IsChecked = transaction.PartielleIdentitaet;
                    LvCheckBox.IsChecked = transaction.UeberlappendeLoesungsverfahren;
                }
            }
        }

        private void sendingComboBox_SelectedItemChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Entferne die Transaktion aus dem vormals zugeordneten betrieblichen Objekt
                    BusinessObject oldSendingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == sendingComboBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();
                    if (oldSendingBO != null)
                    {
                        // Proceed the changes for every selected transaction
                        foreach (Transaction transaction in transactions)
                        {
                            oldSendingBO.ZugeordneteTAs.Remove(transaction);
                        }
                    }

                    // Query for the Blog named ADO.NET Blog
                    BusinessObject newSendingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == sendingComboBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaction in transactions)
                    {
                        transaction.SendingObject = newSendingBO;
                        if (newSendingBO != null)
                        {
                            newSendingBO.ZugeordneteTAs.Add(transaction);
                        }
                        transaction.SendingObject = newSendingBO;
                    }

                    ctx.SaveChanges();
                }
            }
        }

        private void receivingComboBox_SelectedItemChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Entferne die Transaktion aus dem vormals zugeordneten betrieblichen Objekt
                    BusinessObject oldReceivingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == receivingComboBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();
                    if (oldReceivingBO != null)
                    {
                        // Proceed the changes for every selected transaction
                        foreach (Transaction transaction in transactions)
                        {
                            oldReceivingBO.ZugeordneteTAs.Remove(transaction);
                        }
                    }

                    // Query for the Blog named ADO.NET Blog
                    BusinessObject newReceivingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == receivingComboBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaction in transactions)
                    {
                        transaction.ReceivingObject = newReceivingBO;
                        if (newReceivingBO != null)
                        {
                            newReceivingBO.ZugeordneteTAs.Add(transaction);
                        }
                        transaction.ReceivingObject = newReceivingBO;
                    }

                    ctx.SaveChanges();
                }
            }
        }

        private void AddAObutton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(aoTextBox.Text) && tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    TaskObject aufgabenobjekt = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == aoTextBox.Text
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    if (aufgabenobjekt == null)
                    {
                        aufgabenobjekt = new TaskObject() { TaskObjectName = aoTextBox.Text };

                        if (Scenario == true)
                        {
                            aufgabenobjekt.ZugeordnetesSzenario = transaktion.ZugeordnetesSzenario;
                        }
                        else
                        {
                            aufgabenobjekt.ZugeordneteIstanalyse = transaktion.ZugeordneteIstanalyse;
                        }
                        ctx.TaskObjects.Add(aufgabenobjekt);
                        //ctx.SaveChanges();
                    }
                    transaktion.UeberlappendeAOs.Add(aufgabenobjekt);
                    ctx.SaveChanges();
                }
                aoListBox.Items.Add(aoTextBox.Text);
            }
            aoTextBox.Clear();
        }

        private void DelteAObutton_Click(object sender, RoutedEventArgs e)
        {
            if (aoListBox.SelectedItem != null && !string.IsNullOrEmpty(aoListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    TaskObject aufgabenobjekt = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == aoListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    transaktion.UeberlappendeAOs.Remove(aufgabenobjekt);

                    // Sofern das AO keiner anderen Transaktion zugewiesen ist, kann es entfernt werden
                    aufgabenobjekt.ZugeordneteTransaktionen.Remove(transaktion);
                    if (aufgabenobjekt.ZugeordneteTransaktionen.Count == 0)
                    {
                        ctx.TaskObjects.Remove(aufgabenobjekt);
                    }

                    ctx.SaveChanges();
                }
                aoListBox.Items.Remove(aoListBox.SelectedItem.ToString());
            }
        }

        private void ReihenfolgebezCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Reihenfolgebeziehung = ReihenfolgebezCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void GleichheitAOcheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.PartielleGleichheit = GleichheitAOcheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void IdentityAOcheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.PartielleIdentitaet = IdentityAOcheckBox.IsChecked.Value;
                    }

                    // Wenn partielle Identität, dann auch partielle Gleichheit
                    //if (identityAOcheckBox.Checked == true)
                    //{
                    //    gleichheitAOcheckBox.Checked = true;
                    //}
                    //else
                    //{
                    //    gleichheitAOcheckBox.Checked = false;
                    //}
                    //transaktion.PartielleGleichheit = gleichheitAOcheckBox.Checked;
                    ctx.SaveChanges();
                }
            }
        }

        private void LvCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the name of the transaction-group
                    Transaction transactionGroupName = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == tasListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == transactionGroupName.TransactionGroup
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.UeberlappendeLoesungsverfahren = LvCheckBox.IsChecked.Value;
                    }

                    // Wenn überlappende Lösungsverfahren, dann auch partielle Gleichheit und partielle Identität von AO
                    //if (lvCheckBox.Checked == true)
                    //{
                    //    gleichheitAOcheckBox.Checked = true;
                    //    identityAOcheckBox.Checked = true;
                    //}
                    //else
                    //{
                    //    gleichheitAOcheckBox.Checked = false;
                    //    identityAOcheckBox.Checked = false;
                    //}
                    //transaktion.PartielleGleichheit = gleichheitAOcheckBox.Checked;
                    //transaktion.PartielleGleichheit = identityAOcheckBox.Checked;
                    ctx.SaveChanges();
                }
            }
        }
        #endregion

        #region Zuordnung AT und AwS-Grenzen
        private void VorhandeneBOlistBoxZuordnung_SelectedIndexChanged(object sender, SelectionChangedEventArgs e)
        {
            fillATListBox();
            fillZuordnungATtabPage();
        }

        private void PATzuordnenButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(namePATtextBox.Text) && vorhandeneBOlistBoxZuordnung.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()))
            {

                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    //if (bo.ZugeordneteStelle != null)
                    //{
                    //    bo.ZugeordneteStelle.AufgabentraegerName = namePATtextBox.Text;
                    //}
                    //else
                    //{

                    //PersonellerAufgabentraegerDAO pATDAO = new PersonellerAufgabentraegerDAO();
                    PersonellerAufgabentraeger pAT = ctx.PersonelleAufgabentraeger
                        .Where(b => b.AufgabentraegerName == namePATtextBox.Text
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    if (pAT == null)
                    {
                        pAT = new PersonellerAufgabentraeger() { AufgabentraegerName = namePATtextBox.Text };
                        if (Scenario == true)
                        {
                            pAT.ZugeordneteFallstudie = bo.ZugeordnetesSzenario.ZugeordneteFallstudie;
                        }
                        else
                        {
                            pAT.ZugeordneteFallstudie = bo.ZugeordneteIstanalyse.ZugeordneteFallstudie;
                        }
                        //toolStripStatusLabel1.Text = new PersonellerAufgabentraegerDAO().CreateNewPersonellerAufgabentraeger(pAT);
                        ctx.PersonelleAufgabentraeger.Add(pAT);
                        ctx.SaveChanges();
                    }
                    //pAT.UnterstuetzendeBOs.Add(bo);
                    bo.ZugeordneteStelle = pAT;
                    ctx.SaveChanges();
                    //}
                }
                label3.Content = namePATtextBox.Text;
            }
            namePATtextBox.Clear();
        }

        private void PATdeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (PatListBox.SelectedItem != null && !string.IsNullOrEmpty(PatListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    PersonellerAufgabentraeger pAT = ctx.PersonelleAufgabentraeger
                        .Where(b => b.AufgabentraegerName == PatListBox.SelectedItem.ToString()
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    ctx.PersonelleAufgabentraeger.Remove(pAT);
                    ctx.SaveChanges();

                    PatListBox.Items.Remove(PatListBox.SelectedItem);

                    if (!PatListBox.Items.IsEmpty)
                    {
                        PatListBox.SelectedIndex = 0;
                    }
                }
            }
        }

        private void AwSzuordnenButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(nameAwStextBox.Text) && vorhandeneBOlistBoxZuordnung.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()))
            {

                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBoxZuordnung.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .FirstOrDefault();

                    //if (bo.ZugeordnetesAwS != null)
                    //{
                    //    bo.ZugeordnetesAwS.AufgabentraegerName = nameAwStextBox.Text;
                    //}
                    //else
                    //{
                    Anwendungssystem aws = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == nameAwStextBox.Text
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    if (aws == null)
                    {
                        aws = new Anwendungssystem() { AufgabentraegerName = nameAwStextBox.Text };
                        if (Scenario == true)
                        {
                            aws.ZugeordneteFallstudie = bo.ZugeordnetesSzenario.ZugeordneteFallstudie;
                        }
                        else
                        {
                            aws.ZugeordneteFallstudie = bo.ZugeordneteIstanalyse.ZugeordneteFallstudie;
                        }
                        ctx.Anwendungssysteme.Add(aws);
                        ctx.SaveChanges();
                    }
                    bo.ZugeordnetesAwS = aws;
                    ctx.SaveChanges();
                    //}
                }
                label4.Content = nameAwStextBox.Text;
            }
            nameAwStextBox.Clear();
        }

        private void AwSdeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (AwsListBox.SelectedItem != null && !string.IsNullOrEmpty(AwsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Anwendungssystem aws = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == AwsListBox.SelectedItem.ToString()
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    ctx.Anwendungssysteme.Remove(aws);
                    ctx.SaveChanges();

                    AwsListBox.Items.Remove(AwsListBox.SelectedItem);

                    if (!AwsListBox.Items.IsEmpty)
                    {
                        AwsListBox.SelectedIndex = 0;
                    }
                }
            }
        }

        private void AwsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UnterstuetzendeBOlistBox.Items.Clear();

            if (AwsListBox.SelectedItem != null && !string.IsNullOrEmpty(AwsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Anwendungssystem awse = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == AwsListBox.SelectedItem.ToString()
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    ArrayList bos = new ArrayList();
                    foreach (BusinessObject bo in awse.UnterstuetzendeBOs)
                    {
                        // AwS ist einem BO des aktuellen Szenarios zugeordnet, dann füge es der Liste hinzu
                        if (bo.ZugeordneteIstanalyse != null && bo.ZugeordneteIstanalyse.AsIsAnalysisName.Equals(Analysis)
                            || bo.ZugeordnetesSzenario != null && bo.ZugeordnetesSzenario.ScenarioName.Equals(Analysis))
                        {
                            bos.Add(bo.BusinessObjectName);
                        }
                    }
                    // Sortiere die unterstützenden betriebl. Objekte
                    bos.Sort();
                    foreach (string bo in bos)
                    {
                        UnterstuetzendeBOlistBox.Items.Add(bo);
                    }
                }
            }
        }

        private void PatListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UnterstuetzendeBOlistBoxPAT.Items.Clear();

            if (PatListBox.SelectedItem != null && !string.IsNullOrEmpty(PatListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    PersonellerAufgabentraeger pATs = ctx.PersonelleAufgabentraeger
                        .Where(b => b.AufgabentraegerName == PatListBox.SelectedItem.ToString()
                            && b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    ArrayList bos = new ArrayList();
                    foreach (BusinessObject bo in pATs.UnterstuetzendeBOs)
                    {
                        // pAT ist einem BO des aktuellen Szenarios zugeordnet, dann füge es der Liste hinzu
                        if (bo.ZugeordneteIstanalyse != null && bo.ZugeordneteIstanalyse.AsIsAnalysisName.Equals(Analysis)
                            || bo.ZugeordnetesSzenario != null && bo.ZugeordnetesSzenario.ScenarioName.Equals(Analysis))
                        {
                            bos.Add(bo.BusinessObjectName);
                        }
                    }
                    // Sortiere die unterstützenden betriebl. Objekte
                    bos.Sort();
                    foreach (string bo in bos)
                    {
                        UnterstuetzendeBOlistBoxPAT.Items.Add(bo);
                    }
                }
            }
        }
        #endregion

        #region Gruppierung von Transaktionen mit Inter-AwS-Kommunikationskanälen
        private void InterAwSTAsUngroupedListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string nameOfTa = null;
            if ((e.Source as ListBox).Name.Equals("InterAwSTAsGroupedListBox") && InterAwSTAsGroupedListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsGroupedListBox.SelectedItem.ToString()))
            {
                nameOfTa = InterAwSTAsGroupedListBox.SelectedItem.ToString();
            }
            else if ((e.Source as ListBox).Name.Equals("InterAwSTAsUngroupedListBox") && InterAwSTAsUngroupedListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsUngroupedListBox.SelectedItem.ToString()))
            {
                nameOfTa = ((ListBoxItem)InterAwSTAsUngroupedListBox.SelectedItem).Content.ToString();
            }

            if (!string.IsNullOrEmpty(nameOfTa))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == nameOfTa
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    SendingBOungroupedLabel.Content = transaction.SendingObject.BusinessObjectName;
                    if (transaction.SendingObject.ZugeordnetesAwS != null)
                    {
                        SendingAwSungroupedLabel.Content = transaction.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        SendingAwSungroupedLabel.Content = "Kein AwS zugeordnet";
                    }
                    if (transaction.SendingObject.ZugeordneteStelle != null)
                    {
                        SendingPATungroupedLabel.Content = transaction.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        SendingPATungroupedLabel.Content = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    ReceivingBOungroupedLabel.Content = transaction.ReceivingObject.BusinessObjectName;
                    if (transaction.ReceivingObject.ZugeordnetesAwS != null)
                    {
                        ReceivingAwSungroupedLabel.Content = transaction.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        ReceivingAwSungroupedLabel.Content = "Kein AwS zugeordnet";
                    }
                    if (transaction.ReceivingObject.ZugeordneteStelle != null)
                    {
                        ReceivingPATungroupedLabel.Content = transaction.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        ReceivingPATungroupedLabel.Content = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    // Fülle die Liste überlappender Aufgabenobjekte
                    ArrayList aos = new ArrayList();
                    AoUngroupedListBox.Items.Clear();
                    foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                    {
                        aos.Add(aufgabenobjekt.TaskObjectName);
                    }
                    aos.Sort();
                    foreach (string ao in aos)
                    {
                        AoUngroupedListBox.Items.Add(ao);
                    }

                    // AIM
                    ReihenfolgeCheckBoxReadUngrouped.IsChecked = transaction.Reihenfolgebeziehung;
                    AoGleichheitCheckBoxReadUngrouped.IsChecked = transaction.PartielleGleichheit;
                    AoIdentityCheckBoxReadUngrouped.IsChecked = transaction.PartielleIdentitaet;
                    lvOverlappingCheckBoxReadUngrouped.IsChecked = transaction.UeberlappendeLoesungsverfahren;

                    newTransactionGroupTextBox.Text = transaction.TransactionGroup;
                }
            }
        }

        private void TAgroupsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TAgroupsListBox.SelectedItem != null && !string.IsNullOrEmpty(TAgroupsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Fülle die List-Box mit den Transaktionen die in der Transaktionsgruppe enthalten sind
                    var transactions = from b in ctx.Transactions
                                       where b.TransactionGroup == null && b.TransactionName == TAgroupsListBox.SelectedItem.ToString()
                                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                        || b.TransactionGroup != null && b.TransactionGroup == TAgroupsListBox.SelectedItem.ToString()
                                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                       orderby b.TransactionName
                                       select b;

                    InterAwSTAsGroupedListBox.Items.Clear();
                    ArrayList aos = new ArrayList();
                    AoUngroupedListBox.Items.Clear();
                    int i = 0;
                    foreach (Transaction transaction in transactions)
                    {
                        InterAwSTAsGroupedListBox.Items.Add(transaction.TransactionName);
                        // Ändere die Daten der Anzeige nur beim ersten Element, da die anderen Elmente der Liste eigentlich die gleichen Ausprägungen haben müssen.
                        if (i == 0)
                        {
                            SendingBOungroupedLabel.Content = transaction.SendingObject.BusinessObjectName;
                            if (transaction.SendingObject.ZugeordnetesAwS != null)
                            {
                                SendingAwSungroupedLabel.Content = transaction.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                            }
                            else
                            {
                                SendingAwSungroupedLabel.Content = "Kein AwS zugeordnet";
                            }
                            if (transaction.SendingObject.ZugeordneteStelle != null)
                            {
                                SendingPATungroupedLabel.Content = transaction.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                            }
                            else
                            {
                                SendingPATungroupedLabel.Content = "Kein personeller Aufgabenträger zugeordnet";
                            }

                            ReceivingBOungroupedLabel.Content = transaction.ReceivingObject.BusinessObjectName;
                            if (transaction.ReceivingObject.ZugeordnetesAwS != null)
                            {
                                ReceivingAwSungroupedLabel.Content = transaction.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                            }
                            else
                            {
                                ReceivingAwSungroupedLabel.Content = "Kein AwS zugeordnet";
                            }
                            if (transaction.ReceivingObject.ZugeordneteStelle != null)
                            {
                                ReceivingPATungroupedLabel.Content = transaction.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;
                            }
                            else
                            {
                                ReceivingPATungroupedLabel.Content = "Kein personeller Aufgabenträger zugeordnet";
                            }

                            // AIM
                            ReihenfolgeCheckBoxReadUngrouped.IsChecked = transaction.Reihenfolgebeziehung;
                            AoGleichheitCheckBoxReadUngrouped.IsChecked = transaction.PartielleGleichheit;
                            AoIdentityCheckBoxReadUngrouped.IsChecked = transaction.PartielleIdentitaet;
                            lvOverlappingCheckBoxReadUngrouped.IsChecked = transaction.UeberlappendeLoesungsverfahren;
                        }

                        // Fülle die Liste überlappender Aufgabenobjekte aller in der Gruppe enthaltenen Transaktionen
                        foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                        {
                            // Füge nur AO der Liste hinzu, die bislang noch nicht enthalten sind.
                            if (!aos.Contains(aufgabenobjekt.TaskObjectName))
                            {
                                aos.Add(aufgabenobjekt.TaskObjectName);
                            }
                            //if (!AoUngroupedListBox.Items.Contains(aufgabenobjekt.TaskObjectName))
                            //{
                            //    AoUngroupedListBox.Items.Add(aufgabenobjekt.TaskObjectName);
                            //}
                        }
                        i++;
                    }
                    aos.Sort();
                    foreach (string ao in aos)
                    {
                        AoUngroupedListBox.Items.Add(ao);
                    }
                }
            }
        }

        private void AddTransactionGroupButton_Click(object sender, RoutedEventArgs e)
        {
            // TODO Entferne die Transaktion aus der Gruppe der Transaktionen

            // TODO
            // Gruppiert werden können nur die TAs, wo Sender und Empfänger AwS gleich sind

            // Gleiche überlappende AO bei gruppierten TAs
            // Gleiche AIM bei gruppierten TAs

            //InterAwSTAsGroupedListBox
            if (InterAwSTAsUngroupedListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsUngroupedListBox.SelectedItem.ToString())
                && !string.IsNullOrEmpty(newTransactionGroupTextBox.Text))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == ((ListBoxItem)InterAwSTAsUngroupedListBox.SelectedItem).Content.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    transaction.TransactionGroup = newTransactionGroupTextBox.Text;


                    // Der Code hier wird nun "Buggy" sein und die ListBoxen werden sich nicht mehr dynamisch zur Laufzeit neu aufbauen, wenn TAs gruppiert werden. Das wird aber so belassen und bei Vorführungen dürfen keine Gruppen gebildet werden (nur lesende Vorführungen!).

                    // Ändere die Anzeige der Transaktionsgruppen
                    // Entferne die Transaktion aus der Liste mit den gruppierten Transaktionen, sofern sie vorhanden ist.
                    if (TAgroupsListBox.Items.Contains(InterAwSTAsUngroupedListBox.SelectedItem.ToString()))
                    {
                        TAgroupsListBox.Items.Remove(InterAwSTAsUngroupedListBox.SelectedItem.ToString());
                    }
                    // Füge die neu gebildete Transaktiongruppe der Liste mit den gruppierten Transaktionen hinzu.
                    if (!TAgroupsListBox.Items.Contains(newTransactionGroupTextBox.Text))
                    {
                        TAgroupsListBox.Items.Add(newTransactionGroupTextBox.Text);
                    }
                    // Selektiere die neu gebildete Transaktionsgruppe in der Liste mit den gruppierten Transaktionen.
                    TAgroupsListBox.SelectedIndex = TAgroupsListBox.Items.IndexOf(newTransactionGroupTextBox.Text);

                    //InterAwSTAsGroupedListBox.Items.Add(InterAwSTAsUngroupedListBox.SelectedItem.ToString());
                    ctx.SaveChanges();
                }
            }
        }
        #endregion

        #region Sollkonzept für Transaktionen mit Inter-AwS-Kommunikationskanälen
        private void InterAwSTAsGroupedTargetListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsGroupedTargetListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Fülle die List-Box mit den Transaktionen die in der Transaktionsgruppe enthalten sind
                    var transactions = from b in ctx.Transactions
                                       where b.TransactionGroup == null && b.TransactionName == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                        || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                                       orderby b.TransactionName
                                       select b;

                    InterAwSTAsListBox.Items.Clear();
                    ArrayList aos = new ArrayList();
                    AoListBox.Items.Clear();
                    foreach (Transaction transaction in transactions)
                    {
                        InterAwSTAsListBox.Items.Add(transaction.TransactionName);

                        // Fülle die Liste überlappender Aufgabenobjekte aller in der Gruppe enthaltenen Transaktionen
                        foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                        {
                            // Füge nur AO der Liste hinzu, die bislang noch nicht enthalten sind.
                            if (!aos.Contains(aufgabenobjekt.TaskObjectName))
                            {
                                aos.Add(aufgabenobjekt.TaskObjectName);
                            }
                            //if (!AoListBox.Items.Contains(aufgabenobjekt.TaskObjectName))
                            //{
                            //    AoListBox.Items.Add(aufgabenobjekt.TaskObjectName);
                            //}
                        }
                    }
                    aos.Sort();
                    foreach (string ao in aos)
                    {
                        AoListBox.Items.Add(ao);
                    }

                    if (!InterAwSTAsListBox.Items.IsEmpty)
                    {
                        InterAwSTAsGroupedTargetListBox_SourceEvent = true;
                        InterAwSTAsListBox.SelectedIndex = 0;
                    }
                }
            }
        }

        private void InterAwSTAsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //string notSet = "nicht gesetzt";
            // TODO neue Tabs zu programmieren
            // (1) Ein Tab mit der Wahl des Integrationskonzepts erstellen!
            // Da könnte auch die gekürzte Tabelle eingefügt werden.
            // Die umfangreiche Tabelle in der Dissertation mit dem Integrationskonzept zusammenlegen!
            // Statt auf die einzelnen Situationen auf die Auswirkungen bei den Integrationskonzepten eingehen.
            // Diese Tabelle muss dann tatsächlich nur noch die Situationen berücksichtigen bei denen heterogene AwS zum Einsatz kommen!
            // Auf jeden Fall die identifizierten AIM auslesen (lesen) und anzeigen und die Ausprägungen Integrationsmerkmale unterbringen (lesen und schreiben) (kombinierte Tabelle der Präsentation)!

            // TODO aktuelle Tab zu Ende programmieren
            // aoListBox Laden -> ok, testen
            // Zugeordnete betr. Obj. laden -> ok, testen
            // Soll- und Ist-Automatisierung laden -> ok, testen
            // Identifizierte AIM laden -> ok, testen
            // Ausgeprägte Merkmale laden -> ok, testen
            // ausgeprägte Merkmale gibt es einmal in binärer Ausprägung (boolean) (U-Plan und Ressourcenebene) und einmal in dreifacher Ausprägung (niedrig, moderat, hoch) (Aufgabenebene)
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                            //&& b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();

                    SendingBOlabel.Content = transaction.SendingObject.BusinessObjectName;
                    if (transaction.SendingObject.ZugeordnetesAwS != null)
                    {
                        SendingAwSlabel.Content = transaction.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        SendingAwSlabel.Content = "Kein AwS zugeordnet";
                    }
                    if (transaction.SendingObject.ZugeordneteStelle != null)
                    {
                        SendingPATlabel.Content = transaction.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        SendingPATlabel.Content  = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    ReceivingBOlabel.Content = transaction.ReceivingObject.BusinessObjectName;
                    if (transaction.ReceivingObject.ZugeordnetesAwS != null)
                    {
                        ReceivingAwSlabel.Content = transaction.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        ReceivingAwSlabel.Content = "Kein AwS zugeordnet";
                    }
                    if (transaction.ReceivingObject.ZugeordneteStelle != null)
                    {
                        ReceivingPATlabel.Content = transaction.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        ReceivingPATlabel.Content = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    // Fülle die Liste überlappender Aufgabenobjekte nur, wenn von einer einzelnen Transaktion aus darauf geklickt wurde. Bei einer TA-Gruppe sollen alle AOs der in einer TA-Gruppe enthaltenen TAs angezeigt werden.
                    //if ((e.Source as ListBox).Name.Equals("InterAwSTAsListBox"))
                    if (!InterAwSTAsGroupedTargetListBox_SourceEvent)
                    {
                        ArrayList aos = new ArrayList();
                        AoListBox.Items.Clear();
                        foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                        {
                            aos.Add(aufgabenobjekt.TaskObjectName);
                        }
                        aos.Sort();
                        foreach (string ao in aos)
                        {
                            AoListBox.Items.Add(ao);
                        }
                    }
                    // Setze die Variable wieder zurück
                    InterAwSTAsGroupedTargetListBox_SourceEvent = false;

                    // AIM
                    ReihenfolgeCheckBoxRead.IsChecked = transaction.Reihenfolgebeziehung;
                    AoGleichheitCheckBoxRead.IsChecked = transaction.PartielleGleichheit;
                    AoIdentityCheckBoxRead.IsChecked = transaction.PartielleIdentitaet;
                    lvOverlappingCheckBoxRead.IsChecked = transaction.UeberlappendeLoesungsverfahren;

                    // SOM-Unternehmensarchitektur
                    // U-Plan
                    StrategieCheckBox.IsChecked = transaction.Strategie;
                    //VorgabeCheckBox.IsChecked = transaction.Vorgabe;
                    InterdependenzenCheckBox.IsChecked = transaction.Interdependenzen;
                    NormenCheckBox.IsChecked = transaction.Normen;
                    DatenschutzCheckBox.IsChecked = transaction.Datenschutz;
                    KostenTextBox.Text = transaction.Kosten.ToString();

                    // GP-Modell
                    // Periodizität
                    if (transaction.Periodizitaet.Equals(MerkmaleAuspraegungen.niedrig))
                    {
                        //periodizitaetComboBox.SelectedItem = notSet;
                        PeriodizitaetCBIlow.IsSelected = true;
                    }
                    else if (transaction.Periodizitaet.Equals(MerkmaleAuspraegungen.moderat))
                    {
                        PeriodizitaetCBImoderate.IsSelected = true;
                    }
                    else if (transaction.Periodizitaet.Equals(MerkmaleAuspraegungen.hoch))
                    {
                        PeriodizitaetCBIhigh.IsSelected = true;
                    }
                    else
                    {
                        PeriodizitaetCBInotSet.IsSelected = true;
                    }

                    // Größe
                    if (transaction.Groesse.Equals(MerkmaleAuspraegungen.niedrig))
                    {
                        GroesseCBIlow.IsSelected = true;
                        //groesseComboBox.SelectedItem = notSet;
                    }
                    else if (transaction.Groesse.Equals(MerkmaleAuspraegungen.moderat))
                    {
                        GroesseCBImoderate.IsSelected = true;
                    }
                    else if (transaction.Groesse.Equals(MerkmaleAuspraegungen.hoch))
                    {
                        GroesseCBIhigh.IsSelected = true;
                    }
                    else
                    {
                        GroesseCBInotSet.IsSelected = true;
                    }

                    // Intensität
                    if (transaction.Intensitaet.Equals(MerkmaleAuspraegungen.niedrig))
                    {
                        IntensitaetCBIlow.IsSelected = true;
                    }
                    else if (transaction.Intensitaet.Equals(MerkmaleAuspraegungen.moderat))
                    {
                        IntensitaetCBImoderate.IsSelected = true;
                    }
                    else if (transaction.Intensitaet.Equals(MerkmaleAuspraegungen.hoch))
                    {
                        IntensitaetCBIhigh.IsSelected = true;
                    }
                    else
                    {
                        IntensitaetCBInotSet.IsSelected = true;
                    }

                    // Anpassbarkeit
                    if (transaction.Anpassbarkeit.Equals(MerkmaleAuspraegungen.niedrig))
                    {
                        AnpassbarkeitCBIlow.IsSelected = true;
                    }
                    else if (transaction.Anpassbarkeit.Equals(MerkmaleAuspraegungen.moderat))
                    {
                        AnpassbarkeitCBImoderate.IsSelected = true;
                    }
                    else if (transaction.Anpassbarkeit.Equals(MerkmaleAuspraegungen.hoch))
                    {
                        AnpassbarkeitCBIhigh.IsSelected = true;
                    }
                    else
                    {
                        AnpassbarkeitCBInotSet.IsSelected = true;
                    }

                    // Nutzungsdauer
                    if (transaction.Nutzungsdauer.Equals(NutzungsdauerAuspraegungen.kurzfristig))
                    {
                        NutzungsdauerCBIlow.IsSelected = true;
                    }
                    else if (transaction.Nutzungsdauer.Equals(NutzungsdauerAuspraegungen.mittelfristig))
                    {
                        NutzungsdauerCBImoderate.IsSelected = true;
                    }
                    else if (transaction.Nutzungsdauer.Equals(NutzungsdauerAuspraegungen.langfristig))
                    {
                        NutzungsdauerCBIhigh.IsSelected = true;
                    }
                    else
                    {
                        NutzungsdauerCBInotSet.IsSelected = true;
                    }


                    // Ressourcenebene
                    InteroperabilityCheckBox.IsChecked = transaction.Interoberability;
                    KnowHowCheckBox.IsChecked = transaction.KnowHow;
                    EinfachheitCheckBox.IsChecked = transaction.Einfachheit;


                    // Belege die Soll-Automatisierung vor
                    //sollAutomatisierungComboBox.Items.Add(TransactionAutomationDegrees.Automate);
                    //sollAutomatisierungComboBox.Items.Add(TransactionAutomationDegrees.NotAutomate);
                    if (transaction.RequiredAutomation.Equals(TransactionAutomationDegrees.Automate))
                    {
                        //sollAutomatisierungComboBox.SelectedItem = SollAutomatisierungCBItoAutomate;
                        //sollAutomatisierungComboBox.SelectedValue = "zu automatisieren";
                        SollAutomatisierungCBItoAutomate.IsSelected = true;
                    }
                    else if (transaction.RequiredAutomation.Equals(TransactionAutomationDegrees.NotAutomate))
                    {
                        //sollAutomatisierungComboBox.SelectedItem = SollAutomatisierungCBInotToAutomate;
                        //sollAutomatisierungComboBox.SelectedValue = "nicht zu automatisieren";
                        SollAutomatisierungCBInotToAutomate.IsSelected = true;
                    }
                    else
                    {
                        // Nichts vorselektieren
                        //sollAutomatisierungComboBox.SelectedItem = SollAutomatisierungCBInotSet;
                        //sollAutomatisierungComboBox.SelectedValue = notSet;
                        SollAutomatisierungCBInotSet.IsSelected = true;
                    }

                    // Belege die Ist-Automatisierung vor
                    // istAutomatisierungComboBox
                    //transaction.ActualAutomation
                    if (transaction.ActualAutomation.Equals(TransactionAutomationDegrees.Automate))
                    {
                        //istAutomatisierungComboBox.SelectedItem = IstAutomatisierungCBIautomated;
                        //istAutomatisierungComboBox.SelectedValue = "automatisiert";
                        IstAutomatisierungCBIautomated.IsSelected = true;
                    }
                    else if (transaction.ActualAutomation.Equals(TransactionAutomationDegrees.NotAutomate))
                    {
                        //istAutomatisierungComboBox.SelectedItem = IstAutomatisierungCBInotAutomated;
                        //istAutomatisierungComboBox.SelectedValue = "nicht automatisiert";
                        IstAutomatisierungCBInotAutomated.IsSelected = true;
                    }
                    else
                    {
                        // Nichts vorselektieren
                        //istAutomatisierungComboBox.SelectedItem = IstAutomatisierungCBInotSet;
                        //istAutomatisierungComboBox.SelectedValue = notSet;
                        IstAutomatisierungCBInotSet.IsSelected = true;
                    }

                    // TODO zeige auch die zur Gruppe gehörenden Transaktionen
                    //InterAwSTAsGroupedTargetListBox
                }
            }
            //calculateHandlungsempfehlung();
        }

        private void IstAutomatisierungComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (IstAutomatisierungCBIautomated.IsSelected)
                        //if (istAutomatisierungComboBox.Text.Equals("automatisiert"))
                        {
                            transaktion.ActualAutomation = TransactionAutomationDegrees.Automate;
                        }
                        else if (IstAutomatisierungCBInotAutomated.IsSelected)
                        //else if (istAutomatisierungComboBox.Text.Equals("nicht automatisiert"))
                        {
                            transaktion.ActualAutomation = TransactionAutomationDegrees.NotAutomate;
                        }
                        else // nicht gesetzt
                        {
                            transaktion.ActualAutomation = TransactionAutomationDegrees.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void SollAutomatisierungComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (SollAutomatisierungCBItoAutomate.IsSelected)
                        //if (sollAutomatisierungComboBox.Text.Equals("zu automatisieren"))
                        {
                            transaktion.RequiredAutomation = TransactionAutomationDegrees.Automate;
                        }
                        else if (SollAutomatisierungCBInotToAutomate.IsSelected)
                        //else if (sollAutomatisierungComboBox.Text.Equals("nicht zu automatisieren"))
                        {
                            transaktion.RequiredAutomation = TransactionAutomationDegrees.NotAutomate;
                        }
                        else // nicht gesetzt
                        {
                            transaktion.RequiredAutomation = TransactionAutomationDegrees.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void StrategieCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Strategie = StrategieCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        /**
        private void VorgabeCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Vorgabe = VorgabeCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }
        */

        private void InterdependenzenCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Interdependenzen = InterdependenzenCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void NormenCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Normen = NormenCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void RechtlRahmenbedCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Rahmenbedingungen = RechtlRahmenbedCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void DatenschutzCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Datenschutz = DatenschutzCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void PeriodizitaetComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (PeriodizitaetCBIlow.IsSelected)
                        //if (PeriodizitaetCBIlow.IsSelected)
                        {
                            transaktion.Periodizitaet = MerkmaleAuspraegungen.niedrig;
                        }
                        else if (PeriodizitaetCBImoderate.IsSelected)
                        //else if (PeriodizitaetCBImoderate.IsSelected)
                        {
                            transaktion.Periodizitaet = MerkmaleAuspraegungen.moderat;
                        }
                        else if (PeriodizitaetCBIhigh.IsSelected)
                        //else if (PeriodizitaetCBIhigh.IsSelected)
                        {
                            transaktion.Periodizitaet = MerkmaleAuspraegungen.hoch;
                        }
                        else
                        {
                            transaktion.Periodizitaet = MerkmaleAuspraegungen.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void GroesseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (GroesseCBIlow.IsSelected)
                        //if (GroesseCBIlow.IsSelected)
                        {
                            transaktion.Groesse = MerkmaleAuspraegungen.niedrig;
                        }
                        else if (GroesseCBImoderate.IsSelected)
                        //else if (GroesseCBImoderate.IsSelected)
                        {
                            transaktion.Groesse = MerkmaleAuspraegungen.moderat;
                        }
                        else if (GroesseCBIhigh.IsSelected)
                        //else if (GroesseCBIhigh.IsSelected)
                        {
                            transaktion.Groesse = MerkmaleAuspraegungen.hoch;
                        }
                        else
                        {
                            transaktion.Groesse = MerkmaleAuspraegungen.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void IntensitaetComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (IntensitaetCBIlow.IsSelected)
                        //if (IntensitaetCBIlow.IsSelected)
                        {
                            transaktion.Intensitaet = MerkmaleAuspraegungen.niedrig;
                        }
                        else if (IntensitaetCBImoderate.IsSelected)
                        //else if (IntensitaetCBImoderate.IsSelected)
                        {
                            transaktion.Intensitaet = MerkmaleAuspraegungen.moderat;
                        }
                        else if (IntensitaetCBIhigh.IsSelected)
                        //else if (IntensitaetCBIhigh.IsSelected)
                        {
                            transaktion.Intensitaet = MerkmaleAuspraegungen.hoch;
                        }
                        else
                        {
                            transaktion.Intensitaet = MerkmaleAuspraegungen.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void AnpassbarkeitComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (AnpassbarkeitCBIlow.IsSelected)
                        //if (AnpassbarkeitCBIlow.IsSelected)
                        {
                            transaktion.Anpassbarkeit = MerkmaleAuspraegungen.niedrig;
                        }
                        else if (AnpassbarkeitCBImoderate.IsSelected)
                        //else if (AnpassbarkeitCBImoderate.IsSelected)
                        {
                            transaktion.Anpassbarkeit = MerkmaleAuspraegungen.moderat;
                        }
                        else if (AnpassbarkeitCBIhigh.IsSelected)
                        //else if (AnpassbarkeitCBIhigh.IsSelected)
                        {
                            transaktion.Anpassbarkeit = MerkmaleAuspraegungen.hoch;
                        }
                        else
                        {
                            transaktion.Anpassbarkeit = MerkmaleAuspraegungen.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void NutzungsdauerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (NutzungsdauerCBIlow.IsSelected)
                        //if (NutzungsdauerCBIlow.IsSelected)
                        {
                            transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.kurzfristig;
                        }
                        else if (NutzungsdauerCBImoderate.IsSelected)
                        //else if (NutzungsdauerCBImoderate.IsSelected)
                        {
                            transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.mittelfristig;
                        }
                        else if (NutzungsdauerCBIhigh.IsSelected)
                        //else if (NutzungsdauerCBIhigh.IsSelected)
                        {
                            transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.langfristig;
                        }
                        else
                        {
                            transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.nicht_gesetzt;
                        }
                    }
                    ctx.SaveChanges();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void InteroperabilityCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Interoberability = InteroperabilityCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void KnowHowCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.KnowHow = KnowHowCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void EinfachheitCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Einfachheit = EinfachheitCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
            }
        }

        private void KostenTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (InterAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(InterAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == InterAwSTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == InterAwSTAsGroupedTargetListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Kosten = Decimal.Parse(KostenTextBox.Text.ToString());
                    }
                    ctx.SaveChanges();
                }
            }
        }
        #endregion

        #region Integrationskonzepte für die Inter-AwS-Integrationen
        private void DeltaTAsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null
                && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString())
                && !DeltaTAsListBox.SelectedItem.ToString().Equals("Es besteht für keine Transaktion ein Handlungsbedarf!"))
            {
                UeberlappendeAOlistBox.Items.Clear();

                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    Transaction transaktion = transactions.FirstOrDefault();
                    SendBOlabel.Content = transaktion.SendingObject.BusinessObjectName;
                    SendAwSlabel.Content = transaktion.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                    SendPATlabel.Content = transaktion.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                    ReceivBOlabel.Content = transaktion.ReceivingObject.BusinessObjectName;
                    ReceivAwSlabel.Content = transaktion.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                    ReceivPATlabel.Content = transaktion.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;

                    // Fülle die Liste überlappender Aufgabenobjekte aller in der Gruppe enthaltenen Transaktionen
                    ArrayList aos = new ArrayList();
                    foreach (Transaction ta in transactions)
                    {
                        foreach (TaskObject item in ta.UeberlappendeAOs)
                        {
                            // Füge nur AO der Liste hinzu, die bislang noch nicht enthalten sind.
                            if (!aos.Contains(item.TaskObjectName))
                            {
                                aos.Add(item.TaskObjectName);
                            }
                            //if (!UeberlappendeAOlistBox.Items.Contains(item.TaskObjectName))
                            //{
                            //    UeberlappendeAOlistBox.Items.Add(item.TaskObjectName);
                            //}
                        }
                    }
                    aos.Sort();
                    foreach (string ao in aos)
                    {
                        UeberlappendeAOlistBox.Items.Add(ao);
                    }

                    // AIM
                    ReihenfolgeCheckBox.IsChecked = transaktion.Reihenfolgebeziehung;
                    AoGleichheitCheckBox.IsChecked = transaktion.PartielleGleichheit;
                    AoIdentityCheckBox.IsChecked = transaktion.PartielleIdentitaet;
                    lvOverlappingCheckBox.IsChecked = transaktion.UeberlappendeLoesungsverfahren;

                    // Integrationsziele
                    DatenredundanzCheckBox.IsChecked = transaktion.Datenredundanz;
                    FunktionsredundanzCheckBox.IsChecked = transaktion.Funktionsredundanz;
                    VorgangssteuerungCheckBox.IsChecked = transaktion.Vorgangssteuerung;
                    KommunikationsstrukturCheckBox.IsChecked = transaktion.Kommunikationsstrutkur;
                    IntegritaetCheckBox.IsChecked = transaktion.Integritaet;

                    getIntegrationskonzepteUndIhreAuswirkungen(transaktion);

                    /*
                    // bislang gewähltes Integrationskonzept
                    if (transaktion.Integrationskonzept.Equals(Integrationskonzept.aufgabentraegerorientiert))
                    {
                        IntegrationskonzeptCBIFktInt.IsSelected = true;
                        //integrationskonzeptComboBox.SelectedItem = "aufgabenträgerorientierte Funktionsintegration";

                        HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen.";
                        HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                        UeberlappendeLVsTextBox.Text = "wird nicht adressiert.";

                        MMK1TextBox.Text = "bleibt unverändert";
                        MMK2TextBox.Text = "bleibt unverändert; Daten müssen im empfangenden AwS erneut eingegeben werden";

                        RestriktionenTextBox.Text = "Ergebnis nach Anwendung der Merkmale: Zur Aufgabenintegration ist keine AwS-Unterstützung erforderlich; unterschiedliche Lösungsverfahren für die betroffenen Aufgaben";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenflussorientiert))
                    {
                        IntegrationskonzeptCBIDatenfluss.IsSelected = true;
                        //integrationskonzeptComboBox.SelectedItem = "datenflussorientierte Funktionsintegration";

                        HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen.";
                        HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                        UeberlappendeLVsTextBox.Text = "wird nicht adressiert.";

                        MMK1TextBox.Text = "Bleibt unverändert, da nach einer Änderung nicht mehr Attribute des AO erfasst werden als vorher";
                        MMK2TextBox.Text = "Reduziert sich auf die Kontrolle der überlappenden Attribute des AO, nicht überlappende sind dennoch zu pflegen";

                        RestriktionenTextBox.Text = "Involvierte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenintegration))
                    {
                        IntegrationskonzeptCBIDatenint.IsSelected = true;
                        //integrationskonzeptComboBox.SelectedItem = "Datenintegration";

                        HomogAOTypenTextBox.Text = "trägt zur Bildung einer einheitlichen Datrnstruktur bei den involvierten AwS";
                        HomogAOInstanzTextBox.Text = "Unterstützt die Vermeidung redundanter AO-Instanzen.";
                        UeberlappendeLVsTextBox.Text = "Trägt zur Vermeidung beim maschinellen Anteil bei, nicht aber für den personellen.";

                        MMK1TextBox.Text = "Steigt, um die Pflege des Ereignisses; der personelle Kommunikationskanal entfällt";
                        MMK2TextBox.Text = "Sinkt, nicht überlappende Attribute sind nach wie vor zu pflegen";

                        RestriktionenTextBox.Text = "Involvierte AwS werden über einen gemeinsamen Datenspeicher gekoppelt.";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.objektintegration))
                    {
                        IntegrationskonzeptCBIObjektint.IsSelected = true;
                        //integrationskonzeptComboBox.SelectedItem = "Objektintegration";

                        HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen.";
                        HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                        UeberlappendeLVsTextBox.Text = "wird nicht adressiert.";

                        MMK1TextBox.Text = "Steigt, um die Pflege des Ereignisses; der personelle Kommunikationskanal entfällt";
                        MMK2TextBox.Text = "Sinkt, nicht überlappende Attribute sind nach wie vor zu pflegen";

                        RestriktionenTextBox.Text = "Involvierte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";
                    }
                    else // nicht gesetzt
                    {
                        IntegrationskonzeptCBInotSet.IsSelected = true;
                        //integrationskonzeptComboBox.SelectedItem = "nicht gesetzt";

                        HomogAOTypenTextBox.Clear();
                        HomogAOInstanzTextBox.Clear();
                        UeberlappendeLVsTextBox.Clear();

                        MMK1TextBox.Clear();
                        MMK2TextBox.Clear();

                        RestriktionenTextBox.Clear();
                    }
                    */

                    _requiredAutomation = transaktion.RequiredAutomation;


                    //toolTip1.SetToolTip(strategieCheckBox, "Strategie hinsichtlich Adoption und Einsatz von Technologien und folglich die Auswahl der zur Verfügung stehenden Basismaschinen.");
                    //toolTip1.SetToolTip(vorgabeCheckBox, "vorgegebene AwS im Hinblick auf Standardisierung bzw. Vereinheitlichung der AwS-Landschaft unter Berücksichtigung von Ausfallsicherheit im laufenden Betrieb und Investitionsschutz sowie Restdauer des Supports und Auswirkungen auf die Freiheitsgrade bei der Wahl der Basismaschinen.");
                    //toolTip1.SetToolTip(interdependenzenCheckBox, "Einfluss von oder auf andere geplante oder gegenwärtig laufende Projekte.");
                    //toolTip1.SetToolTip(normenCheckBox, "Einhaltung von Normen");
                    //toolTip1.SetToolTip(rechtlRahmenbedCheckBox, "rechtliche Rahmenbedingungen, wie Gesetzesvorgaben");
                    //toolTip1.SetToolTip(kostenCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");

                    //toolTip1.SetToolTip(interoperabilityCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");
                    //toolTip1.SetToolTip(knowHowCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");
                    //toolTip1.SetToolTip(einfachheitCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");

                    // Blende Empfehlungen ein
                    HinweiseTextBox.Clear();
                    if (!transaktion.Strategie)
                    {
                        HinweiseTextBox.Text += "Es bestehen keine Vorgaben hinsichtlich der Adoption und dem Einsatz von Technologien.";
                    }
                    else
                    {
                        HinweiseTextBox.Text += "Es bestehen Vorgaben hinsichtlich der Adoption und dem Einsatz von Technologien.";
                    }
                    HinweiseTextBox.Text += Environment.NewLine;

                    /**
                    // AwS-Vorgaben wurden entfernt 
                    if (!transaktion.Vorgabe)
                    {
                        HinweiseTextBox.Text += "Es bestehen keine Vorgaben hinsichtlich der einzusetzenden AwS. Deshalb kann auch die Möglichkeit die involvierten AwS durch ein integriertes AwS abzulösen in Erwägung gezogen werden. Als Integrationskonzepte stünden hierfür dann die Daten- sowie die Objektintegration zur Verfügung.";
                    }
                    else
                    {
                        HinweiseTextBox.Text += "Es bestehen Vorgaben hinsichtlich der einzusetzenden AwS.";
                    }
                    HinweiseTextBox.Text += Environment.NewLine;
                    */

                    if (transaktion.Interdependenzen)
                    {
                        HinweiseTextBox.Text += "Achtung: Es bestehen Abhängigkeiten zu anderen, laufenden Projekten!";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (transaktion.Normen)
                    {
                        HinweiseTextBox.Text += "";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (transaktion.Rahmenbedingungen)
                    {
                        HinweiseTextBox.Text += "";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (!string.IsNullOrEmpty(transaktion.Kosten.ToString()))
                    {
                        HinweiseTextBox.Text += "Für die Integration bestehen Vorgaben bzgl. der Kosten. Diese dürfen nicht überschritten werden.";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (!transaktion.Interoberability)
                    {
                        HinweiseTextBox.Text += "Die involvierten AwS sind nicht interoperabel bzw. können nicht interoperabel gemacht werden. Deshalb ist eine AwS-Integration ausgeschlossen.";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }
                    if (!transaktion.KnowHow)
                    {
                        HinweiseTextBox.Text += "";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }
                    if (transaktion.Einfachheit)
                    {
                        HinweiseTextBox.Text += "";
                        HinweiseTextBox.Text += Environment.NewLine;
                    }
                }
            }
        }

        private void DatenredundanzCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Datenredundanz = DatenredundanzCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
                calculateIntegrationskonzept();
            }
        }

        private void FunktionsredundanzCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Funktionsredundanz = FunktionsredundanzCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
                calculateIntegrationskonzept();
            }
        }

        private void VorgangssteuerungCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Vorgangssteuerung = VorgangssteuerungCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
                calculateIntegrationskonzept();
            }
        }

        private void KommunikationsstrukturCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Kommunikationsstrutkur = KommunikationsstrukturCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
                calculateIntegrationskonzept();
            }
        }

        private void IntegritaetCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        transaktion.Integritaet = IntegritaetCheckBox.IsChecked.Value;
                    }
                    ctx.SaveChanges();
                }
                calculateIntegrationskonzept();
            }
        }

        private void IntegrationskonzeptComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DeltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(DeltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Select the transaction which is not connected with a group OR the transactions which are in the group of the selected transaction-group
                    List<Transaction> transactions = ctx.Transactions
                        .Where(b => b.TransactionGroup == null && b.TransactionName == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis)
                            || b.TransactionGroup != null && b.TransactionGroup == DeltaTAsListBox.SelectedItem.ToString()
                            && (b.ZugeordneteIstanalyse.AsIsAnalysisName == Analysis || b.ZugeordnetesSzenario.ScenarioName == Analysis))
                        .ToList();

                    // Proceed the changes for every selected transaction
                    foreach (Transaction transaktion in transactions)
                    {
                        if (IntegrationskonzeptCBIFktInt.IsSelected)
                        //if (integrationskonzeptComboBox.Text.Equals("aufgabenträgerorientierte Funktionsintegration"))
                        {
                            transaktion.Integrationskonzept = Integrationskonzept.aufgabentraegerorientiert;
                        }
                        else if (IntegrationskonzeptCBIDatenfluss.IsSelected)
                        //else if (integrationskonzeptComboBox.Text.Equals("datenflussorientierte Funktionsintegration"))
                        {
                            transaktion.Integrationskonzept = Integrationskonzept.datenflussorientiert;
                        }
                        else if (IntegrationskonzeptCBIDatenint.IsSelected)
                        //else if (integrationskonzeptComboBox.Text.Equals("Datenintegration"))
                        {
                            transaktion.Integrationskonzept = Integrationskonzept.datenintegration;
                        }
                        else if (IntegrationskonzeptCBIObjektint.IsSelected)
                        //else if (integrationskonzeptComboBox.Text.Equals("Objektintegration"))
                        {
                            transaktion.Integrationskonzept = Integrationskonzept.objektintegration;
                        }
                        else // nicht gesetzt
                        {
                            transaktion.Integrationskonzept = Integrationskonzept.nicht_gesetzt;
                        }

                        getIntegrationskonzepteUndIhreAuswirkungen(transaktion);

                    }
                    ctx.SaveChanges();
                }
            }
        }
        #endregion

        #region Methoden zur Generierung von Handlungsempfehlungen und Empfehlungen für das zu verwendende Integrationskonzept
        private void calculateHandlungsempfehlung()
        {
            // Wurden Werte noch nicht ausgewählt
            //string notSet = "nicht gesetzt";

            //if (string.IsNullOrEmpty(periodizitaetComboBox.Text)
            //    || string.IsNullOrEmpty(groesseComboBox.Text)
            //    || string.IsNullOrEmpty(intensitaetComboBox.Text)
            //    || string.IsNullOrEmpty(anpassbarkeitComboBox.Text)
            //    || string.IsNullOrEmpty(nutzungsdauerComboBox.Text)

            //    || periodizitaetComboBox.Text.Equals(notSet)
            //    || groesseComboBox.Text.Equals(notSet)
            //    || intensitaetComboBox.Text.Equals(notSet)
            //    || anpassbarkeitComboBox.Text.Equals(notSet)
            //    || nutzungsdauerComboBox.Text.Equals(notSet))
            if (PeriodizitaetCBInotSet.IsSelected
                || GroesseCBInotSet.IsSelected
                || IntensitaetCBInotSet.IsSelected
                || AnpassbarkeitCBInotSet.IsSelected
                || NutzungsdauerCBInotSet.IsSelected)
            {
                HandlungsempfehlungTextBox.Clear();
                HandlungsempfehlungTextBox.Text = "Kann noch keine Handlungsempfehlung erstellen, da noch nicht alle Merkmale ausgeprägt wurden.";
                return;
            }

            HandlungsempfehlungTextBox.Clear();
            HandlungsempfehlungTextBox.Text = "LEER";

            //periodizitaetComboBox.SelectedItem = transaction.Periodizitaet;
            //groesseComboBox.SelectedItem = transaction.Groesse;
            //intensitaetComboBox.SelectedItem = transaction.Intensitaet;
            //anpassbarkeitComboBox.SelectedItem = transaction.Anpassbarkeit;
            //nutzungsdauerComboBox.SelectedItem = transaction.Nutzungsdauer;

            /**
            // Regel 1
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch))
            {
                ausgabe.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            if ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) || groesse.Text.Equals(MerkmaleAuspraegungen.hoch)
                || intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) || nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.hoch))
                && anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            if (nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.niedrig)
                && (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch) == false || periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false
                || groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false || intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.niedrig)
                && (periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) || groesse.Text.Equals(MerkmaleAuspraegungen.moderat) || intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))
                && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.niedrig) && periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.moderat) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat)
                && ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) && groesse.Text.Equals(MerkmaleAuspraegungen.moderat) && intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) && groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false && intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false && groesse.Text.Equals(MerkmaleAuspraegungen.moderat) && intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.moderat) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat)
                && ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig))
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }
            */

            // Regel 1
            if (AnpassbarkeitCBIhigh.IsSelected)
            {
                HandlungsempfehlungTextBox.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            else if ((PeriodizitaetCBIhigh.IsSelected || GroesseCBIhigh.IsSelected
                || IntensitaetCBIhigh.IsSelected || NutzungsdauerCBIhigh.IsSelected)
                && AnpassbarkeitCBIhigh.IsSelected == false)
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            else if (NutzungsdauerCBIlow.IsSelected
                && (AnpassbarkeitCBIhigh.IsSelected == false || PeriodizitaetCBIhigh.IsSelected == false
                || GroesseCBIhigh.IsSelected == false || IntensitaetCBIhigh.IsSelected == false))
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            else if (AnpassbarkeitCBIlow.IsSelected
                && (PeriodizitaetCBImoderate.IsSelected || GroesseCBImoderate.IsSelected || IntensitaetCBImoderate.IsSelected)
                && NutzungsdauerCBImoderate.IsSelected)
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            else if (AnpassbarkeitCBIlow.IsSelected
                && PeriodizitaetCBIlow.IsSelected
                && GroesseCBIlow.IsSelected
                && IntensitaetCBIlow.IsSelected
                && NutzungsdauerCBImoderate.IsSelected)
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            else if (AnpassbarkeitCBImoderate.IsSelected && NutzungsdauerCBImoderate.IsSelected
                && ((PeriodizitaetCBImoderate.IsSelected && GroesseCBImoderate.IsSelected && IntensitaetCBIhigh.IsSelected == false)
                || (PeriodizitaetCBImoderate.IsSelected && GroesseCBIhigh.IsSelected == false && IntensitaetCBImoderate.IsSelected)
                || (PeriodizitaetCBIhigh.IsSelected == false && GroesseCBImoderate.IsSelected && IntensitaetCBImoderate.IsSelected)))
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            else if (AnpassbarkeitCBImoderate.IsSelected && NutzungsdauerCBImoderate.IsSelected
                && ((PeriodizitaetCBIlow.IsSelected && GroesseCBIlow.IsSelected && IntensitaetCBIhigh.IsSelected == false)
                || (PeriodizitaetCBIlow.IsSelected && GroesseCBIhigh.IsSelected == false && IntensitaetCBIlow.IsSelected)
                || (PeriodizitaetCBIhigh.IsSelected == false && GroesseCBIlow.IsSelected && IntensitaetCBIlow.IsSelected)))
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }

            }
            else
            {
                if (HandlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    HandlungsempfehlungTextBox.Text = "FEHLER: Keine Regel schlug an";
                }
                else
                {
                    HandlungsempfehlungTextBox.Text += Environment.NewLine + "FEHLER: Keine Regel schlug an";
                }
            }

        }

        private void calculateIntegrationskonzept()
        {
            // Empfehlung für das Integrationskonzept: Funktionsintegration
            if (!DatenredundanzCheckBox.IsChecked.Value
                && !FunktionsredundanzCheckBox.IsChecked.Value
                && !IntegritaetCheckBox.IsChecked.Value
                && (VorgangssteuerungCheckBox.IsChecked.Value
                    || KommunikationsstrukturCheckBox.IsChecked.Value)
                && !SendAwSlabel.Content.Equals(ReceivAwSlabel.Content))
            {
                IntegrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich die Integrationskonzepte datenflussorientierte Funktions- und Objektintegration an.";
                if (AoIdentityCheckBox.IsChecked.Value)
                {
                    IntegrationskonzeptEmpfehlungTextBox.Text += " Bei Implementierung des Integrationskonzepts datenflussorientierte Funktionsintegration sind zur Beherrschung der Datenredundanz Behelfslösungen vorzusehen (bspw. Festlegung des führenden AwS).";
                }
            }
            else if (!FunktionsredundanzCheckBox.IsChecked.Value
                && !VorgangssteuerungCheckBox.IsChecked.Value
                && !KommunikationsstrukturCheckBox.IsChecked.Value
                && (DatenredundanzCheckBox.IsChecked.Value
                    || IntegritaetCheckBox.IsChecked.Value)
                && !SendAwSlabel.Content.Equals(ReceivAwSlabel.Content))
            {
                IntegrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich die Integrationskonzepte Datenintegration sowie Objektintegration an.";
            }
            else if (SendAwSlabel.Content.Equals(ReceivAwSlabel.Content))
            {
                IntegrationskonzeptEmpfehlungTextBox.Text = "Es ist ein Integrationskonzept für eine Intra-AwS-Lösung zu implementieren. Dies wird an dieser Stelle nicht weiter betrachtet.";
            }
            else if (!DatenredundanzCheckBox.IsChecked.Value
                && !FunktionsredundanzCheckBox.IsChecked.Value
                && !VorgangssteuerungCheckBox.IsChecked.Value
                && !KommunikationsstrukturCheckBox.IsChecked.Value
                && !IntegritaetCheckBox.IsChecked.Value)
            {
                IntegrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich alle Integrationskonzepte an.";
            }
            else
            {
                IntegrationskonzeptEmpfehlungTextBox.Text = "Es bietet sich das Integrationskonzept Objektintegration an.";
            }

            if (SendPATlabel.Content.Equals(ReceivPATlabel.Content)) // && !_requiredAutomation.Equals(TransactionAutomationDegrees.Automate))
            {
                IntegrationskonzeptEmpfehlungTextBox.Text += " Unter Ausschluss der Implementierung einer maschinellen Unterstützung der Integration ist auch die aufgabenträgerorientierte Funktionsintegration möglich.";
            }
        }

        private void getIntegrationskonzepteUndIhreAuswirkungen(Transaction transaktion)
        {
            // die Anzeige muss noch dynamisiert werden -> wird durch getIntegrationskonzepteUndIhreAuswirkungen nun dynamisiert
            // empfehlungTextBox.Text = "Maschinelle Datenübertragung zwischen vorhandenen AwS genügt; personelle Vorgangsauslösung";
            /*
            HomogAOTypenTextBox.Text = "Wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen.";
            HomogAOInstanzTextBox.Text = "Lösung erzeugt redundante AO-Instanzen.";
            UeberlappendeLVsTextBox.Text = "Wird nicht adressiert.";

            MMK1TextBox.Text = "Bleibt unverändert, da nach einer Änderung nicht mehr Attribute des AO erfasst werden als vorher.";
            MMK2TextBox.Text = "Reduziert sich auf die Kontrolle der überlappenden Attribute des AO; nicht überlappende sind dennoch zu pflegen";

            RestriktionenTextBox.Text = "Beteiligte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";
            */

            // bislang gewähltes Integrationskonzept
            if (transaktion.Integrationskonzept.Equals(Integrationskonzept.aufgabentraegerorientiert))
            {
                IntegrationskonzeptCBIFktInt.IsSelected = true;
                //integrationskonzeptComboBox.SelectedItem = "aufgabenträgerorientierte Funktionsintegration";

                HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen";
                HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                UeberlappendeLVsTextBox.Text = "wird nicht adressiert";

                MMK1TextBox.Text = "bleibt unverändert";
                MMK2TextBox.Text = "bleibt unverändert; Daten müssen im empfangenden AwS erneut eingegeben werden";

                RestriktionenTextBox.Text = "Ergebnis nach Anwendung der Merkmale: Zur Aufgabenintegration ist keine AwS-Unterstützung erforderlich; unterschiedliche Lösungsverfahren für die betroffenen Aufgaben";
            }
            else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenflussorientiert))
            {
                IntegrationskonzeptCBIDatenfluss.IsSelected = true;
                //integrationskonzeptComboBox.SelectedItem = "datenflussorientierte Funktionsintegration";

                HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen";
                HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                UeberlappendeLVsTextBox.Text = "wird nicht adressiert";

                MMK1TextBox.Text = "Bleibt unverändert, da nach einer Änderung nicht mehr Attribute des AO erfasst werden als vorher.";
                MMK2TextBox.Text = "Sinkt, nicht überlappende Attribute sind nach wie vor zu pflegen.";

                RestriktionenTextBox.Text = "Involvierte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";
            }
            else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenintegration))
            {
                IntegrationskonzeptCBIDatenint.IsSelected = true;
                //integrationskonzeptComboBox.SelectedItem = "Datenintegration";

                HomogAOTypenTextBox.Text = "trägt zur Bildung einer einheitlichen Datrnstruktur bei den involvierten AwS bei";
                HomogAOInstanzTextBox.Text = "Unterstützt die Vermeidung redundanter AO-Instanzen.";
                UeberlappendeLVsTextBox.Text = "wird nicht adressiert";

                MMK1TextBox.Text = "Steigt, um die Pflege des Ereignisses; personeller Kommunikationskanal entfällt";
                MMK2TextBox.Text = "Sinkt, nicht überlappende Attribute sind nach wie vor zu pflegen.";

                RestriktionenTextBox.Text = "Involvierte AwS werden über einen gemeinsamen Datenspeicher gekoppelt.";
            }
            else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.objektintegration))
            {
                IntegrationskonzeptCBIObjektint.IsSelected = true;
                //integrationskonzeptComboBox.SelectedItem = "Objektintegration";

                HomogAOTypenTextBox.Text = "wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen";
                HomogAOInstanzTextBox.Text = "Lösung kann redundante AO-Instanzen erzeugen.";
                UeberlappendeLVsTextBox.Text = "wird nicht adressiert";

                MMK1TextBox.Text = "Steigt, um die Pflege des Ereignisses; personeller Kommunikationskanal entfällt";
                MMK2TextBox.Text = "Sinkt, nicht überlappende Attribute sind nach wie vor zu pflegen.";

                RestriktionenTextBox.Text = "Involvierte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";
            }
            else // nicht gesetzt
            {
                IntegrationskonzeptCBInotSet.IsSelected = true;
                //integrationskonzeptComboBox.SelectedItem = "nicht gesetzt";

                HomogAOTypenTextBox.Clear();
                HomogAOInstanzTextBox.Clear();
                UeberlappendeLVsTextBox.Clear();

                MMK1TextBox.Clear();
                MMK2TextBox.Clear();

                RestriktionenTextBox.Clear();
            }
        }
        #endregion

    }
}
