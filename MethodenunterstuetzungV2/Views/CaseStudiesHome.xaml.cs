﻿using MethodenunterstuetzungV2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MethodenunterstuetzungV2.Views
{
    /// <summary>
    /// Interaktionslogik für CaseStudiesHome.xaml
    /// </summary>
    public partial class CaseStudiesHome : Page
    {
        public CaseStudiesHome()
        {
            InitializeComponent();

            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                // Query for the Blog named ADO.NET Blog
                //var mandant = ctx.Mandanten
                //                .Where(b => b.MandantName == textBox1.Text)
                //                .FirstOrDefault();

                // Query for all blogs with names starting with B
                var caseStudies = from b in ctx.CaseStudies
                                    //where b.MandantName.StartsWith("B")
                                select b;
                foreach (CaseStudy cases in caseStudies)
                {
                    caseStudyListBox.Items.Add(cases.CaseStudyName);
                }

                if (!caseStudyListBox.Items.IsEmpty)
                {
                    caseStudyListBox.SelectedIndex = 0;
                }
            }

            //toolStripStatusLabel1.Text = "Momentan ist Fallstudie " + Settings.Default.Mandant + " ausgewählt.";
        }

        private void OpenCaseStudyButton_Click(object sender, RoutedEventArgs e)
        {
            if (caseStudyListBox.SelectedItem != null && !string.IsNullOrEmpty(caseStudyListBox.SelectedItem.ToString()))
            {
                Settings.Default.FallstudieName = caseStudyListBox.SelectedItem.ToString();
                Settings.Default.Save();

                // Edit selected Case Study
                //EditCaseStudyPage caseStudyPage = new EditCaseStudyPage(caseStudyListBox.SelectedItem.ToString());
                EditCaseStudyPage caseStudyPage = new EditCaseStudyPage();
                NavigationService.Navigate(caseStudyPage);

                //toolStripStatusLabel1.Text = "Es wurde die Fallstudie " + Settings.Default.Mandant + " ausgewählt.";
            }
        }

        private void DeleteCaseStudyButton_Click(object sender, RoutedEventArgs e)
        {
            if (caseStudyListBox.SelectedItem != null && !string.IsNullOrEmpty(caseStudyListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    CaseStudy caseStudy = ctx.CaseStudies
                        .Where(b => b.CaseStudyName == caseStudyListBox.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.CaseStudies.Remove(caseStudy);
                    ctx.SaveChanges();
                }

                caseStudyListBox.Items.Remove(caseStudyListBox.SelectedItem);
            }
        }

        private void AddCaseStudyButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(newCaseStudyTextBox.Text))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    CaseStudy caseStudy = new CaseStudy() { CaseStudyName = newCaseStudyTextBox.Text };
                    ctx.CaseStudies.Add(caseStudy);
                    ctx.SaveChanges();
                }
                caseStudyListBox.Items.Add(newCaseStudyTextBox.Text);
            }

            newCaseStudyTextBox.Clear();
        }

    }
}