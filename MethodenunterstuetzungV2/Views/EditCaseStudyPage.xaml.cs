﻿using MethodenunterstuetzungV2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MethodenunterstuetzungV2.Views
{
    /// <summary>
    /// Interaktionslogik für EditCaseStudyPage.xaml
    /// </summary>
    public partial class EditCaseStudyPage : Page
    {
        public EditCaseStudyPage()
        {
            InitializeComponent();

            Title = "Fallstudie - " + Settings.Default.FallstudieName;
            // Bind to expense report data.
            //this.DataContext = data;
            //this.caseStudyLabel.Content = "Bearbeite Fallstudie: " + data;
            caseStudyLabel.Content = "Bearbeite Fallstudie: " + Settings.Default.FallstudieName;

            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                // Query for all blogs with names starting with B
                var scenarios = from b in ctx.Scenarios
                                where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                select b;
                foreach (Scenario scenario in scenarios)
                {
                    scenariosListBox.Items.Add(scenario.ScenarioName);
                }

                if (!scenariosListBox.Items.IsEmpty)
                {
                    scenariosListBox.SelectedIndex = 0;
                }
            }
        }

        private void AsIsAnalysisButton_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                // Query for the Blog named ADO.NET Blog
                AsIsAnalysis asIsAnalysis = ctx.AsIsAnalysis
                    .Where(b => b.AsIsAnalysisName == Settings.Default.FallstudieName)
                    .FirstOrDefault();

                // If an as-is analysis doesn't exist, create one for the case study
                if (asIsAnalysis == null)
                {
                    asIsAnalysis = new AsIsAnalysis() { AsIsAnalysisName = Settings.Default.FallstudieName };

                    // Verkette die Istanalyse mit der Fallstudie
                    CaseStudy caseStudy = ctx.CaseStudies
                        .Where(b => b.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();
                    caseStudy.Istanalyse = asIsAnalysis;
                    asIsAnalysis.ZugeordneteFallstudie = caseStudy;

                    ctx.AsIsAnalysis.Add(asIsAnalysis);
                    ctx.SaveChanges();
                }

                // Edit the as-is analysis of the examined Case Study
                EditAnalysisPage asIsAnalysisPage = new EditAnalysisPage(asIsAnalysis.AsIsAnalysisName, false);
                NavigationService.Navigate(asIsAnalysisPage);
            }
        }

        private void EditScenarioButton_Click(object sender, RoutedEventArgs e)
        {
            if (scenariosListBox.SelectedItem != null && !string.IsNullOrEmpty(scenariosListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Scenario scenario = ctx.Scenarios
                        .Where(b => b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                            && b.ScenarioName == scenariosListBox.SelectedItem.ToString())
                        .FirstOrDefault();

                    // Edit selected Case Study
                    EditAnalysisPage asIsAnalysisPage = new EditAnalysisPage(scenario.ScenarioName, true);
                    NavigationService.Navigate(asIsAnalysisPage);
                }
            }
        }

        private void AddScenarioButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(newScenarioTextBox.Text))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    Scenario scenario = new Scenario() { ScenarioName = newScenarioTextBox.Text };

                    // Verkette das Szenario mit der Fallstudie
                    CaseStudy caseStudy = ctx.CaseStudies
                        .Where(b => b.CaseStudyName == Settings.Default.FallstudieName)
                        .FirstOrDefault();
                    caseStudy.Szenarien.Add(scenario);
                    scenario.ZugeordneteFallstudie = caseStudy;

                    ctx.Scenarios.Add(scenario);
                    ctx.SaveChanges();
                }
                scenariosListBox.Items.Add(newScenarioTextBox.Text);
            }

            newScenarioTextBox.Clear();
        }

        private void DeleteScenarioButton_Click(object sender, RoutedEventArgs e)
        {
            if (scenariosListBox.SelectedItem != null && !string.IsNullOrEmpty(scenariosListBox.SelectedItem.ToString()))
            {
                using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Scenario scenario = ctx.Scenarios
                        .Where(b => b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                            && b.ScenarioName == scenariosListBox.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.Scenarios.Remove(scenario);
                    ctx.SaveChanges();
                }

                scenariosListBox.Items.Remove(scenariosListBox.SelectedItem);
            }
        }

        private void EvaluationButton_Click(object sender, RoutedEventArgs e)
        {
            // Großes TODO!
            // Evaluate the examined Case Study
            EvaluationPage evaluationPage = new EvaluationPage();
            NavigationService.Navigate(evaluationPage);
        }

    }
}
