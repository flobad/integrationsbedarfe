﻿using MethodenunterstuetzungV2.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MethodenunterstuetzungV2.Views
{
    /// <summary>
    /// Interaktionslogik für EvaluationPage.xaml
    /// </summary>
    public partial class EvaluationPage : Page
    {
        public EvaluationPage()
        {
            InitializeComponent();

            Title = "Auswertung der Fallstudie " + Settings.Default.FallstudieName;

            using (var ctx = new MethodenunterstuetzungV2EntityDataModel())
            {
                // Bereich: Istzustand
                // Binde das IAS für den Istzustand ein
                //string linkZumModellIst = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace("ü", "ue").Replace(" ", "_") + "/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/22_IAS.png");
                string linkZumModellIst = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/22_IAS.png");
                image.Source = new BitmapImage(new Uri(linkZumModellIst));

                // Bereite die Inhalte für die List-Box mit den gruppierten Transaktionen vor
                CollectionOfStrings = new ObservableCollection<string>();
                ArrayList asIsTas = new ArrayList();

                var transactionsWithoutGroupsAsIs = from b in ctx.Transactions
                                                    //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                                where b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                                                  && b.SendingObject.ZugeordnetesAwS != b.ReceivingObject.ZugeordnetesAwS
                                                  && b.ZugeordneteIstanalyse.AsIsAnalysisName == Settings.Default.FallstudieName
                                                  && b.TransactionGroup == null
                                                orderby b.TransactionName
                                                select b;
                foreach (Transaction ta in transactionsWithoutGroupsAsIs)
                {
                    //TasListBoxAsIs.Items.Add(ta.TransactionName);
                    CollectionOfStrings.Add(ta.TransactionName);
                    asIsTas.Add(ta.TransactionName);
                }

                // oder not null und dann aber das Erste Element
                var transactionsWithGroupsAsIs = ctx.Transactions
                    .Where(b => b.SendingObject.ZugeordnetesAwS != null && b.ReceivingObject.ZugeordnetesAwS != null
                        && b.SendingObject.ZugeordnetesAwS != b.ReceivingObject.ZugeordnetesAwS
                        && b.ZugeordneteIstanalyse.AsIsAnalysisName == Settings.Default.FallstudieName
                        && b.TransactionGroup != null)
                    .OrderBy(b => b.TransactionName)
                    .GroupBy(b => b.TransactionGroup)
                    .ToList();
                //.FirstOrDefault();
                foreach (var group in transactionsWithGroupsAsIs)
                {
                    //TasListBoxAsIs.Items.Add(group.First().TransactionGroup);
                    CollectionOfStrings.Add(group.First().TransactionGroup);
                    asIsTas.Add(group.First().TransactionGroup);
                }
                // Sortierung der Liste (Quelle: https://stackoverflow.com/questions/19112922/sort-observablecollectionstring-c-sharp) -> müsste mittels MVVM mit der GUI verknüpft werden (weitere Quelle hierzu: https://stackoverflow.com/questions/1280704/how-can-i-sort-a-listbox-using-only-xaml-and-no-code-behind)
                CollectionOfStrings = new ObservableCollection<string>(CollectionOfStrings.OrderBy(i => i));
                asIsTas.Sort();
                // Fülle die List-Box mit den gruppierten und sortierten Transaktionen
                TasListBoxAsIs.Items.Clear();
                foreach (string i in asIsTas)
                {
                    TasListBoxAsIs.Items.Add(i);
                }



                // Bereich: Sollkonzept (Szenario)
                // Query for the Blog named ADO.NET Blog
                Scenario scenario = ctx.Scenarios
                    .Where(b => b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName)
                    //&& b.ScenarioName == scenariosListBox.SelectedItem.ToString())
                    .FirstOrDefault();

                // Binde das IAS für das Sollkonzept ein
                //string linkZumModellSzenario = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace("ü", "ue").Replace(" ", "_") + "/" + scenario.ScenarioName.Replace(" ", "_") + "/22_IAS.png");
                string linkZumModellSzenario = string.Format(@"https://orckmmwkpvbjwzbl.myfritz.net/diss-pics/" + Settings.Default.FallstudieName.Replace(" ", "_") + "/" + scenario.ScenarioName.Replace(" ", "_") + "/22_IAS.png");
                imageTarget.Source = new BitmapImage(new Uri(linkZumModellSzenario));
                Szenario.Content = "Szenario: " + scenario.ScenarioName;

                // Bereite die Inhalte für die List-Box mit den gruppierten Transaktionen vor
                ArrayList targetTas = new ArrayList();
                Implementierungskosten.Text = "0";
                var transactionsWithoutGroups = from b in ctx.Transactions
                                                    //where b.ZugeordneteFallstudie.CaseStudyName == Settings.Default.FallstudieName
                                                where b.RequiredAutomation != b.ActualAutomation
                                                  && (b.ZugeordneteIstanalyse.AsIsAnalysisName == scenario.ScenarioName || b.ZugeordnetesSzenario.ScenarioName == scenario.ScenarioName)
                                                  && b.TransactionGroup == null
                                                orderby b.TransactionName
                                                select b;
                foreach (Transaction ta in transactionsWithoutGroups)
                {
                    targetTas.Add(ta.TransactionName);
                    Implementierungskosten.Text = Decimal.Add(Decimal.Parse(Implementierungskosten.Text.ToString()), ta.Kosten).ToString();
                }

                // oder not null und dann aber das Erste Element
                var transactionsWithGroups = ctx.Transactions
                    .Where(b => b.RequiredAutomation != b.ActualAutomation
                        && (b.ZugeordneteIstanalyse.AsIsAnalysisName == scenario.ScenarioName || b.ZugeordnetesSzenario.ScenarioName == scenario.ScenarioName)
                        && b.TransactionGroup != null)
                    .OrderBy(b => b.TransactionName)
                    .GroupBy(b => b.TransactionGroup)
                    .ToList();
                //.FirstOrDefault();
                foreach (var group in transactionsWithGroups)
                {
                    targetTas.Add(group.First().TransactionGroup);
                    Implementierungskosten.Text = Decimal.Add(Decimal.Parse(Implementierungskosten.Text.ToString()), group.First().Kosten).ToString();
                }
                targetTas.Sort();
                // Fülle die List-Box mit den gruppierten und sortierten Transaktionen
                TasListBoxTarget.Items.Clear();
                foreach (string i in targetTas)
                {
                    TasListBoxTarget.Items.Add(i);
                }
            }
        }

        public ObservableCollection<string> CollectionOfStrings { get; private set; }

        // Custom constructor to pass expense report data
        public EvaluationPage(string data) : this()
        {
            Title = "Auswertung der Fallstudie " + Settings.Default.FallstudieName;
        }
    }
}
