﻿namespace MethodenunterstuetzungV2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class MethodenunterstuetzungV2EntityDataModel : DbContext
    {
        // Der Kontext wurde für die Verwendung einer MethodenunterstuetzungV2EntityDataModel-Verbindungszeichenfolge aus der 
        // Konfigurationsdatei ('App.config' oder 'Web.config') der Anwendung konfiguriert. Diese Verbindungszeichenfolge hat standardmäßig die 
        // Datenbank 'MethodenunterstuetzungV2.MethodenunterstuetzungV2EntityDataModel' auf der LocalDb-Instanz als Ziel. 
        // 
        // Wenn Sie eine andere Datenbank und/oder einen anderen Anbieter als Ziel verwenden möchten, ändern Sie die MethodenunterstuetzungV2EntityDataModel-Zeichenfolge 
        // in der Anwendungskonfigurationsdatei.
        public MethodenunterstuetzungV2EntityDataModel()
            : base("name=MethodenunterstuetzungV2EntityDataModel")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MethodenunterstuetzungV2EntityDataModel, Migrations.Configuration>("MethodenunterstuetzungV2EntityDataModel"));
            //Database.SetInitializer(new DropCreateDatabaseAlways<MethodenunterstuetzungV2EntityDataModel>());
        }

        // Fügen Sie ein 'DbSet' für jeden Entitätstyp hinzu, den Sie in das Modell einschließen möchten. Weitere Informationen 
        // zum Konfigurieren und Verwenden eines Code First-Modells finden Sie unter 'http://go.microsoft.com/fwlink/?LinkId=390109'.

        //public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<CaseStudy> CaseStudies { get; set; }
        public virtual DbSet<AsIsAnalysis> AsIsAnalysis { get; set; }
        public virtual DbSet<Scenario> Scenarios { get; set; }
        public virtual DbSet<BusinessObject> BusinessObjects { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TaskObject> TaskObjects { get; set; }
        public virtual DbSet<Anwendungssystem> Anwendungssysteme { get; set; }
        public virtual DbSet<PersonellerAufgabentraeger> PersonelleAufgabentraeger { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Configure One-to-One relationship using Fluent API (Quelle: http://www.entityframeworktutorial.net/code-first/configure-one-to-one-relationship-in-code-first.aspx)
            // Erforderlich, um die InvalidOperationException zu umgehen
            // Configure CaseStudyName as PK for AsIsAnalysis
            modelBuilder.Entity<AsIsAnalysis>()
                .HasKey(e => e.AsIsAnalysisName);

            // Configure CaseStudyId as FK for AsIsAnalysis
            modelBuilder.Entity<CaseStudy>()
                .HasRequired(s => s.Istanalyse)
                .WithRequiredPrincipal(ad => ad.ZugeordneteFallstudie);
        }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class CaseStudy
    {
        /// <summary>
        /// Default minimal constructor for a case study.
        /// </summary>
        public CaseStudy() { }

        /// <summary>
        /// Public constructor for a case study.
        /// </summary>
        /// <param name="name"></param>
        public CaseStudy(string name) { this.CaseStudyName = name; }

        /// <summary>
        /// Bezeichnung der Fallstudie
        /// </summary>
        [Key]
        public string CaseStudyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual AsIsAnalysis Istanalyse { get; set; }
        public virtual ICollection<Scenario> Szenarien { get; set; }
    }

    public class AsIsAnalysis
    {
        public AsIsAnalysis() { }

        /// <summary>
        /// Public constructor for a as-is analysis.
        /// </summary>
        /// <param name="name"></param>
        public AsIsAnalysis(string name) { this.AsIsAnalysisName = name; }

        /// <summary>
        /// Bezeichnung der Istanalyse
        /// </summary>
        [Key]
        public string AsIsAnalysisName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<BusinessObject> BusinessObjects { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

        public virtual CaseStudy ZugeordneteFallstudie { get; set; }
    }

    public class Scenario
    {
        public Scenario() { }

        /// <summary>
        /// Public constructor for a scenario.
        /// </summary>
        /// <param name="name"></param>
        public Scenario(string name) { this.ScenarioName = name; }

        /// <summary>
        /// Bezeichnung des Szenarios
        /// </summary>
        [Key]
        public string ScenarioName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<BusinessObject> BusinessObjects { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

        public virtual CaseStudy ZugeordneteFallstudie { get; set; }
    }

    public class BusinessObject
    {
        /// <summary>
        /// Default minimal constructor for characteristic.
        /// </summary>
        public BusinessObject() { }

        /// <summary>
        /// Public constructor for a business object.
        /// </summary>
        /// <param name="name"></param>
        public BusinessObject(string name) { this.BusinessObjectName = name; }

        /// <summary>
        /// Bezeichnung des betrieblichen Objekts
        /// </summary>
        [Required] // [Key]
        public string BusinessObjectName { get; set; }

        [Key]
        public int BusinessObjectKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Anwendungssystem ZugeordnetesAwS { get; set; }
        public virtual PersonellerAufgabentraeger ZugeordneteStelle { get; set; }
        public virtual AsIsAnalysis ZugeordneteIstanalyse { get; set; }
        public virtual Scenario ZugeordnetesSzenario { get; set; }
        //public virtual CaseStudy ZugeordneteFallstudie { get; set; }
        public virtual ICollection<Transaction> ZugeordneteTAs { get; set; }
    }

    public class Transaction
    {
        /// <summary>
        /// Bezeichnung der Transaktion
        /// </summary>
        [Required] // [Key]
        public string TransactionName { get; set; }

        [Key]
        public int TransactionKey { get; set; }

        public string TransactionGroup { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public TransactionAutomationDegrees RequiredAutomation { get; set; }

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public TransactionAutomationDegrees ActualAutomation { get; set; }

        /// <summary>
        /// Sendendes betriebliches Objekt
        /// </summary>
        public virtual BusinessObject SendingObject { get; set; }

        /// <summary>
        /// Empfangendes betriebliches Objekt
        /// </summary>
        public virtual BusinessObject ReceivingObject { get; set; }

        public virtual AsIsAnalysis ZugeordneteIstanalyse { get; set; }
        public virtual Scenario ZugeordnetesSzenario { get; set; }
        //public virtual CaseStudy ZugeordneteFallstudie { get; set; }

        public virtual ICollection<TaskObject> UeberlappendeAOs { get; set; }

        // Identifizierte Aufgabenintegrationsmuster
        //public virtual TaskObject IdentifizierteAufgabenintegrationsmuster { get; set; }
        public bool Reihenfolgebeziehung { get; set; }
        public bool PartielleGleichheit { get; set; }
        public bool PartielleIdentitaet { get; set; }
        public bool UeberlappendeLoesungsverfahren { get; set; }


        // Unternehmensplan: Strategie; Vorgabe; Interdependenzen; Normen; rechtl. Rahmenbedingungen; Datenschutz; Kosten
        public bool Strategie { get; set; }
        public bool Vorgabe { get; set; }
        public bool Interdependenzen { get; set; }
        public bool Normen { get; set; }
        public bool Rahmenbedingungen { get; set; }
        public bool Datenschutz { get; set; }
        public Decimal Kosten { get; set; }

        // Geschäfsprozessmodell: Merkmale
        public MerkmaleAuspraegungen Periodizitaet { get; set; }
        public MerkmaleAuspraegungen Groesse { get; set; }
        public MerkmaleAuspraegungen Intensitaet { get; set; }
        public MerkmaleAuspraegungen Anpassbarkeit { get; set; }
        public NutzungsdauerAuspraegungen Nutzungsdauer { get; set; }

        // Interoperabilität; Know-how; Einfachheit
        public bool Interoberability { get; set; }
        public bool KnowHow { get; set; }
        public bool Einfachheit { get; set; }

        // Integrationsziele
        public bool Datenredundanz { get; set; }
        public bool Funktionsredundanz { get; set; }
        public bool Vorgangssteuerung { get; set; }
        public bool Kommunikationsstrutkur { get; set; }
        public bool Integritaet { get; set; }

        public Integrationskonzept Integrationskonzept { get; set; }
    }

    public enum Integrationskonzept
    {
        nicht_gesetzt, aufgabentraegerorientiert, datenflussorientiert, datenintegration, objektintegration
    }

    public enum MerkmaleAuspraegungen
    {
        nicht_gesetzt, niedrig, moderat, hoch
    }

    public enum NutzungsdauerAuspraegungen
    {
        nicht_gesetzt, kurzfristig, mittelfristig, langfristig
    }

    public enum TransactionAutomationDegrees
    {
        nicht_gesetzt, Automate, NotAutomate
    }

    public class TaskObject
    {
        /// <summary>
        /// Bezeichnung des Aufgabenobjekts
        /// </summary>
        [Required]
        public string TaskObjectName { get; set; }

        [Key]
        public int TaskObjectKey { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        //public enum RequiredAutomation { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        //public enum ActualAutomation { automated, notAutomated };

        public virtual AsIsAnalysis ZugeordneteIstanalyse { get; set; }
        public virtual Scenario ZugeordnetesSzenario { get; set; }
        //public virtual CaseStudy ZugeordneteFallstudie { get; set; }
        public virtual ICollection<Transaction> ZugeordneteTransaktionen { get; set; }
    }

    public class Aufgabentraeger
    {
        [Required]
        public string AufgabentraegerName { get; set; }
        [Key]
        public int AufgabentraegerKey { get; set; }
        public virtual ICollection<BusinessObject> UnterstuetzendeBOs { get; set; }
        public virtual CaseStudy ZugeordneteFallstudie { get; set; }
    }

    public class Anwendungssystem : Aufgabentraeger
    {
    }

    public class PersonellerAufgabentraeger : Aufgabentraeger
    {
        public PersonellerAufgabentraeger() { }
        public PersonellerAufgabentraeger(string name) { this.AufgabentraegerName = name; }
    }

}