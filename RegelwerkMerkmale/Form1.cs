﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegelwerkMerkmale
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ausgabe.Text = "LEER";

            /**
            // Regel 1
            if (anpassbarkeit.Text.Equals("hoch"))
            {
                ausgabe.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            if ((periodizitaet.Text.Equals("hoch") || groesse.Text.Equals("hoch")
                || intensitaet.Text.Equals("hoch") || nutzungsdauer.Text.Equals("hoch"))
                && anpassbarkeit.Text.Equals("hoch") == false)
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            if (nutzungsdauer.Text.Equals("niedrig")
                && (anpassbarkeit.Text.Equals("hoch") == false || periodizitaet.Text.Equals("hoch") == false
                || groesse.Text.Equals("hoch") == false || intensitaet.Text.Equals("hoch") == false))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            if (anpassbarkeit.Text.Equals("niedrig")
                && (periodizitaet.Text.Equals("moderat") || groesse.Text.Equals("moderat") || intensitaet.Text.Equals("moderat"))
                && nutzungsdauer.Text.Equals("moderat"))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            if (anpassbarkeit.Text.Equals("niedrig") && periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("niedrig") && nutzungsdauer.Text.Equals("moderat"))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            if (anpassbarkeit.Text.Equals("moderat") && nutzungsdauer.Text.Equals("moderat")
                && ((periodizitaet.Text.Equals("moderat") && groesse.Text.Equals("moderat") && intensitaet.Text.Equals("hoch") == false)
                || (periodizitaet.Text.Equals("moderat") && groesse.Text.Equals("hoch") == false && intensitaet.Text.Equals("moderat"))
                || (periodizitaet.Text.Equals("hoch") == false && groesse.Text.Equals("moderat") && intensitaet.Text.Equals("moderat"))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            if (anpassbarkeit.Text.Equals("moderat") && nutzungsdauer.Text.Equals("moderat")
                && ((periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("hoch") == false)
                || (periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("hoch") == false && intensitaet.Text.Equals("niedrig"))
                || (periodizitaet.Text.Equals("hoch") == false && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("niedrig"))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }
            */

            // Regel 1
            if (anpassbarkeit.Text.Equals("hoch"))
            {
                ausgabe.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            else if ((periodizitaet.Text.Equals("hoch") || groesse.Text.Equals("hoch")
                || intensitaet.Text.Equals("hoch") || nutzungsdauer.Text.Equals("hoch"))
                && anpassbarkeit.Text.Equals("hoch") == false)
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            else if (nutzungsdauer.Text.Equals("niedrig")
                && (anpassbarkeit.Text.Equals("hoch") == false || periodizitaet.Text.Equals("hoch") == false
                || groesse.Text.Equals("hoch") == false || intensitaet.Text.Equals("hoch") == false))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            else if (anpassbarkeit.Text.Equals("niedrig")
                && (periodizitaet.Text.Equals("moderat") || groesse.Text.Equals("moderat") || intensitaet.Text.Equals("moderat"))
                && nutzungsdauer.Text.Equals("moderat"))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            else if (anpassbarkeit.Text.Equals("niedrig") && periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("niedrig") && nutzungsdauer.Text.Equals("moderat"))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            else if (anpassbarkeit.Text.Equals("moderat") && nutzungsdauer.Text.Equals("moderat")
                && ((periodizitaet.Text.Equals("moderat") && groesse.Text.Equals("moderat") && intensitaet.Text.Equals("hoch") == false)
                || (periodizitaet.Text.Equals("moderat") && groesse.Text.Equals("hoch") == false && intensitaet.Text.Equals("moderat"))
                || (periodizitaet.Text.Equals("hoch") == false && groesse.Text.Equals("moderat") && intensitaet.Text.Equals("moderat"))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            else if (anpassbarkeit.Text.Equals("moderat") && nutzungsdauer.Text.Equals("moderat")
                && ((periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("hoch") == false)
                || (periodizitaet.Text.Equals("niedrig") && groesse.Text.Equals("hoch") == false && intensitaet.Text.Equals("niedrig"))
                || (periodizitaet.Text.Equals("hoch") == false && groesse.Text.Equals("niedrig") && intensitaet.Text.Equals("niedrig"))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }

            }
            else
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "FEHLER: Keine Regel schlug an";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "FEHLER: Keine Regel schlug an";
                }
            }

        }
    }
}
