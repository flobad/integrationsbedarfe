﻿namespace RegelwerkMerkmale
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ausgabe = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.periodizitaet = new System.Windows.Forms.ComboBox();
            this.nutzungsdauer = new System.Windows.Forms.ComboBox();
            this.anpassbarkeit = new System.Windows.Forms.ComboBox();
            this.intensitaet = new System.Windows.Forms.ComboBox();
            this.groesse = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Periodizität des GP";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(146, 212);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 41);
            this.button1.TabIndex = 6;
            this.button1.Text = "Berechne Handlungsempfehlung";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ausgabe
            // 
            this.ausgabe.ForeColor = System.Drawing.Color.DarkGreen;
            this.ausgabe.Location = new System.Drawing.Point(12, 259);
            this.ausgabe.Multiline = true;
            this.ausgabe.Name = "ausgabe";
            this.ausgabe.ReadOnly = true;
            this.ausgabe.Size = new System.Drawing.Size(390, 68);
            this.ausgabe.TabIndex = 0;
            this.ausgabe.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Größe einer zu übertragenden AO-Instanz";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Intensität bei der Durchführung einer Transaktion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Anpassbarkeit der Transaktion";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(71, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(186, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nutzungsdauer der Integrationslösung";
            // 
            // periodizitaet
            // 
            this.periodizitaet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.periodizitaet.FormattingEnabled = true;
            this.periodizitaet.Items.AddRange(new object[] {
            "niedrig",
            "moderat",
            "hoch"});
            this.periodizitaet.Location = new System.Drawing.Point(263, 19);
            this.periodizitaet.Name = "periodizitaet";
            this.periodizitaet.Size = new System.Drawing.Size(121, 21);
            this.periodizitaet.TabIndex = 1;
            // 
            // nutzungsdauer
            // 
            this.nutzungsdauer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nutzungsdauer.FormattingEnabled = true;
            this.nutzungsdauer.Items.AddRange(new object[] {
            "niedrig",
            "moderat",
            "hoch"});
            this.nutzungsdauer.Location = new System.Drawing.Point(263, 46);
            this.nutzungsdauer.Name = "nutzungsdauer";
            this.nutzungsdauer.Size = new System.Drawing.Size(121, 21);
            this.nutzungsdauer.TabIndex = 5;
            // 
            // anpassbarkeit
            // 
            this.anpassbarkeit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.anpassbarkeit.FormattingEnabled = true;
            this.anpassbarkeit.Items.AddRange(new object[] {
            "niedrig",
            "moderat",
            "hoch"});
            this.anpassbarkeit.Location = new System.Drawing.Point(263, 19);
            this.anpassbarkeit.Name = "anpassbarkeit";
            this.anpassbarkeit.Size = new System.Drawing.Size(121, 21);
            this.anpassbarkeit.TabIndex = 4;
            // 
            // intensitaet
            // 
            this.intensitaet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.intensitaet.FormattingEnabled = true;
            this.intensitaet.Items.AddRange(new object[] {
            "niedrig",
            "moderat",
            "hoch"});
            this.intensitaet.Location = new System.Drawing.Point(263, 73);
            this.intensitaet.Name = "intensitaet";
            this.intensitaet.Size = new System.Drawing.Size(121, 21);
            this.intensitaet.TabIndex = 3;
            // 
            // groesse
            // 
            this.groesse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groesse.FormattingEnabled = true;
            this.groesse.Items.AddRange(new object[] {
            "niedrig",
            "moderat",
            "hoch"});
            this.groesse.Location = new System.Drawing.Point(263, 46);
            this.groesse.Name = "groesse";
            this.groesse.Size = new System.Drawing.Size(121, 21);
            this.groesse.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.periodizitaet);
            this.groupBox1.Controls.Add(this.intensitaet);
            this.groupBox1.Controls.Add(this.groesse);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(390, 108);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Leistungsanforderungen";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.anpassbarkeit);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.nutzungsdauer);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(390, 80);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Flexibilitätsanforderungen";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 335);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ausgabe);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Handlungsempehlungsgenerator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox ausgabe;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox periodizitaet;
        private System.Windows.Forms.ComboBox nutzungsdauer;
        private System.Windows.Forms.ComboBox anpassbarkeit;
        private System.Windows.Forms.ComboBox intensitaet;
        private System.Windows.Forms.ComboBox groesse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;

    }
}

