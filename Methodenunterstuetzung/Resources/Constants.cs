﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.Resources
{
    /// <summary>
    /// The outsourced constants. 
    /// </summary>
    public class Constants
    {
        // @ Character to not explicite escape the backslash
        public const string DATABASE_FILE = @"integrationsarchitektur.yap";
    }
}
