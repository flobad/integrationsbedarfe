﻿using Methodenunterstuetzung.ConceptualObjects;
using Methodenunterstuetzung.DataAccessObjects;
using Methodenunterstuetzung.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung.Views
{
    public partial class BetrieblicheObjekteView : Form
    {
        private static TransactionAutomationDegrees _requiredAutomation = TransactionAutomationDegrees.nicht_gesetzt;

        /// <summary>
        /// Eine (versteckte) Klassenvariable vom Typ der eigene Klasse
        /// </summary>
        //private static BetrieblicheObjekteView m_Instance;

        //private BetrieblichesObjektServiceClient proxy;

        /// <summary>
        /// Instanziierung
        /// </summary>
        /// <returns></returns>
        //public static BetrieblicheObjekteView GetInstance()
        //{
        //    // lazy creation
        //    if (m_Instance == null)
        //    {
        //        m_Instance = new BetrieblicheObjekteView();
        //    }
        //    return m_Instance;
        //}

        /// <summary>
        /// Konstuktor
        /// Dieser Konstruktor kann von außen nicht erreicht werden.
        /// </summary>
        public BetrieblicheObjekteView()
        {
            InitializeComponent();

            // Set up the ToolTips
            toolTip1.SetToolTip(strategieCheckBox, "Strategie hinsichtlich Adoption und Einsatz von Technologien und folglich die Auswahl der zur Verfügung stehenden Basismaschinen.");
            toolTip1.SetToolTip(vorgabeCheckBox, "vorgegebene AwS im Hinblick auf Standardisierung bzw. Vereinheitlichung der AwS-Landschaft unter Berücksichtigung von Ausfallsicherheit im laufenden Betrieb und Investitionsschutz sowie Restdauer des Supports und Auswirkungen auf die Freiheitsgrade bei der Wahl der Basismaschinen.");
            toolTip1.SetToolTip(interdependenzenCheckBox, "Einfluss von oder auf andere geplante oder gegenwärtig laufende Projekte.");
            //toolTip1.SetToolTip(normenCheckBox, "Einhaltung von Normen");
            //toolTip1.SetToolTip(rechtlRahmenbedCheckBox, "rechtliche Rahmenbedingungen, wie Gesetzesvorgaben");
            //toolTip1.SetToolTip(kostenCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");


            this.pictureBox1.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\011_Lenkungssicht_des_untersuchten_TP.wmf");
            this.pictureBox12.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\102_Aufgabentraegerzuordnung_in_der_Lenkungssicht_des_untersuchten_TP.wmf");
            this.pictureBox10.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\201_AwS-Grenzen_fuer_den_untersuchten_TP.wmf");
            this.pictureBox3.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\303_Sollkonzept_fuer_den_untersuchten_TP.wmf");
            this.pictureBox4.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\401_Ist-Zustand_in_der_Lenkungssicht_des_untersuchten_TP.wmf");
            this.pictureBox5.Image = System.Drawing.Image.FromFile(@"..\..\Resources\ModelleFallstudie" + Settings.Default.Mandant + "\\501_Zielkonzept_fuer_den_untersuchten_TP.wmf");


            using (var ctx = new DissEntityDataModel())
            {
                // Query for the Blog named ADO.NET Blog
                //var mandant = ctx.Mandanten
                //                .Where(b => b.MandantName == textBox1.Text)
                //                .FirstOrDefault();

                // Query for all blogs with names starting with B
                var bos = from b in ctx.BusinessObjects
                          where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                              //where b.MandantName.StartsWith("B")
                          select b;
                foreach (BusinessObject bo in bos)
                {
                    vorhandeneBOlistBox.Items.Add(bo.BusinessObjectName);

                    // Fülle die Combo-Boxen zur Festlegung sendender und empfangender betrieblicher Objekte
                    sendingComboBox.Items.Add(bo.BusinessObjectName);
                    receivingComboBox.Items.Add(bo.BusinessObjectName);
                }
            }
        }

        private void openBetrObjButton_Click(object sender, EventArgs e)
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                if (File.Exists(@"..\..\Resources\ModelleFallstudie\Daten" + vorhandeneBOlistBox.SelectedItem.ToString() + ".xml"))
                {
                    BetrieblichesObjektDetailView bo = new BetrieblichesObjektDetailView(vorhandeneBOlistBox.SelectedItem.ToString());
                    bo.Show();
                }
                else
                {
                    toolStripStatusLabel1.Text = "Zum selektierten betrieblichen Objekt existiert noch keine Katalogisierung gegenwärtigert Automatisierungsgrade!";
                }
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein betriebliches Objekt ausgewählt!";
            }
        }

        private void hinzufuegenBetrObjButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                //betrieblichesObjekt bo = new betrieblichesObjekt();
                //bo.nameBO = textBox1.Text;
                //proxy.create(bo);

                using (var ctx = new DissEntityDataModel())
                {
                    BusinessObject bo = new BusinessObject() { BusinessObjectName = textBox1.Text };

                    // Ordne das Betriebliche Objekt dem Mandanten zu
                    Mandant mandant = ctx.Mandanten
                        .Where(b => b.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();
                    mandant.BusinessObjects.Add(bo);
                    bo.ZugeordneterMandant = mandant;

                    ctx.BusinessObjects.Add(bo);
                    ctx.SaveChanges();
                }

                //toolStripStatusLabel1.Text = new BusinessObjectDAO().CreateNewBusinessObject(new BusinessObject(textBox1.Text));
                vorhandeneBOlistBox.Items.Add(textBox1.Text);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein betriebliches Objekt hinzugefügt!";
            }
            textBox1.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addTAbutton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(taTextBox.Text) && vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == taTextBox.Text
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (transaction == null)
                    {
                        transaction = new Transaction() { TransactionName = taTextBox.Text };

                        // AIM
                        //transaction.Reihenfolgebeziehung = false;
                        //transaction.PartielleGleichheit = false;
                        //transaction.PartielleIdentitaet = false;
                        //transaction.UeberlappendeLoesungsverfahren = false;

                        // SOM-Unternehmensarchitektur
                        // U-Plan
                        //transaction.Strategie = false;
                        //transaction.Vorgabe = false;
                        //transaction.Interdependenzen = false;
                        //transaction.Normen = false;
                        //transaction.Datenschutz = false;
                        //transaction.Kosten = false;

                        // Ressourcenebene
                        //transaction.Interoberability = false;
                        //transaction.KnowHow = false;
                        //transaction.Einfachheit = false;

                        transaction.ZugeordneterMandant = bo.ZugeordneterMandant;

                        ctx.Transactions.Add(transaction);
                        ctx.SaveChanges();
                    }
                    bo.ZugeordneteTAs.Add(transaction);
                    ctx.SaveChanges();
                }
                tasListBox.Items.Add(taTextBox.Text);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde keine Transaktion hinzugefügt!";
            }
            taTextBox.Clear();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBetrObjButton_Click(object sender, EventArgs e)
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    ctx.BusinessObjects.Remove(bo);
                    ctx.SaveChanges();
                }

                //toolStripStatusLabel1.Text = new BusinessObjectDAO().DeleteBusinessObjectByName(listBox1.SelectedItem.ToString());
                vorhandeneBOlistBox.Items.Remove(vorhandeneBOlistBox.SelectedItem);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein betriebliches Objekt ausgewählt!";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteTAbutton_Click(object sender, EventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    Transaction aws = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.Text
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (aws == null)
                    {
                        aws = new Transaction() { TransactionName = taTextBox.Text };
                        ctx.Transactions.Add(aws);
                        ctx.SaveChanges();
                    }
                    ctx.Transactions.Remove(aws);
                    ctx.SaveChanges();
                }
                tasListBox.Items.Remove(tasListBox.SelectedItem);
            }
        }

        #region SelectedIndexChanged
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void vorhandeneBOlistBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab.Equals(awsGrenzenTabPage))
            {
                fillAwSListBox();
            }
            else if (tabControl.SelectedTab.Equals(zuordnungATtabPage))
            {
                fillZuordnungATtabPage();
            }
            else if (tabControl.SelectedTab.Equals(tasBetrObjTabPage))
            {
                fillTasListBox();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tasListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Clearing Combo-Boxes
                    //sendingComboBox.Items.Clear();
                    //receivingComboBox.Items.Clear();


                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    //sendingComboBox.Items.Add(bo.SendingObject.BusinessObjectName);
                    //receivingComboBox.Items.Add(bo.ReceivingObject.BusinessObjectName);

                    // Fülle die Combo-Boxen
                    //var bos = from b in ctx.BusinessObjects
                    //          select b;
                    //foreach (BusinessObject ba in bos)
                    //{
                    //    sendingComboBox.Items.Add(ba.BusinessObjectName);
                    //    receivingComboBox.Items.Add(ba.BusinessObjectName);
                    //}

                    // Lege den selektierten Eintrag in der Combo-Box fest
                    string sendingText = "Kein sendendes betr. Obj. ausgewählt";
                    if (transaction.SendingObject != null)
                    {
                        sendingComboBox.Items.Remove(sendingText);
                        sendingComboBox.SelectedItem = transaction.SendingObject.BusinessObjectName;
                    }
                    else
                    {
                        sendingComboBox.Items.Add(sendingText);
                        sendingComboBox.SelectedItem = sendingText;
                    }

                    string receivingText = "Kein empfangendes betr. Obj. ausgewählt";
                    if (transaction.ReceivingObject != null)
                    {
                        //receivingComboBox.Items.Contains(receivingComboBox);
                        receivingComboBox.Items.Remove(receivingText);
                        receivingComboBox.SelectedItem = transaction.ReceivingObject.BusinessObjectName;
                    }
                    else
                    {
                        receivingComboBox.Items.Add(receivingText);
                        receivingComboBox.SelectedItem = receivingText;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sendingComboBox_SelectedItemChanged(object sender, EventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    // Entferne die Transaktion aus dem vormals zugeordneten betrieblichen Objekt
                    BusinessObject oldSendingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == sendingComboBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();
                    if (oldSendingBO != null)
                    {
                        oldSendingBO.ZugeordneteTAs.Remove(transaction);
                    }

                    // Query for the Blog named ADO.NET Blog
                    BusinessObject newSendingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == sendingComboBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaction.SendingObject = newSendingBO;
                    if (newSendingBO != null)
                    {
                        newSendingBO.ZugeordneteTAs.Add(transaction);
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void receivingComboBox_SelectedItemChanged(object sender, EventArgs e)
        {
            if (tasListBox.SelectedItem != null && !string.IsNullOrEmpty(tasListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == tasListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    // Entferne die Transaktion aus dem vormals zugeordneten betrieblichen Objekt
                    BusinessObject oldReceivingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == receivingComboBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();
                    if (oldReceivingBO != null)
                    {
                        oldReceivingBO.ZugeordneteTAs.Remove(transaction);
                    }

                    // Query for the Blog named ADO.NET Blog
                    BusinessObject newReceivingBO = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == receivingComboBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaction.ReceivingObject = newReceivingBO;
                    if (newReceivingBO != null)
                    {
                        newReceivingBO.ZugeordneteTAs.Add(transaction);
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void awsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            unterstuetzendeBOlistBox.Items.Clear();

            if (awsListBox.SelectedItem != null && !string.IsNullOrEmpty(awsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Anwendungssystem awse = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == awsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    foreach (BusinessObject bo in awse.UnterstuetzendeBOs)
                    {
                        unterstuetzendeBOlistBox.Items.Add(bo.BusinessObjectName);
                    }
                }
            }
        }

        /// <summary>
        /// Lade die Daten, die an der Transaktion hängen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void interAwSTAsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string notSet = "nicht gesetzt";
            // TODO neue Tabs zu programmieren
            // (1) Ein Tab mit der Wahl des Integrationskonzepts erstellen!
            // Da könnte auch die gekürzte Tabelle eingefügt werden.
            // Die umfangreiche Tabelle in der Dissertation mit dem Integrationskonzept zusammenlegen!
            // Statt auf die einzelnen Situationen auf die Auswirkungen bei den Integrationskonzepten eingehen.
            // Diese Tabelle muss dann tatsächlich nur noch die Situationen berücksichtigen bei denen heterogene AwS zum Einsatz kommen!
            // Auf jeden Fall die identifizierten AIM auslesen (lesen) und anzeigen und die Ausprägungen Integrationsmerkmale unterbringen (lesen und schreiben) (kombinierte Tabelle der Präsentation)!

            // TODO aktuelle Tab zu Ende programmieren
            // aoListBox Laden -> ok, testen
            // Zugeordnete betr. Obj. laden -> ok, testen
            // Soll- und Ist-Automatisierung laden -> ok, testen
            // Identifizierte AIM laden -> ok, testen
            // Ausgeprägte Merkmale laden -> ok, testen
            // ausgeprägte Merkmale gibt es einmal in binärer Ausprägung (boolean) (U-Plan und Ressourcenebene) und einmal in dreifacher Ausprägung (niedrig, moderat, hoch) (Aufgabenebene)
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaction = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    sendingBOlabel.Text = transaction.SendingObject.BusinessObjectName;
                    if (transaction.SendingObject.ZugeordnetesAwS != null)
                    {
                        sendingAwSlabel.Text = transaction.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        sendingAwSlabel.Text = "Kein AwS zugeordnet";
                    }
                    if (transaction.SendingObject.ZugeordneteStelle != null)
                    {
                        sendingPATlabel.Text = transaction.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        sendingPATlabel.Text = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    receivingBOlabel.Text = transaction.ReceivingObject.BusinessObjectName;
                    if (transaction.ReceivingObject.ZugeordnetesAwS != null)
                    {
                        receivingAwSlabel.Text = transaction.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        receivingAwSlabel.Text = "Kein AwS zugeordnet";
                    }
                    if (transaction.ReceivingObject.ZugeordneteStelle != null)
                    {
                        receivingPATlabel.Text = transaction.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        receivingPATlabel.Text = "Kein personeller Aufgabenträger zugeordnet";
                    }

                    // Fülle die Liste überlappender Aufgabenobjekte
                    aoListBox.Items.Clear();
                    foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                    {
                        aoListBox.Items.Add(aufgabenobjekt.TaskObjectName);
                    }

                    // AIM
                    reihenfolgebezCheckBox.Checked = transaction.Reihenfolgebeziehung;
                    gleichheitAOcheckBox.Checked = transaction.PartielleGleichheit;
                    identityAOcheckBox.Checked = transaction.PartielleIdentitaet;
                    lvCheckBox.Checked = transaction.UeberlappendeLoesungsverfahren;

                    // SOM-Unternehmensarchitektur
                    // U-Plan
                    strategieCheckBox.Checked = transaction.Strategie;
                    vorgabeCheckBox.Checked = transaction.Vorgabe;
                    interdependenzenCheckBox.Checked = transaction.Interdependenzen;
                    normenCheckBox.Checked = transaction.Normen;
                    datenschutzCheckBox.Checked = transaction.Datenschutz;
                    kostenCheckBox.Checked = transaction.Kosten;

                    // GP-Modell
                    // Periodizität
                    if (transaction.Periodizitaet.Equals(MerkmaleAuspraegungen.nicht_gesetzt))
                    {
                        periodizitaetComboBox.SelectedItem = notSet;
                    }
                    else
                    {
                        periodizitaetComboBox.SelectedItem = transaction.Periodizitaet.ToString();
                    }

                    // Größe
                    if (transaction.Groesse.Equals(MerkmaleAuspraegungen.nicht_gesetzt))
                    {
                        groesseComboBox.SelectedItem = notSet;
                    }
                    else
                    {
                        groesseComboBox.SelectedItem = transaction.Groesse.ToString();
                    }

                    // Intensität
                    if (transaction.Intensitaet.Equals(MerkmaleAuspraegungen.nicht_gesetzt))
                    {
                        intensitaetComboBox.SelectedItem = notSet;
                    }
                    else
                    {
                        intensitaetComboBox.SelectedItem = transaction.Intensitaet.ToString();
                    }

                    // Anpassbarkeit
                    if (transaction.Anpassbarkeit.Equals(MerkmaleAuspraegungen.nicht_gesetzt))
                    {
                        anpassbarkeitComboBox.SelectedItem = notSet;
                    }
                    else
                    {
                        anpassbarkeitComboBox.SelectedItem = transaction.Anpassbarkeit.ToString();
                    }

                    // Nutzungsdauer
                    if (transaction.Nutzungsdauer.Equals(NutzungsdauerAuspraegungen.nicht_gesetzt))
                    {
                        nutzungsdauerComboBox.SelectedItem = notSet;
                    }
                    else
                    {
                        nutzungsdauerComboBox.SelectedItem = transaction.Nutzungsdauer.ToString();
                    }


                    // Ressourcenebene
                    interoperabilityCheckBox.Checked = transaction.Interoberability;
                    knowHowCheckBox.Checked = transaction.KnowHow;
                    einfachheitCheckBox.Checked = transaction.Einfachheit;


                    // Belege die Soll-Automatisierung vor
                    //sollAutomatisierungComboBox.Items.Add(TransactionAutomationDegrees.Automate);
                    //sollAutomatisierungComboBox.Items.Add(TransactionAutomationDegrees.NotAutomate);
                    if (transaction.RequiredAutomation.Equals(TransactionAutomationDegrees.Automate))
                    {
                        sollAutomatisierungComboBox.SelectedItem = "zu automatisieren";
                    }
                    else if (transaction.RequiredAutomation.Equals(TransactionAutomationDegrees.NotAutomate))
                    {
                        sollAutomatisierungComboBox.SelectedItem = "nicht zu automatisieren";
                    }
                    else
                    {
                        // Nichts vorselektieren
                        sollAutomatisierungComboBox.SelectedItem = notSet;
                    }

                    // Belege die Ist-Automatisierung vor
                    // istAutomatisierungComboBox
                    //transaction.ActualAutomation
                    if (transaction.ActualAutomation.Equals(TransactionAutomationDegrees.Automate))
                    {
                        istAutomatisierungComboBox.SelectedItem = "automatisiert";
                    }
                    else if (transaction.ActualAutomation.Equals(TransactionAutomationDegrees.NotAutomate))
                    {
                        istAutomatisierungComboBox.SelectedItem = "nicht automatisiert";
                    }
                    else
                    {
                        // Nichts vorselektieren
                        istAutomatisierungComboBox.SelectedItem = notSet;
                    }
                }
            }
            //calculateHandlungsempfehlung();
        }

        private void istAutomatisierungComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (istAutomatisierungComboBox.Text.Equals("automatisiert"))
                    {
                        transaktion.ActualAutomation = TransactionAutomationDegrees.Automate;
                    }
                    else if (istAutomatisierungComboBox.Text.Equals("nicht automatisiert"))
                    {
                        transaktion.ActualAutomation = TransactionAutomationDegrees.NotAutomate;
                    }
                    else // nicht gesetzt
                    {
                        transaktion.ActualAutomation = TransactionAutomationDegrees.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void sollAutomatisierungComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (sollAutomatisierungComboBox.Text.Equals("zu automatisieren"))
                    {
                        transaktion.RequiredAutomation = TransactionAutomationDegrees.Automate;
                    }
                    else if (sollAutomatisierungComboBox.Text.Equals("nicht zu automatisieren"))
                    {
                        transaktion.RequiredAutomation = TransactionAutomationDegrees.NotAutomate;
                    }
                    else // nicht gesetzt
                    {
                        transaktion.RequiredAutomation = TransactionAutomationDegrees.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void deltaTAsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                ueberlappendeAOlistBox.Items.Clear();

                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    sendBOlabel.Text = transaktion.SendingObject.BusinessObjectName;
                    sendAwSlabel.Text = transaktion.SendingObject.ZugeordnetesAwS.AufgabentraegerName;
                    sendPATlabel.Text = transaktion.SendingObject.ZugeordneteStelle.AufgabentraegerName;
                    receivBOlabel.Text = transaktion.ReceivingObject.BusinessObjectName;
                    receivAwSlabel.Text = transaktion.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName;
                    receivPATlabel.Text = transaktion.ReceivingObject.ZugeordneteStelle.AufgabentraegerName;

                    foreach (TaskObject item in transaktion.UeberlappendeAOs)
                    {
                        ueberlappendeAOlistBox.Items.Add(item.TaskObjectName);
                    }

                    // AIM
                    reihenfolgeCheckBox.Checked = transaktion.Reihenfolgebeziehung;
                    aoGleichheitCheckBox.Checked = transaktion.PartielleGleichheit;
                    aoIdentityCheckBox.Checked = transaktion.PartielleIdentitaet;
                    lvOverlappingCheckBox.Checked = transaktion.UeberlappendeLoesungsverfahren;

                    // Integrationsziele
                    datenredundanzCheckBox.Checked = transaktion.Datenredundanz;
                    funktionsredundanzCheckBox.Checked = transaktion.Funktionsredundanz;
                    vorgangssteuerungCheckBox.Checked = transaktion.Vorgangssteuerung;
                    kommunikationsstrukturCheckBox.Checked = transaktion.Kommunikationsstrutkur;
                    integritaetCheckBox.Checked = transaktion.Integritaet;

                    // bislang gewähltes Integrationskonzept
                    if (transaktion.Integrationskonzept.Equals(Integrationskonzept.aufgabentraegerorientiert))
                    {
                        integrationskonzeptComboBox.SelectedItem = "aufgabenträgerorientierte Funktionsintegration";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenflussorientiert))
                    {
                        integrationskonzeptComboBox.SelectedItem = "datenflussorientierte Funktionsintegration";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.datenintegration))
                    {
                        integrationskonzeptComboBox.SelectedItem = "Datenintegration";
                    }
                    else if (transaktion.Integrationskonzept.Equals(Integrationskonzept.objektintegration))
                    {
                        integrationskonzeptComboBox.SelectedItem = "Objektintegration";
                    }
                    else // nicht gesetzt
                    {
                        integrationskonzeptComboBox.SelectedItem = "nicht gesetzt";
                    }

                    _requiredAutomation = transaktion.RequiredAutomation;


                    toolTip1.SetToolTip(strategieCheckBox, "Strategie hinsichtlich Adoption und Einsatz von Technologien und folglich die Auswahl der zur Verfügung stehenden Basismaschinen.");
                    toolTip1.SetToolTip(vorgabeCheckBox, "vorgegebene AwS im Hinblick auf Standardisierung bzw. Vereinheitlichung der AwS-Landschaft unter Berücksichtigung von Ausfallsicherheit im laufenden Betrieb und Investitionsschutz sowie Restdauer des Supports und Auswirkungen auf die Freiheitsgrade bei der Wahl der Basismaschinen.");
                    toolTip1.SetToolTip(interdependenzenCheckBox, "Einfluss von oder auf andere geplante oder gegenwärtig laufende Projekte.");
                    //toolTip1.SetToolTip(normenCheckBox, "Einhaltung von Normen");
                    //toolTip1.SetToolTip(rechtlRahmenbedCheckBox, "rechtliche Rahmenbedingungen, wie Gesetzesvorgaben");
                    //toolTip1.SetToolTip(kostenCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");

                    //toolTip1.SetToolTip(interoperabilityCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");
                    //toolTip1.SetToolTip(knowHowCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");
                    //toolTip1.SetToolTip(einfachheitCheckBox, "Kosten der Implementierung sowie Kostenvorgaben");

                    // Blende Empfehlungen ein
                    hinweiseTextBox.Clear();
                    if (!transaktion.Strategie)
                    {
                        hinweiseTextBox.Text += "Es bestehen keine Vorgaben hinsichtlich der Adoption und dem Einsatz von Technologien.";
                    }
                    else
                    {
                        hinweiseTextBox.Text += "Es bestehen Vorgaben hinsichtlich der Adoption und dem Einsatz von Technologien.";
                    }
                    hinweiseTextBox.Text += Environment.NewLine;

                    if (!transaktion.Vorgabe)
                    {
                        hinweiseTextBox.Text += "Es bestehen keine Vorgaben hinsichtlich der einzusetzenden AwS. Deshalb kann auch die Möglichkeit die involvierten AwS durch ein integriertes AwS abzulösen in Erwägung gezogen werden. Als Integrationskonzepte stünden hierfür dann die Daten- sowie die Objektintegration zur Verfügung.";
                    }
                    else
                    {
                        hinweiseTextBox.Text += "Es bestehen Vorgaben hinsichtlich der einzusetzenden AwS.";
                    }
                    hinweiseTextBox.Text += Environment.NewLine;

                    if (transaktion.Interdependenzen)
                    {
                        hinweiseTextBox.Text += "Achtung: Es bestehen Abhängigkeiten zu anderen, laufenden Projekten!";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (transaktion.Normen)
                    {
                        hinweiseTextBox.Text += "";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (transaktion.Rahmenbedingungen)
                    {
                        hinweiseTextBox.Text += "";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (transaktion.Kosten)
                    {
                        hinweiseTextBox.Text += "Für die Integration bestehen Vorgaben bzgl. der Kosten. Diese dürfen nicht überschritten werden.";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }

                    if (!transaktion.Interoberability)
                    {
                        hinweiseTextBox.Text += "Die involvierten AwS sind nicht interoperabel bzw. können nicht interoperabel gemacht werden. Deshalb ist eine AwS-Integration ausgeschlossen.";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }
                    if (!transaktion.KnowHow)
                    {
                        hinweiseTextBox.Text += "";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }
                    if (transaktion.Einfachheit)
                    {
                        hinweiseTextBox.Text += "";
                        hinweiseTextBox.Text += Environment.NewLine;
                    }


                    // TODO die Anzeige muss noch dynamisiert werden.
                    mmk1TextBox.Text = "Bleibt unverändert, da nach einer Änderung nicht mehr Attribute des AO erfasst werden als vorher.";
                    mmk2TextBox.Text = "Reduziert sich auf die Kontrolle der überlappenden Attribute des AO; nicht überlappende sind dennoch zu pflegen";
                    empfehlungTextBox.Text = "Maschinelle Datenübertragung zwischen vorhandenen AwS genügt; personelle Vorgangsauslösung";
                    restriktionenTextBox.Text = "Beteiligte AwS sind kopplungsfähig oder können kopplungsfähig gemacht werden.";

                    homogAOTypenTextBox.Text = "Wird nicht gelöst - Behelfslösung durch Mapping der Attribute partiell gleicher AO-Typen.";
                    homogAOInstanzTextBox.Text = "Lösung erzeugt redundante AO-Instanzen.";
                    ueberlappendeLVsTextBox.Text = "Wird nicht adressiert.";
                }
            }
        }

        private void integrationskonzeptComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (integrationskonzeptComboBox.Text.Equals("aufgabenträgerorientierte Funktionsintegration"))
                    {
                        transaktion.Integrationskonzept = Integrationskonzept.aufgabentraegerorientiert;
                    }
                    else if (integrationskonzeptComboBox.Text.Equals("datenflussorientierte Funktionsintegration"))
                    {
                        transaktion.Integrationskonzept = Integrationskonzept.datenflussorientiert;
                    }
                    else if (integrationskonzeptComboBox.Text.Equals("Datenintegration"))
                    {
                        transaktion.Integrationskonzept = Integrationskonzept.datenintegration;
                    }
                    else if (integrationskonzeptComboBox.Text.Equals("Objektintegration"))
                    {
                        transaktion.Integrationskonzept = Integrationskonzept.objektintegration;
                    }
                    else // nicht gesetzt
                    {
                        transaktion.Integrationskonzept = Integrationskonzept.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Blende die Boxen zur Bearbeitung der betrieblichen Objekte ein
            //groupBox1.Visible = true;
            //groupBox2.Visible = true;

            if (tabControl.SelectedTab.Equals(somGpModellIAStabPage))
            {
                // Blende die Boxen zur Bearbeitung der betrieblichen Objekte ein
                //groupBox1.Visible = true;
                //groupBox2.Visible = true;
            }
            else if (tabControl.SelectedTab.Equals(awsGrenzenTabPage))
            {
                fillAwSListBox();
            }
            else if (tabControl.SelectedTab.Equals(zuordnungATtabPage))
            {
                fillZuordnungATtabPage();
            }
            else if (tabControl.SelectedTab.Equals(tasBetrObjTabPage))
            {
                fillTasListBox();
            }
            else if (tabControl.SelectedTab.Equals(deltaTAsTabPage))
            {
                // Blende die Boxen zur Bearbeitung der betrieblichen Objekte aus
                //groupBox1.Visible = false;
                //groupBox2.Visible = false;

                deltaTAsListBox.Items.Clear();
                using (var ctx = new DissEntityDataModel())
                {
                    // Fülle die List-Box mit den Transaktionen, deren Ist-Automatisierung nicht mit der Soll-Automatisierung übereinstimmen.
                    var transactions = from b in ctx.Transactions
                              where b.RequiredAutomation != b.ActualAutomation
                                && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                              select b;

                    bool isFirstSelected = false;
                    foreach (Transaction transaction in transactions)
                    {
                        deltaTAsListBox.Items.Add(transaction.TransactionName);
                        if (isFirstSelected == false)
                        {
                            deltaTAsListBox.SelectedItem = transaction.TransactionName;
                        }
                        isFirstSelected = true;
                    }
                    if (deltaTAsListBox.Items.Count == 0)
                    {
                        deltaTAsListBox.Items.Add("Es besteht für keine Transaktion ein Handlungsbedarf!");
                    }
                }
                calculateIntegrationskonzept();
                toolStripStatusLabel1.Text = "Transaktionen, für die ein Handlungsbedarf besteht, werden angezeigt.";
            }
            else if (tabControl.SelectedTab.Equals(sollkonzeptTabPage))
            {
                // Blende die Boxen zur Bearbeitung der betrieblichen Objekte aus
                //groupBox1.Visible = false;
                //groupBox2.Visible = false;

                interAwSTAsListBox.Items.Clear();
                using (var ctx = new DissEntityDataModel())
                {
                    // Fülle die List-Box mit den Inter-AwS-Transaktionen
                    var transactions = from b in ctx.Transactions
                              where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                                && b.SendingObject.ZugeordnetesAwS.AufgabentraegerName != b.ReceivingObject.ZugeordnetesAwS.AufgabentraegerName
                              select b;
                    int i = 0;
                    foreach (Transaction transaction in transactions)
                    {
                        interAwSTAsListBox.Items.Add(transaction.TransactionName);
                        if (i == 0)
                        {
                            // Selektiere das erste Element der Liste vor
                            interAwSTAsListBox.SelectedItem = transaction.TransactionName;

                            // TODO Aufrufen der zugehörigen fill-Methode (aus interAwSTAsListBox_SelectedIndexChanged)!
                            //sendingBOlabel.Text = transaction.SendingObject.BusinessObjectName;
                            //receivingBOlabel.Text = transaction.ReceivingObject.BusinessObjectName;

                            //// Fülle die Liste überlappender Aufgabenobjekte
                            //aoListBox.Items.Clear();
                            //foreach (TaskObject aufgabenobjekt in transaction.UeberlappendeAOs)
                            //{
                            //    aoListBox.Items.Add(aufgabenobjekt.TaskObjectName);
                            //}
                            // istAutomatisierungComboBox
                            // sollAutomatisierungComboBox
                        }
                        i++;
                    }
                }
                // TODO Performance: Beim Aufruf dieser TabPage das SelectedIndexChanged nur für die selektierte Transaktion aufrufen und für die anderen nicht!!!
                //interAwSTAsListBox_SelectedIndexChanged(sender, e);
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pATzuordnenButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(namePATtextBox.Text) && vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {

                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    //if (bo.ZugeordneteStelle != null)
                    //{
                    //    bo.ZugeordneteStelle.AufgabentraegerName = namePATtextBox.Text;
                    //}
                    //else
                    //{

                    //PersonellerAufgabentraegerDAO pATDAO = new PersonellerAufgabentraegerDAO();
                    PersonellerAufgabentraeger pAT = ctx.PersonelleAufgabentraeger
                        .Where(b => b.AufgabentraegerName == namePATtextBox.Text
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (pAT == null)
                    {
                        pAT = new PersonellerAufgabentraeger() { AufgabentraegerName = namePATtextBox.Text };
                        pAT.ZugeordneterMandant = bo.ZugeordneterMandant;
                        //toolStripStatusLabel1.Text = new PersonellerAufgabentraegerDAO().CreateNewPersonellerAufgabentraeger(pAT);
                        ctx.PersonelleAufgabentraeger.Add(pAT);
                        ctx.SaveChanges();
                    }
                    //pAT.UnterstuetzendeBOs.Add(bo);
                    bo.ZugeordneteStelle = pAT;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                    //}
                }
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein personeller Aufgabenträger hinzugefügt!";
            }
            namePATtextBox.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void awSzuordnenButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(nameAwStextBox.Text) && vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {

                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    //if (bo.ZugeordnetesAwS != null)
                    //{
                    //    bo.ZugeordnetesAwS.AufgabentraegerName = nameAwStextBox.Text;
                    //}
                    //else
                    //{
                    Anwendungssystem aws = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == nameAwStextBox.Text
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (aws == null)
                    {
                        aws = new Anwendungssystem() { AufgabentraegerName = nameAwStextBox.Text };
                        aws.ZugeordneterMandant = bo.ZugeordneterMandant;
                        ctx.Anwendungssysteme.Add(aws);
                        ctx.SaveChanges();
                    }
                    bo.ZugeordnetesAwS = aws;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                    //}
                }
                label4.Text = nameAwStextBox.Text;
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Anwendungssystem hinzugefügt!";
            }
            nameAwStextBox.Clear();
        }

        #region fill-methods
        private void fillAwSListBox()
        {
            awsListBox.Items.Clear();
            unterstuetzendeBOlistBox.Items.Clear();

            using (var ctx = new DissEntityDataModel())
            {
                var awse = from b in ctx.Anwendungssysteme
                           where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                           select b;
                foreach (Anwendungssystem aws in awse)
                {
                    awsListBox.Items.Add(aws.AufgabentraegerName);

                    // Falls ein betriebliches Objekt selektiert ist, selektiere das unterstützende AwS und die anderen unterstützenden AwS
                    if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
                    {
                        foreach (BusinessObject bo in aws.UnterstuetzendeBOs)
                        {
                            if (bo.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString()))
                            {
                                awsListBox.SelectedItem = aws.AufgabentraegerName;
                            }
                        }
                    }
                }
            }
        }

        private void fillZuordnungATtabPage()
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    BusinessObject bo = ctx.BusinessObjects
                        .Where(b => b.BusinessObjectName == vorhandeneBOlistBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    // Fülle die zugeordnete Stelle
                    if (bo.ZugeordneteStelle != null)
                    {
                        label3.Text = bo.ZugeordneteStelle.AufgabentraegerName;
                        namePATtextBox.Text = bo.ZugeordneteStelle.AufgabentraegerName;
                    }
                    else
                    {
                        label3.Text = "bislang wurde noch kein personeller AT zugeordnet";
                        namePATtextBox.Clear();
                    }

                    // Fülle das zugeordnete AwS
                    if (bo.ZugeordnetesAwS != null)
                    {
                        label4.Text = bo.ZugeordnetesAwS.AufgabentraegerName;
                        nameAwStextBox.Text = bo.ZugeordnetesAwS.AufgabentraegerName;
                    }
                    else
                    {
                        label4.Text = "bislang wurde noch kein AwS zugeordnet";
                        nameAwStextBox.Clear();
                    }
                }
            }
        }

        private void fillTasListBox()
        {
            if (vorhandeneBOlistBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneBOlistBox.SelectedItem.ToString()))
            {
                tasListBox.Items.Clear();
                using (var ctx = new DissEntityDataModel())
                {
                    // Fülle die List-Box mit den Transaktionen, die zu einem betrieblichen Objekt gehören.
                    var transactions = from b in ctx.Transactions
                              where b.SendingObject.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString())
                                && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                                || b.ReceivingObject.BusinessObjectName.Equals(vorhandeneBOlistBox.SelectedItem.ToString())
                                && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                              select b;
                    int i = 0;
                    foreach (Transaction transaction in transactions)
                    {
                        tasListBox.Items.Add(transaction.TransactionName);
                        if (i == 0)
                        {
                            tasListBox.SelectedItem = transaction.TransactionName;
                        }
                        i++;
                    }
                }
            }
        }
        #endregion

        private void delteAObutton_Click(object sender, EventArgs e)
        {
            if (aoListBox.SelectedItem != null && !string.IsNullOrEmpty(aoListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    TaskObject aufgabenobjekt = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == aoListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.UeberlappendeAOs.Remove(aufgabenobjekt);

                    // Sofern das AO keiner anderen Transaktion zugewiesen ist, kann es entfernt werden
                    aufgabenobjekt.ZugeordneteTransaktionen.Remove(transaktion);
                    if (aufgabenobjekt.ZugeordneteTransaktionen.Count == 0)
                    {
                        ctx.TaskObjects.Remove(aufgabenobjekt);
                    }

                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                aoListBox.Items.Remove(aoListBox.SelectedItem.ToString());
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Aufgabenobjekt gelöscht!";
            }
            //aoTextBox.Clear();
        }


        /// <summary>
        /// Hinzufügen eines Aufgabenobjekts zur Bestimmung der Soll-Automatisierung.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addAObutton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(aoTextBox.Text) && interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    TaskObject aufgabenobjekt = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == aoTextBox.Text
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (aufgabenobjekt == null)
                    {
                        aufgabenobjekt = new TaskObject() { TaskObjectName = aoTextBox.Text };

                        Mandant mandant = ctx.Mandanten
                            .Where(b => b.MandantName == Settings.Default.Mandant)
                            .FirstOrDefault();

                        aufgabenobjekt.ZugeordneterMandant = mandant;
                        ctx.TaskObjects.Add(aufgabenobjekt);
                        ctx.SaveChanges();
                    }
                    transaktion.UeberlappendeAOs.Add(aufgabenobjekt);
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                aoListBox.Items.Add(aoTextBox.Text);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Aufgabenobjekt hinzugefügt!";
            }
            aoTextBox.Clear();
        }

        #region CheckedChanged
        private void reihenfolgebezCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Reihenfolgebeziehung = reihenfolgebezCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void gleichheitAOcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.PartielleGleichheit = gleichheitAOcheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void identityAOcheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.PartielleIdentitaet = identityAOcheckBox.Checked;

                    // Wenn partielle Identität, dann auch partielle Gleichheit
                    //if (identityAOcheckBox.Checked == true)
                    //{
                    //    gleichheitAOcheckBox.Checked = true;
                    //}
                    //else
                    //{
                    //    gleichheitAOcheckBox.Checked = false;
                    //}
                    //transaktion.PartielleGleichheit = gleichheitAOcheckBox.Checked;

                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void lvCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.UeberlappendeLoesungsverfahren = lvCheckBox.Checked;

                    // Wenn überlappende Lösungsverfahren, dann auch partielle Gleichheit und partielle Identität von AO
                    //if (lvCheckBox.Checked == true)
                    //{
                    //    gleichheitAOcheckBox.Checked = true;
                    //    identityAOcheckBox.Checked = true;
                    //}
                    //else
                    //{
                    //    gleichheitAOcheckBox.Checked = false;
                    //    identityAOcheckBox.Checked = false;
                    //}
                    //transaktion.PartielleGleichheit = gleichheitAOcheckBox.Checked;
                    //transaktion.PartielleGleichheit = identityAOcheckBox.Checked;

                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void strategieCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Strategie = strategieCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void vorgabeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Vorgabe = vorgabeCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void interdependenzenCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Interdependenzen = interdependenzenCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void normenCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Normen = normenCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void rechtlRahmenbedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Rahmenbedingungen = rechtlRahmenbedCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void datenschutzCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Datenschutz = datenschutzCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void kostenCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Kosten = kostenCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void interoperabilityCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Interoberability = interoperabilityCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void knowHowCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.KnowHow = knowHowCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void einfachheitCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Einfachheit = einfachheitCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }

        private void datenredundanzCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    //
                    transaktion.Datenredundanz = datenredundanzCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateIntegrationskonzept();
            }
        }

        private void funktionsredundanzCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Funktionsredundanz = funktionsredundanzCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateIntegrationskonzept();
            }
        }

        private void vorgangssteuerungCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Vorgangssteuerung = vorgangssteuerungCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateIntegrationskonzept();
            }
        }

        private void kommunikationsstrukturCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Kommunikationsstrutkur = kommunikationsstrukturCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateIntegrationskonzept();
            }
        }

        private void integritaetCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (deltaTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(deltaTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == deltaTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    transaktion.Integritaet = integritaetCheckBox.Checked;
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateIntegrationskonzept();
            }
        }
        #endregion

        #region ComboBoxChanged
        private void periodizitaetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()))
                    {
                        transaktion.Periodizitaet = MerkmaleAuspraegungen.niedrig;
                    }
                    else if (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                    {
                        transaktion.Periodizitaet = MerkmaleAuspraegungen.moderat;
                    }
                    else if (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()))
                    {
                        transaktion.Periodizitaet = MerkmaleAuspraegungen.hoch;
                    }
                    else
                    {
                        transaktion.Periodizitaet = MerkmaleAuspraegungen.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void groesseComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (groesseComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()))
                    {
                        transaktion.Groesse = MerkmaleAuspraegungen.niedrig;
                    }
                    else if (groesseComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                    {
                        transaktion.Groesse = MerkmaleAuspraegungen.moderat;
                    }
                    else if (groesseComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()))
                    {
                        transaktion.Groesse = MerkmaleAuspraegungen.hoch;
                    }
                    else
                    {
                        transaktion.Groesse = MerkmaleAuspraegungen.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void intensitaetComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()))
                    {
                        transaktion.Intensitaet = MerkmaleAuspraegungen.niedrig;
                    }
                    else if (intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                    {
                        transaktion.Intensitaet= MerkmaleAuspraegungen.moderat;
                    }
                    else if (intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()))
                    {
                        transaktion.Intensitaet = MerkmaleAuspraegungen.hoch;
                    }
                    else
                    {
                        transaktion.Intensitaet = MerkmaleAuspraegungen.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void anpassbarkeitComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()))
                    {
                        transaktion.Anpassbarkeit = MerkmaleAuspraegungen.niedrig;
                    }
                    else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                    {
                        transaktion.Anpassbarkeit = MerkmaleAuspraegungen.moderat;
                    }
                    else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()))
                    {
                        transaktion.Anpassbarkeit = MerkmaleAuspraegungen.hoch;
                    }
                    else
                    {
                        transaktion.Anpassbarkeit = MerkmaleAuspraegungen.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateHandlungsempfehlung();
            }
        }

        private void nutzungsdauerComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (interAwSTAsListBox.SelectedItem != null && !string.IsNullOrEmpty(interAwSTAsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Transaction transaktion = ctx.Transactions
                        .Where(b => b.TransactionName == interAwSTAsListBox.SelectedItem.ToString()
                            && b.ZugeordneterMandant.MandantName == Settings.Default.Mandant)
                        .FirstOrDefault();

                    if (nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.kurzfristig.ToString()))
                    {
                        transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.kurzfristig;
                    }
                    else if (nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.mittelfristig.ToString()))
                    {
                        transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.mittelfristig;
                    }
                    else if (nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.langfristig.ToString()))
                    {
                        transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.langfristig;
                    }
                    else
                    {
                        transaktion.Nutzungsdauer = NutzungsdauerAuspraegungen.nicht_gesetzt;
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
                calculateHandlungsempfehlung();
            }
        }
        #endregion

        private void calculateHandlungsempfehlung()
        {
            // Wurden Werte noch nicht ausgewählt
            string notSet = "nicht gesetzt";
            if (string.IsNullOrEmpty(periodizitaetComboBox.Text)
                || string.IsNullOrEmpty(groesseComboBox.Text)
                || string.IsNullOrEmpty(intensitaetComboBox.Text)
                || string.IsNullOrEmpty(anpassbarkeitComboBox.Text)
                || string.IsNullOrEmpty(nutzungsdauerComboBox.Text)

                || periodizitaetComboBox.Text.Equals(notSet)
                || groesseComboBox.Text.Equals(notSet)
                || intensitaetComboBox.Text.Equals(notSet)
                || anpassbarkeitComboBox.Text.Equals(notSet)
                || nutzungsdauerComboBox.Text.Equals(notSet))
            {
                handlungsempfehlungTextBox.Clear();
                handlungsempfehlungTextBox.Text = "Kann noch keine Handlungsempfehlung erstellen, da noch nicht alle Merkmale ausgeprägt wurden.";
                return;
            }

            handlungsempfehlungTextBox.Clear();
            handlungsempfehlungTextBox.Text = "LEER";

            //periodizitaetComboBox.SelectedItem = transaction.Periodizitaet;
            //groesseComboBox.SelectedItem = transaction.Groesse;
            //intensitaetComboBox.SelectedItem = transaction.Intensitaet;
            //anpassbarkeitComboBox.SelectedItem = transaction.Anpassbarkeit;
            //nutzungsdauerComboBox.SelectedItem = transaction.Nutzungsdauer;

            /**
            // Regel 1
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch))
            {
                ausgabe.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            if ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) || groesse.Text.Equals(MerkmaleAuspraegungen.hoch)
                || intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) || nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.hoch))
                && anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            if (nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.niedrig)
                && (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.hoch) == false || periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false
                || groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false || intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.niedrig)
                && (periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) || groesse.Text.Equals(MerkmaleAuspraegungen.moderat) || intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))
                && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.niedrig) && periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.moderat) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat)
                && ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) && groesse.Text.Equals(MerkmaleAuspraegungen.moderat) && intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.moderat) && groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false && intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false && groesse.Text.Equals(MerkmaleAuspraegungen.moderat) && intensitaet.Text.Equals(MerkmaleAuspraegungen.moderat))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            if (anpassbarkeit.Text.Equals(MerkmaleAuspraegungen.moderat) && nutzungsdauer.Text.Equals(MerkmaleAuspraegungen.moderat)
                && ((periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false)
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.niedrig) && groesse.Text.Equals(MerkmaleAuspraegungen.hoch) == false && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig))
                || (periodizitaet.Text.Equals(MerkmaleAuspraegungen.hoch) == false && groesse.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaet.Text.Equals(MerkmaleAuspraegungen.niedrig))))
            {
                if (ausgabe.Text.Equals("LEER"))
                {
                    ausgabe.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    ausgabe.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }
            */

            // Regel 1
            if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()))
            {
                handlungsempfehlungTextBox.Text = "Regel 1: Aufgabenintegration NICHT maschinell unterstützen";
            }

            // Regel 2
            else if ((periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) || groesseComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString())
                || intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) || nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.langfristig.ToString()))
                && anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false)
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 2: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 3
            else if (nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.kurzfristig.ToString())
                && (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false || periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false
                || groesseComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false || intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false))
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 3: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 4
            else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString())
                && (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) || groesseComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) || intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                && nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.mittelfristig.ToString()))
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 4: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 5
            else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString())
                && periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString())
                && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString())
                && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString())
                && nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.mittelfristig.ToString()))
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 5: Aufgabenintegration NICHT maschinell unterstützen";
                }
            }

            // Regel 6
            else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.mittelfristig.ToString())
                && ((periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false)
                || (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))
                || (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()))))
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 6: Aufgabenintegration maschinell unterstützen";
                }
            }

            // Regel 7
            else if (anpassbarkeitComboBox.Text.Equals(MerkmaleAuspraegungen.moderat.ToString()) && nutzungsdauerComboBox.Text.Equals(NutzungsdauerAuspraegungen.mittelfristig.ToString())
                && ((periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()) && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()) && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false)
                || (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()) && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.hoch.ToString()) == false && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig.ToString()))
                || (periodizitaetComboBox.Text.Equals(MerkmaleAuspraegungen.hoch) == false && groesseComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig) && intensitaetComboBox.Text.Equals(MerkmaleAuspraegungen.niedrig))))
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "Regel 7: Aufgabenintegration NICHT maschinell unterstützen";
                }

            }
            else
            {
                if (handlungsempfehlungTextBox.Text.Equals("LEER"))
                {
                    handlungsempfehlungTextBox.Text = "FEHLER: Keine Regel schlug an";
                }
                else
                {
                    handlungsempfehlungTextBox.Text += Environment.NewLine + "FEHLER: Keine Regel schlug an";
                }
            }

        }

        private void calculateIntegrationskonzept()
        {
            // Empfehlung für das Integrationskonzept: Funktionsintegration
            if (!datenredundanzCheckBox.Checked
                && !funktionsredundanzCheckBox.Checked
                && !integritaetCheckBox.Checked
                && (vorgangssteuerungCheckBox.Checked
                    || kommunikationsstrukturCheckBox.Checked)
                && !sendAwSlabel.Text.Equals(receivAwSlabel.Text))
            {
                integrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich die Integrationskonzepte datenflussorientierte Funktions- und Objektintegration an.";
                if (aoIdentityCheckBox.Checked)
                {
                    integrationskonzeptEmpfehlungTextBox.Text += " Bei Implementierung des Integrationskonzepts datenflussorientierte Funktionsintegration sind zur Beherrschung der Datenredundanz Behelfslösungen vorzusehen (bspw. Festlegung des führenden AwS).";
                }
            }
            else if (!funktionsredundanzCheckBox.Checked
                && !vorgangssteuerungCheckBox.Checked
                && !kommunikationsstrukturCheckBox.Checked
                && (datenredundanzCheckBox.Checked
                    || integritaetCheckBox.Checked)
                && !sendAwSlabel.Text.Equals(receivAwSlabel.Text))
            {
                integrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich die Integrationskonzepte Datenintegration sowie Objektintegration an.";
            }
            else if (sendAwSlabel.Text.Equals(receivAwSlabel.Text))
            {
                integrationskonzeptEmpfehlungTextBox.Text = "Es ist ein Integrationskonzept für eine Intra-AwS-Lösung zu implementieren. Dies wird an dieser Stelle nicht weiter betrachtet.";
            }
            else if (!datenredundanzCheckBox.Checked
                && !funktionsredundanzCheckBox.Checked
                && !vorgangssteuerungCheckBox.Checked
                && !kommunikationsstrukturCheckBox.Checked
                && !integritaetCheckBox.Checked)
            {
                integrationskonzeptEmpfehlungTextBox.Text = "Es bieten sich alle Integrationskonzepte an.";
            }
            else
            {
                integrationskonzeptEmpfehlungTextBox.Text = "Es bietet sich das Integrationskonzept Objektintegration an.";
            }

            if (sendPATlabel.Text.Equals(receivPATlabel.Text)) // && !_requiredAutomation.Equals(TransactionAutomationDegrees.Automate))
            {
                integrationskonzeptEmpfehlungTextBox.Text += " Unter Ausschluss der Implementierung einer maschinellen Unterstützung der Integration ist auch die aufgabenträgerorientierte Funktionsintegration möglich.";
            }
        }
    }
}