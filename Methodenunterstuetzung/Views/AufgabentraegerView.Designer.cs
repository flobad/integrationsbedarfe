﻿namespace Methodenunterstuetzung.Views
{
    partial class AufgabentraegerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AufgabentraegerView));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deletePATbutton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.deleteAwSbutton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deletePATbutton);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 364);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vorhandene personelle Aufgabenträger";
            // 
            // deletePATbutton
            // 
            this.deletePATbutton.Image = ((System.Drawing.Image)(resources.GetObject("deletePATbutton.Image")));
            this.deletePATbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deletePATbutton.Location = new System.Drawing.Point(74, 329);
            this.deletePATbutton.Name = "deletePATbutton";
            this.deletePATbutton.Size = new System.Drawing.Size(95, 23);
            this.deletePATbutton.TabIndex = 1;
            this.deletePATbutton.Text = "lösche pAT";
            this.deletePATbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deletePATbutton.UseVisualStyleBackColor = true;
            this.deletePATbutton.Click += new System.EventHandler(this.deletePATbutton_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(7, 20);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(228, 303);
            this.listBox1.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 399);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(520, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deleteAwSbutton);
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Location = new System.Drawing.Point(259, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(241, 364);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vorhandene Anwendungssysteme";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(6, 20);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(229, 303);
            this.listBox2.TabIndex = 0;
            // 
            // deleteAwSbutton
            // 
            this.deleteAwSbutton.Image = ((System.Drawing.Image)(resources.GetObject("deleteAwSbutton.Image")));
            this.deleteAwSbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteAwSbutton.Location = new System.Drawing.Point(73, 329);
            this.deleteAwSbutton.Name = "deleteAwSbutton";
            this.deleteAwSbutton.Size = new System.Drawing.Size(95, 23);
            this.deleteAwSbutton.TabIndex = 1;
            this.deleteAwSbutton.Text = "lösche AwS";
            this.deleteAwSbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteAwSbutton.UseVisualStyleBackColor = true;
            this.deleteAwSbutton.Click += new System.EventHandler(this.deleteAwSbutton_Click);
            // 
            // AufgabentraegerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 421);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AufgabentraegerView";
            this.Text = "Details zu den Aufgabenträgern";
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button deletePATbutton;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button deleteAwSbutton;
        private System.Windows.Forms.ListBox listBox2;
    }
}