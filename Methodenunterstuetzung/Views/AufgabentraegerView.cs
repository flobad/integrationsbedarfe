﻿using Methodenunterstuetzung.ConceptualObjects;
using Methodenunterstuetzung.DataAccessObjects;
using Methodenunterstuetzung.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung.Views
{
    public partial class AufgabentraegerView : Form
    {
        public AufgabentraegerView()
        {
            InitializeComponent();

            using (var ctx = new DissEntityDataModel())
            {
                // Query for all blogs with names starting with B
                var pATs = from b in ctx.PersonelleAufgabentraeger
                           where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                           select b;
                foreach (PersonellerAufgabentraeger pAT in pATs)
                {
                    listBox1.Items.Add(pAT.AufgabentraegerName);
                }

                var awse = from b in ctx.Anwendungssysteme
                           where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                           select b;
                foreach (Anwendungssystem aws in awse)
                {
                    listBox2.Items.Add(aws.AufgabentraegerName);
                }
            }
        }

        private void deletePATbutton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null && !string.IsNullOrEmpty(listBox1.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    PersonellerAufgabentraeger pAT = ctx.PersonelleAufgabentraeger
                        .Where(b => b.AufgabentraegerName == listBox1.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.PersonelleAufgabentraeger.Remove(pAT);
                    ctx.SaveChanges();
                }

                //toolStripStatusLabel1.Text = new PersonellerAufgabentraegerDAO().DeletePersonellerAufgabentraegerByName(listBox1.SelectedItem.ToString());
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Mandant ausgewählt!";
            }
        }

        private void deleteAwSbutton_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null && !string.IsNullOrEmpty(listBox2.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Anwendungssystem aws = ctx.Anwendungssysteme
                        .Where(b => b.AufgabentraegerName == listBox2.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.Anwendungssysteme.Remove(aws);
                    ctx.SaveChanges();
                }
                listBox2.Items.Remove(listBox2.SelectedItem);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Mandant ausgewählt!";
            }

        }
    }
}
