﻿namespace Methodenunterstuetzung.Views
{
    partial class AufgabenobjekteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AufgabenobjekteView));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.vorhandeneAOsListBox = new System.Windows.Forms.ListBox();
            this.zugeordneteTAsListBox = new System.Windows.Forms.ListBox();
            this.zugeordBetrObjListBox = new System.Windows.Forms.ListBox();
            this.deleteAObutton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deleteAObutton);
            this.groupBox1.Controls.Add(this.vorhandeneAOsListBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 482);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vorhandene Aufgabenobjekte";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.zugeordneteTAsListBox);
            this.groupBox2.Location = new System.Drawing.Point(370, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 238);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Zugeordnete Transaktionen";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(308, 115);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.zugeordBetrObjListBox);
            this.groupBox3.Location = new System.Drawing.Point(370, 256);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 238);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zugeordnete betriebliche Objekte";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(308, 359);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // vorhandeneAOsListBox
            // 
            this.vorhandeneAOsListBox.FormattingEnabled = true;
            this.vorhandeneAOsListBox.Location = new System.Drawing.Point(6, 19);
            this.vorhandeneAOsListBox.Name = "vorhandeneAOsListBox";
            this.vorhandeneAOsListBox.Size = new System.Drawing.Size(278, 420);
            this.vorhandeneAOsListBox.TabIndex = 0;
            this.vorhandeneAOsListBox.SelectedIndexChanged += new System.EventHandler(this.vorhandeneAOsListBox_SelectedIndexChanged);
            // 
            // zugeordneteTAsListBox
            // 
            this.zugeordneteTAsListBox.FormattingEnabled = true;
            this.zugeordneteTAsListBox.Location = new System.Drawing.Point(6, 19);
            this.zugeordneteTAsListBox.Name = "zugeordneteTAsListBox";
            this.zugeordneteTAsListBox.Size = new System.Drawing.Size(278, 212);
            this.zugeordneteTAsListBox.TabIndex = 0;
            // 
            // zugeordBetrObjListBox
            // 
            this.zugeordBetrObjListBox.FormattingEnabled = true;
            this.zugeordBetrObjListBox.Location = new System.Drawing.Point(6, 19);
            this.zugeordBetrObjListBox.Name = "zugeordBetrObjListBox";
            this.zugeordBetrObjListBox.Size = new System.Drawing.Size(278, 212);
            this.zugeordBetrObjListBox.TabIndex = 0;
            // 
            // deleteAObutton
            // 
            this.deleteAObutton.Image = ((System.Drawing.Image)(resources.GetObject("deleteAObutton.Image")));
            this.deleteAObutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteAObutton.Location = new System.Drawing.Point(102, 445);
            this.deleteAObutton.Name = "deleteAObutton";
            this.deleteAObutton.Size = new System.Drawing.Size(87, 23);
            this.deleteAObutton.TabIndex = 1;
            this.deleteAObutton.Text = "lösche AO";
            this.deleteAObutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteAObutton.UseVisualStyleBackColor = true;
            this.deleteAObutton.Click += new System.EventHandler(this.deleteAObutton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 509);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(674, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // AufgabenobjekteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 531);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AufgabenobjekteView";
            this.Text = "Details zu den Aufgabenobjekten";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button deleteAObutton;
        private System.Windows.Forms.ListBox vorhandeneAOsListBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox zugeordneteTAsListBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox zugeordBetrObjListBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}