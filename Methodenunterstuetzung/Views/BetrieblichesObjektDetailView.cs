﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung.Views
{
    public partial class BetrieblichesObjektDetailView : Form
    {
        /*
        private Panel buttonPanel = new Panel();
        private Button addNewRowButton = new Button();
        private Button deleteRowButton = new Button();
        */
        private string betrieblichesObjekt = null;

        public BetrieblichesObjektDetailView(string bo)
        {
            betrieblichesObjekt = bo;
            this.Load += new EventHandler(BetrieblichesObjektDetail_Load);
            InitializeComponent();
        }

        private void BetrieblichesObjektDetail_Load(System.Object sender, System.EventArgs e)
        {
            //SetupLayout();
            SetupDataGridView();
            //PopulateDataGridView();
        }


        private void songsDataGridView_CellFormatting(object sender,
            System.Windows.Forms.DataGridViewCellFormattingEventArgs e)
        {
            if (e != null)
            {
                if (this.dataGridView1.Columns[e.ColumnIndex].Name == "Release Date")
                {
                    if (e.Value != null)
                    {
                        try
                        {
                            e.Value = DateTime.Parse(e.Value.ToString())
                                .ToLongDateString();
                            e.FormattingApplied = true;
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("{0} is not a valid date.", e.Value.ToString());
                        }
                    }
                }
            }
        }

        private void addNewRowButton_Click(object sender, EventArgs e)
        {
            this.dataGridView1.Rows.Add();
        }

        private void deleteRowButton_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0 &&
                this.dataGridView1.SelectedRows[0].Index !=
                this.dataGridView1.Rows.Count - 1)
            {
                this.dataGridView1.Rows.RemoveAt(
                    this.dataGridView1.SelectedRows[0].Index);
            }
        }

        /*
        private void SetupLayout()
        {
            this.Size = new Size(600, 500);

            addNewRowButton.Text = "Add Row";
            addNewRowButton.Location = new Point(10, 10);
            addNewRowButton.Click += new EventHandler(addNewRowButton_Click);

            deleteRowButton.Text = "Delete Row";
            deleteRowButton.Location = new Point(100, 10);
            deleteRowButton.Click += new EventHandler(deleteRowButton_Click);

            buttonPanel.Controls.Add(addNewRowButton);
            buttonPanel.Controls.Add(deleteRowButton);
            buttonPanel.Height = 50;
            buttonPanel.Dock = DockStyle.Bottom;

            this.Controls.Add(this.buttonPanel);
        }
        */

        private void SetupDataGridView()
        {
            this.Controls.Add(dataGridView1);

            //dataGridView1.ColumnCount = 13;

            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.Navy;
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.ColumnHeadersDefaultCellStyle.Font =
                new Font(dataGridView1.Font, FontStyle.Bold);

            //songsDataGridView.Name = "songsDataGridView";
            //songsDataGridView.Location = new Point(8, 8);
            //songsDataGridView.Size = new Size(500, 250);
            dataGridView1.AutoSizeRowsMode =
                DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            dataGridView1.ColumnHeadersBorderStyle =
                DataGridViewHeaderBorderStyle.Single;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView1.GridColor = Color.Black;
            dataGridView1.RowHeadersVisible = false;








            // Bind the DataGridView to the BindingSource
            // and load the data from the database.
            //dataGridView1.DataSource = bindingSource1;

            dataGridView1.AutoGenerateColumns = true;

            // Populate a new data table and bind it to the BindingSource.
            DataSet dt = new DataSet();
            //dt.ReadXmlSchema(@"D:\CustvaluesSchema.xsd");
            dt.ReadXml(@"..\..\Resources\ModelleFallstudie\Daten" + betrieblichesObjekt + ".xml");
            //dt.ReadXml(xmlFile);
            dataGridView1.DataSource = dt.Tables[0];
            //dataGridView1.DataMember = "Custvalues";


            // Resize the DataGridView columns to fit the newly loaded content.
            //dataGridView1.AutoResizeColumns(
            //    DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);

            dataGridView1.Columns["Transaktion"].Name = "Zugehörige Transaktion";

            // Sollkonzept
            Color farbeSollkonzept1 = ColorTranslator.FromHtml("#FFF0FFF0");
            dataGridView1.Columns["AktionensteuerungSoll"].DefaultCellStyle.BackColor = farbeSollkonzept1;
            dataGridView1.Columns["AktionensteuerungSoll"].Name = "Aktionensteuerung Soll";
            dataGridView1.Columns["VorgangsausloesungSoll"].DefaultCellStyle.BackColor = farbeSollkonzept1;
            dataGridView1.Columns["VorgangsausloesungSoll"].Name = "Vorgangsauslösung Soll";
            dataGridView1.Columns["AktionTransaktionSoll"].DefaultCellStyle.BackColor = farbeSollkonzept1;
            dataGridView1.Columns["AktionTransaktionSoll"].Name = "Aktion / Transaktion";
            dataGridView1.Columns["AutomatAufgabenobjektSoll"].DefaultCellStyle.BackColor = farbeSollkonzept1;
            dataGridView1.Columns["AutomatAufgabenobjektSoll"].Name = "Automatisierung Aufgabenobjekt Soll";
            dataGridView1.Columns["AutomatResultatSoll"].DefaultCellStyle.BackColor = farbeSollkonzept1;
            dataGridView1.Columns["AutomatResultatSoll"].Name = "Resultat";

            // Ist-Zustand
            Color farbeAwS = ColorTranslator.FromHtml("#FFFFDAB9");
            dataGridView1.Columns["AktionensteuerungIst"].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns["AktionensteuerungIst"].Name = "Aktionensteuerung Ist";
            dataGridView1.Columns["VorgangsausloesungIst"].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns["VorgangsausloesungIst"].Name = "Vorgangsauslösung Ist";
            dataGridView1.Columns["AktionTransaktionIst"].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns["AktionTransaktionIst"].Name = "Aktion / Transaktion";
            dataGridView1.Columns["AutomatAufgabenobjektIst"].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns["AutomatAufgabenobjektIst"].Name = "Automatisierung Aufgabenobjekt Ist";
            dataGridView1.Columns["AutomatResultatIst"].DefaultCellStyle.BackColor = farbeAwS;
            // Blende die Spalte programmgesteuert aus.
            //dataGridView1.Columns["AutomatResultatIst"].Visible = false;
            dataGridView1.Columns["AutomatResultatIst"].Name = "Automatisierung Ist";

            //dataGridView1.SelectionMode =
            //    DataGridViewSelectionMode.FullRowSelect;
            //dataGridView1.MultiSelect = false;






            /*
            dataGridView1.Columns[0].Name = "Aufgabe";
            dataGridView1.Columns[1].Name = "Aktion";
            dataGridView1.Columns[2].Name = "Aufgabenobjekt";
            dataGridView1.Columns[3].Name = "Zugehörige Transaktion";

            // Sollkonzept
            Color farbeSollkonzept = ColorTranslator.FromHtml("#FFF0FFF0");
            dataGridView1.Columns[4].Name = "Aktionensteuerung";
            dataGridView1.Columns[4].DefaultCellStyle.BackColor = farbeSollkonzept;
            //songsDataGridView.Columns[4].DefaultCellStyle.Font =
            //    new Font(songsDataGridView.DefaultCellStyle.Font, FontStyle.Italic);
            dataGridView1.Columns[5].Name = "Vorgangsauslösung";
            dataGridView1.Columns[5].DefaultCellStyle.BackColor = farbeSollkonzept;
            dataGridView1.Columns[6].Name = "Aktion / Transaktion";
            dataGridView1.Columns[6].DefaultCellStyle.BackColor = farbeSollkonzept;
            dataGridView1.Columns[7].Name = "Resultat";
            dataGridView1.Columns[7].DefaultCellStyle.BackColor = farbeSollkonzept;

            // Ist-Zustand
            Color farbeAwS = ColorTranslator.FromHtml("#FFFFDAB9");
            dataGridView1.Columns[8].Name = "Aktionensteuerung";
            dataGridView1.Columns[8].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns[9].Name = "Vorgangsauslösung";
            dataGridView1.Columns[9].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns[10].Name = "Aktion / Transaktion";
            dataGridView1.Columns[10].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns[11].Name = "Aufgabenobjekt";
            dataGridView1.Columns[11].DefaultCellStyle.BackColor = farbeAwS;
            dataGridView1.Columns[12].Name = "Resultat";
            dataGridView1.Columns[12].DefaultCellStyle.BackColor = farbeAwS;
            */



            dataGridView1.SelectionMode =
                DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;
            //songsDataGridView.Dock = DockStyle.Fill;

            dataGridView1.CellFormatting += new
                DataGridViewCellFormattingEventHandler(
                songsDataGridView_CellFormatting);



        }

        private void PopulateDataGridView()
        {

            string[] row0 = { "AuffPrf>", // Aufgabe
                "(1) Zusammenstellen der Module, die in der aktuellen Periode angeboten werden und zu denen Leistungsnachweise zu erbringen sind. Die Information, in welchem Turnus ein Modul angeboten wird, ist im Modul festgelegt.", // Aktion
                "Modul", // AO
                "", // TA
                "-", // AS
                "-", // VA
                "+ (automatisierbar, da Informationen bereits im Modul enthalten sind)", // Aktion / Transaktion
                "+", // AO
                "teil", // Resultat
                "-", // AwS: AS
                "-", // AwS: VA
                "-", // AwS: Aktion / Transaktion
                "+", // AwS: AO
                "teilTODO" }; // AwS: Resultat
            string[] row1 = { "", // Aufgabe
                "(2) Festlegung der Termine, zu denen die Prüfungsangebote spätestens vorliegen müssen.", // Aktion
                "Abgabetermin", // AO
                "", // TA
                "", // AS
                "", // VA
                "-", // Aktion / Transaktion
                "-", // AO
                "", // Resultat
                "", // AwS: AS
                "", // AwS: VA
                "-", // AwS: Aktion / Transaktion
                "-", // AwS: AO
                "" }; // AwS: Resultat
            string[] row2 = { "", // Aufgabe
                "(3) Versenden der Aufforderung zur Erstellung von Prüfungsangeboten für Modulprüfungen.", // Aktion
                "Modul", // AO
                "AuffPrf", // TA
                "", // AS
                "", // VA
                "+", // Aktion / Transaktion
                "+", // AO
                "PFEIL", // Resultat
                "", // AwS: AS
                "", // AwS: VA
                "- (per E-Mail)", // AwS: Aktion / Transaktion
                "+", // AwS: AO
                "nichtTODO" }; // AwS: Resultat

            dataGridView1.Rows.Add(row0);
            dataGridView1.Rows.Add(row1);
            dataGridView1.Rows.Add(row2);

            //songsDataGridView.Columns[0].DisplayIndex = 3;
            //songsDataGridView.Columns[1].DisplayIndex = 4;
            //songsDataGridView.Columns[2].DisplayIndex = 0;
            //songsDataGridView.Columns[3].DisplayIndex = 1;
            //songsDataGridView.Columns[4].DisplayIndex = 2;
        }

    }
}
