﻿using Methodenunterstuetzung.Properties;

namespace Methodenunterstuetzung.Views
{
    partial class BetrieblicheObjekteView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BetrieblicheObjekteView));
            this.hinzufuegenBetrObjButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.vorhandeneBOlistBox = new System.Windows.Forms.ListBox();
            this.openBetrObjButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deleteBetrObjButton = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.somGpModellIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tasBetrObjTabPage = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.sendingComboBox = new System.Windows.Forms.ComboBox();
            this.receivingComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.taTextBox = new System.Windows.Forms.TextBox();
            this.addTAbutton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.deleteTAbutton = new System.Windows.Forms.Button();
            this.tasListBox = new System.Windows.Forms.ListBox();
            this.zuordnungAtIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.zuordnungATtabPage = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pATzuordnenButton = new System.Windows.Forms.Button();
            this.namePATtextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nameAwStextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.awSzuordnenButton = new System.Windows.Forms.Button();
            this.awsGrenzenIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.awsGrenzenTabPage = new System.Windows.Forms.TabPage();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.unterstuetzendeBOlistBox = new System.Windows.Forms.ListBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.awsListBox = new System.Windows.Forms.ListBox();
            this.sollkonzeptTabPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.handlungsempfehlungTextBox = new System.Windows.Forms.TextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.nutzungsdauerComboBox = new System.Windows.Forms.ComboBox();
            this.anpassbarkeitComboBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.intensitaetComboBox = new System.Windows.Forms.ComboBox();
            this.groesseComboBox = new System.Windows.Forms.ComboBox();
            this.periodizitaetComboBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.einfachheitCheckBox = new System.Windows.Forms.CheckBox();
            this.knowHowCheckBox = new System.Windows.Forms.CheckBox();
            this.interoperabilityCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.kostenCheckBox = new System.Windows.Forms.CheckBox();
            this.datenschutzCheckBox = new System.Windows.Forms.CheckBox();
            this.rechtlRahmenbedCheckBox = new System.Windows.Forms.CheckBox();
            this.normenCheckBox = new System.Windows.Forms.CheckBox();
            this.interdependenzenCheckBox = new System.Windows.Forms.CheckBox();
            this.vorgabeCheckBox = new System.Windows.Forms.CheckBox();
            this.strategieCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.sollAutomatisierungComboBox = new System.Windows.Forms.ComboBox();
            this.istAutomatisierungComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.aoTextBox = new System.Windows.Forms.TextBox();
            this.addAObutton = new System.Windows.Forms.Button();
            this.delteAObutton = new System.Windows.Forms.Button();
            this.aoListBox = new System.Windows.Forms.ListBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lvCheckBox = new System.Windows.Forms.CheckBox();
            this.identityAOcheckBox = new System.Windows.Forms.CheckBox();
            this.gleichheitAOcheckBox = new System.Windows.Forms.CheckBox();
            this.reihenfolgebezCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.receivingAwSlabel = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.receivingPATlabel = new System.Windows.Forms.Label();
            this.sendingAwSlabel = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.sendingPATlabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.receivingBOlabel = new System.Windows.Forms.Label();
            this.sendingBOlabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.interAwSTAsListBox = new System.Windows.Forms.ListBox();
            this.sollkonzeptIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.istZustandIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.deltaTAsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.ueberlappendeLVsTextBox = new System.Windows.Forms.TextBox();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.homogAOInstanzTextBox = new System.Windows.Forms.TextBox();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.homogAOTypenTextBox = new System.Windows.Forms.TextBox();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.restriktionenTextBox = new System.Windows.Forms.TextBox();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.empfehlungTextBox = new System.Windows.Forms.TextBox();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.mmk2TextBox = new System.Windows.Forms.TextBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.mmk1TextBox = new System.Windows.Forms.TextBox();
            this.hinweiseTextBox = new System.Windows.Forms.TextBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.sendAwSlabel = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.sendPATlabel = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.receivPATlabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.receivAwSlabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.receivBOlabel = new System.Windows.Forms.Label();
            this.sendBOlabel = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.ueberlappendeAOlistBox = new System.Windows.Forms.ListBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.integrationskonzeptComboBox = new System.Windows.Forms.ComboBox();
            this.integrationskonzeptEmpfehlungTextBox = new System.Windows.Forms.TextBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.integritaetCheckBox = new System.Windows.Forms.CheckBox();
            this.kommunikationsstrukturCheckBox = new System.Windows.Forms.CheckBox();
            this.vorgangssteuerungCheckBox = new System.Windows.Forms.CheckBox();
            this.funktionsredundanzCheckBox = new System.Windows.Forms.CheckBox();
            this.datenredundanzCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.lvOverlappingCheckBox = new System.Windows.Forms.CheckBox();
            this.aoIdentityCheckBox = new System.Windows.Forms.CheckBox();
            this.aoGleichheitCheckBox = new System.Windows.Forms.CheckBox();
            this.reihenfolgeCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.deltaTAsListBox = new System.Windows.Forms.ListBox();
            this.zielkonzeptIAStabPage = new System.Windows.Forms.TabPage();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.somGpModellIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tasBetrObjTabPage.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.zuordnungAtIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.zuordnungATtabPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.awsGrenzenIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.awsGrenzenTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.sollkonzeptTabPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.sollkonzeptIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.istZustandIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.deltaTAsTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.groupBox23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.zielkonzeptIAStabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // hinzufuegenBetrObjButton
            // 
            this.hinzufuegenBetrObjButton.Image = ((System.Drawing.Image)(resources.GetObject("hinzufuegenBetrObjButton.Image")));
            this.hinzufuegenBetrObjButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.hinzufuegenBetrObjButton.Location = new System.Drawing.Point(155, 54);
            this.hinzufuegenBetrObjButton.Name = "hinzufuegenBetrObjButton";
            this.hinzufuegenBetrObjButton.Size = new System.Drawing.Size(92, 23);
            this.hinzufuegenBetrObjButton.TabIndex = 0;
            this.hinzufuegenBetrObjButton.Text = "Hinzufügen";
            this.hinzufuegenBetrObjButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.hinzufuegenBetrObjButton.UseVisualStyleBackColor = true;
            this.hinzufuegenBetrObjButton.Click += new System.EventHandler(this.hinzufuegenBetrObjButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(169, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(204, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name des betrieblichen Objekts";
            // 
            // vorhandeneBOlistBox
            // 
            this.vorhandeneBOlistBox.FormattingEnabled = true;
            this.vorhandeneBOlistBox.Location = new System.Drawing.Point(9, 32);
            this.vorhandeneBOlistBox.Name = "vorhandeneBOlistBox";
            this.vorhandeneBOlistBox.Size = new System.Drawing.Size(364, 316);
            this.vorhandeneBOlistBox.TabIndex = 3;
            this.vorhandeneBOlistBox.SelectedIndexChanged += new System.EventHandler(this.vorhandeneBOlistBox_SelectedIndexChanged);
            // 
            // openBetrObjButton
            // 
            this.openBetrObjButton.Enabled = false;
            this.openBetrObjButton.Image = ((System.Drawing.Image)(resources.GetObject("openBetrObjButton.Image")));
            this.openBetrObjButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.openBetrObjButton.Location = new System.Drawing.Point(9, 358);
            this.openBetrObjButton.Name = "openBetrObjButton";
            this.openBetrObjButton.Size = new System.Drawing.Size(92, 23);
            this.openBetrObjButton.TabIndex = 4;
            this.openBetrObjButton.Text = "Öffnen";
            this.openBetrObjButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.openBetrObjButton.UseVisualStyleBackColor = true;
            this.openBetrObjButton.Click += new System.EventHandler(this.openBetrObjButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(110, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vorhandene betriebliche Objekte";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.hinzufuegenBetrObjButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 405);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(379, 83);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hinzufügen betrieblicher Objekte";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deleteBetrObjButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.vorhandeneBOlistBox);
            this.groupBox2.Controls.Add(this.openBetrObjButton);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(379, 387);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bearbeite vorhandene betriebliche Objekte";
            // 
            // deleteBetrObjButton
            // 
            this.deleteBetrObjButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteBetrObjButton.Image")));
            this.deleteBetrObjButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteBetrObjButton.Location = new System.Drawing.Point(281, 358);
            this.deleteBetrObjButton.Name = "deleteBetrObjButton";
            this.deleteBetrObjButton.Size = new System.Drawing.Size(92, 23);
            this.deleteBetrObjButton.TabIndex = 8;
            this.deleteBetrObjButton.Text = "Löschen";
            this.deleteBetrObjButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteBetrObjButton.UseVisualStyleBackColor = true;
            this.deleteBetrObjButton.Click += new System.EventHandler(this.deleteBetrObjButton_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 718);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1384, 22);
            this.statusStrip.TabIndex = 8;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl);
            this.splitContainer1.Size = new System.Drawing.Size(1384, 718);
            this.splitContainer1.SplitterDistance = 399;
            this.splitContainer1.TabIndex = 9;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.somGpModellIAStabPage);
            this.tabControl.Controls.Add(this.tasBetrObjTabPage);
            this.tabControl.Controls.Add(this.zuordnungAtIAStabPage);
            this.tabControl.Controls.Add(this.zuordnungATtabPage);
            this.tabControl.Controls.Add(this.awsGrenzenIAStabPage);
            this.tabControl.Controls.Add(this.awsGrenzenTabPage);
            this.tabControl.Controls.Add(this.sollkonzeptTabPage);
            this.tabControl.Controls.Add(this.sollkonzeptIAStabPage);
            this.tabControl.Controls.Add(this.istZustandIAStabPage);
            this.tabControl.Controls.Add(this.deltaTAsTabPage);
            this.tabControl.Controls.Add(this.zielkonzeptIAStabPage);
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(964, 712);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // somGpModellIAStabPage
            // 
            this.somGpModellIAStabPage.Controls.Add(this.pictureBox1);
            this.somGpModellIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.somGpModellIAStabPage.Name = "somGpModellIAStabPage";
            this.somGpModellIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.somGpModellIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.somGpModellIAStabPage.TabIndex = 0;
            this.somGpModellIAStabPage.Text = "(0.1) SOM-GP-Modell (IAS)";
            this.somGpModellIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(950, 680);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tasBetrObjTabPage
            // 
            this.tasBetrObjTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.tasBetrObjTabPage.Controls.Add(this.groupBox7);
            this.tasBetrObjTabPage.Controls.Add(this.groupBox6);
            this.tasBetrObjTabPage.Controls.Add(this.groupBox5);
            this.tasBetrObjTabPage.Location = new System.Drawing.Point(4, 22);
            this.tasBetrObjTabPage.Name = "tasBetrObjTabPage";
            this.tasBetrObjTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tasBetrObjTabPage.Size = new System.Drawing.Size(956, 686);
            this.tasBetrObjTabPage.TabIndex = 10;
            this.tasBetrObjTabPage.Text = "(0.3) Transaktionen betr. Obj.";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.sendingComboBox);
            this.groupBox7.Controls.Add(this.receivingComboBox);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Location = new System.Drawing.Point(293, 7);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(368, 84);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Der Transaktion zugeordnete betriebliche Objekte";
            // 
            // sendingComboBox
            // 
            this.sendingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sendingComboBox.FormattingEnabled = true;
            this.sendingComboBox.Location = new System.Drawing.Point(150, 13);
            this.sendingComboBox.Name = "sendingComboBox";
            this.sendingComboBox.Size = new System.Drawing.Size(207, 21);
            this.sendingComboBox.TabIndex = 3;
            this.sendingComboBox.SelectedIndexChanged += new System.EventHandler(this.sendingComboBox_SelectedItemChanged);
            // 
            // receivingComboBox
            // 
            this.receivingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.receivingComboBox.FormattingEnabled = true;
            this.receivingComboBox.Location = new System.Drawing.Point(150, 47);
            this.receivingComboBox.Name = "receivingComboBox";
            this.receivingComboBox.Size = new System.Drawing.Size(207, 21);
            this.receivingComboBox.TabIndex = 2;
            this.receivingComboBox.SelectedIndexChanged += new System.EventHandler(this.receivingComboBox_SelectedItemChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = ">empfangendes betr. Obj.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "sendendes betr. Obj.<";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.taTextBox);
            this.groupBox6.Controls.Add(this.addTAbutton);
            this.groupBox6.Location = new System.Drawing.Point(6, 380);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(280, 83);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Hinzufügen von Transaktionen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "TA-Name";
            // 
            // taTextBox
            // 
            this.taTextBox.Location = new System.Drawing.Point(70, 16);
            this.taTextBox.Name = "taTextBox";
            this.taTextBox.Size = new System.Drawing.Size(204, 20);
            this.taTextBox.TabIndex = 3;
            // 
            // addTAbutton
            // 
            this.addTAbutton.Image = ((System.Drawing.Image)(resources.GetObject("addTAbutton.Image")));
            this.addTAbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addTAbutton.Location = new System.Drawing.Point(100, 54);
            this.addTAbutton.Name = "addTAbutton";
            this.addTAbutton.Size = new System.Drawing.Size(92, 23);
            this.addTAbutton.TabIndex = 3;
            this.addTAbutton.Text = "Hinzufügen";
            this.addTAbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addTAbutton.UseVisualStyleBackColor = true;
            this.addTAbutton.Click += new System.EventHandler(this.addTAbutton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.deleteTAbutton);
            this.groupBox5.Controls.Add(this.tasListBox);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(280, 368);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Transaktionen des betr. Obj.";
            // 
            // deleteTAbutton
            // 
            this.deleteTAbutton.Image = ((System.Drawing.Image)(resources.GetObject("deleteTAbutton.Image")));
            this.deleteTAbutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteTAbutton.Location = new System.Drawing.Point(182, 335);
            this.deleteTAbutton.Name = "deleteTAbutton";
            this.deleteTAbutton.Size = new System.Drawing.Size(92, 23);
            this.deleteTAbutton.TabIndex = 9;
            this.deleteTAbutton.Text = "Löschen";
            this.deleteTAbutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteTAbutton.UseVisualStyleBackColor = true;
            this.deleteTAbutton.Click += new System.EventHandler(this.deleteTAbutton_Click);
            // 
            // tasListBox
            // 
            this.tasListBox.FormattingEnabled = true;
            this.tasListBox.Location = new System.Drawing.Point(6, 13);
            this.tasListBox.Name = "tasListBox";
            this.tasListBox.Size = new System.Drawing.Size(268, 316);
            this.tasListBox.TabIndex = 0;
            this.tasListBox.SelectedIndexChanged += new System.EventHandler(this.tasListBox_SelectedIndexChanged);
            // 
            // zuordnungAtIAStabPage
            // 
            this.zuordnungAtIAStabPage.Controls.Add(this.pictureBox12);
            this.zuordnungAtIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.zuordnungAtIAStabPage.Name = "zuordnungAtIAStabPage";
            this.zuordnungAtIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.zuordnungAtIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.zuordnungAtIAStabPage.TabIndex = 15;
            this.zuordnungAtIAStabPage.Text = "(1.1) Zuordnung AT (IAS)";
            this.zuordnungAtIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(3, 3);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(950, 680);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            // 
            // zuordnungATtabPage
            // 
            this.zuordnungATtabPage.BackColor = System.Drawing.SystemColors.Control;
            this.zuordnungATtabPage.Controls.Add(this.groupBox4);
            this.zuordnungATtabPage.Controls.Add(this.groupBox3);
            this.zuordnungATtabPage.Location = new System.Drawing.Point(4, 22);
            this.zuordnungATtabPage.Name = "zuordnungATtabPage";
            this.zuordnungATtabPage.Padding = new System.Windows.Forms.Padding(3);
            this.zuordnungATtabPage.Size = new System.Drawing.Size(956, 686);
            this.zuordnungATtabPage.TabIndex = 9;
            this.zuordnungATtabPage.Text = "(1.2) Zuordnung AT";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.SkyBlue;
            this.groupBox4.Controls.Add(this.pATzuordnenButton);
            this.groupBox4.Controls.Add(this.namePATtextBox);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(279, 88);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Zugeordneter personeller Aufgabenträger";
            // 
            // pATzuordnenButton
            // 
            this.pATzuordnenButton.Location = new System.Drawing.Point(83, 58);
            this.pATzuordnenButton.Name = "pATzuordnenButton";
            this.pATzuordnenButton.Size = new System.Drawing.Size(116, 23);
            this.pATzuordnenButton.TabIndex = 2;
            this.pATzuordnenButton.Text = "Ordne pAT zu";
            this.pATzuordnenButton.UseVisualStyleBackColor = true;
            this.pATzuordnenButton.Click += new System.EventHandler(this.pATzuordnenButton_Click);
            // 
            // namePATtextBox
            // 
            this.namePATtextBox.Location = new System.Drawing.Point(18, 32);
            this.namePATtextBox.Name = "namePATtextBox";
            this.namePATtextBox.Size = new System.Drawing.Size(246, 20);
            this.namePATtextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(249, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "bislang wurde noch kein personeller AT zugeordnet";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.groupBox3.Controls.Add(this.nameAwStextBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.awSzuordnenButton);
            this.groupBox3.Location = new System.Drawing.Point(6, 100);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(279, 88);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Zugeordnetes AwS";
            // 
            // nameAwStextBox
            // 
            this.nameAwStextBox.Location = new System.Drawing.Point(18, 33);
            this.nameAwStextBox.Name = "nameAwStextBox";
            this.nameAwStextBox.Size = new System.Drawing.Size(246, 20);
            this.nameAwStextBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "bislang wurde noch kein AwS zugeordnet";
            // 
            // awSzuordnenButton
            // 
            this.awSzuordnenButton.Location = new System.Drawing.Point(83, 59);
            this.awSzuordnenButton.Name = "awSzuordnenButton";
            this.awSzuordnenButton.Size = new System.Drawing.Size(116, 23);
            this.awSzuordnenButton.TabIndex = 0;
            this.awSzuordnenButton.Text = "Ordne AwS zu";
            this.awSzuordnenButton.UseVisualStyleBackColor = true;
            this.awSzuordnenButton.Click += new System.EventHandler(this.awSzuordnenButton_Click);
            // 
            // awsGrenzenIAStabPage
            // 
            this.awsGrenzenIAStabPage.Controls.Add(this.pictureBox10);
            this.awsGrenzenIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.awsGrenzenIAStabPage.Name = "awsGrenzenIAStabPage";
            this.awsGrenzenIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.awsGrenzenIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.awsGrenzenIAStabPage.TabIndex = 12;
            this.awsGrenzenIAStabPage.Text = "(2.1) AwS-Grenzen (IAS)";
            this.awsGrenzenIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(3, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(950, 680);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 0;
            this.pictureBox10.TabStop = false;
            // 
            // awsGrenzenTabPage
            // 
            this.awsGrenzenTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.awsGrenzenTabPage.Controls.Add(this.pictureBox11);
            this.awsGrenzenTabPage.Controls.Add(this.groupBox9);
            this.awsGrenzenTabPage.Controls.Add(this.groupBox8);
            this.awsGrenzenTabPage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.awsGrenzenTabPage.Location = new System.Drawing.Point(4, 22);
            this.awsGrenzenTabPage.Name = "awsGrenzenTabPage";
            this.awsGrenzenTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.awsGrenzenTabPage.Size = new System.Drawing.Size(956, 686);
            this.awsGrenzenTabPage.TabIndex = 11;
            this.awsGrenzenTabPage.Text = "(2.2) AwS-Grenzen";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(286, 177);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(33, 26);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 2;
            this.pictureBox11.TabStop = false;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.unterstuetzendeBOlistBox);
            this.groupBox9.Location = new System.Drawing.Point(325, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(274, 368);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Unterstützende betriebliche Objekte";
            // 
            // unterstuetzendeBOlistBox
            // 
            this.unterstuetzendeBOlistBox.FormattingEnabled = true;
            this.unterstuetzendeBOlistBox.Location = new System.Drawing.Point(6, 19);
            this.unterstuetzendeBOlistBox.Name = "unterstuetzendeBOlistBox";
            this.unterstuetzendeBOlistBox.Size = new System.Drawing.Size(260, 342);
            this.unterstuetzendeBOlistBox.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.awsListBox);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(274, 368);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Anwendungssysteme";
            // 
            // awsListBox
            // 
            this.awsListBox.FormattingEnabled = true;
            this.awsListBox.Location = new System.Drawing.Point(6, 19);
            this.awsListBox.Name = "awsListBox";
            this.awsListBox.Size = new System.Drawing.Size(260, 342);
            this.awsListBox.TabIndex = 0;
            this.awsListBox.SelectedIndexChanged += new System.EventHandler(this.awsListBox_SelectedIndexChanged);
            // 
            // sollkonzeptTabPage
            // 
            this.sollkonzeptTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.sollkonzeptTabPage.Controls.Add(this.groupBox16);
            this.sollkonzeptTabPage.Controls.Add(this.groupBox15);
            this.sollkonzeptTabPage.Controls.Add(this.groupBox14);
            this.sollkonzeptTabPage.Controls.Add(this.groupBox13);
            this.sollkonzeptTabPage.Controls.Add(this.groupBox12);
            this.sollkonzeptTabPage.Controls.Add(this.groupBox11);
            this.sollkonzeptTabPage.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sollkonzeptTabPage.Location = new System.Drawing.Point(4, 22);
            this.sollkonzeptTabPage.Name = "sollkonzeptTabPage";
            this.sollkonzeptTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.sollkonzeptTabPage.Size = new System.Drawing.Size(956, 686);
            this.sollkonzeptTabPage.TabIndex = 14;
            this.sollkonzeptTabPage.Text = "(3.1) Sollkonzept für die Inter-AwS-Transaktionen";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.groupBox19);
            this.groupBox16.Controls.Add(this.pictureBox13);
            this.groupBox16.Controls.Add(this.groupBox18);
            this.groupBox16.Controls.Add(this.groupBox17);
            this.groupBox16.Location = new System.Drawing.Point(251, 128);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(696, 548);
            this.groupBox16.TabIndex = 6;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "SOM-Unternehmensarchitektur";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.handlungsempfehlungTextBox);
            this.groupBox19.Controls.Add(this.groupBox21);
            this.groupBox19.Controls.Add(this.groupBox20);
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(125, 219);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(565, 229);
            this.groupBox19.TabIndex = 7;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Merkmale auf Ebene des Geschäftsprozessmodells";
            // 
            // handlungsempfehlungTextBox
            // 
            this.handlungsempfehlungTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.handlungsempfehlungTextBox.ForeColor = System.Drawing.Color.DarkRed;
            this.handlungsempfehlungTextBox.Location = new System.Drawing.Point(6, 200);
            this.handlungsempfehlungTextBox.Name = "handlungsempfehlungTextBox";
            this.handlungsempfehlungTextBox.ReadOnly = true;
            this.handlungsempfehlungTextBox.Size = new System.Drawing.Size(553, 20);
            this.handlungsempfehlungTextBox.TabIndex = 2;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.nutzungsdauerComboBox);
            this.groupBox21.Controls.Add(this.anpassbarkeitComboBox);
            this.groupBox21.Controls.Add(this.label16);
            this.groupBox21.Controls.Add(this.label15);
            this.groupBox21.Location = new System.Drawing.Point(6, 127);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(396, 67);
            this.groupBox21.TabIndex = 1;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Flexibilitätsanforderungen";
            // 
            // nutzungsdauerComboBox
            // 
            this.nutzungsdauerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nutzungsdauerComboBox.FormattingEnabled = true;
            this.nutzungsdauerComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "kurzfristig",
            "mittelfristig",
            "langfristig"});
            this.nutzungsdauerComboBox.Location = new System.Drawing.Point(269, 39);
            this.nutzungsdauerComboBox.Name = "nutzungsdauerComboBox";
            this.nutzungsdauerComboBox.Size = new System.Drawing.Size(121, 21);
            this.nutzungsdauerComboBox.TabIndex = 3;
            this.nutzungsdauerComboBox.SelectedIndexChanged += new System.EventHandler(this.nutzungsdauerComboBox_SelectedIndexChanged);
            // 
            // anpassbarkeitComboBox
            // 
            this.anpassbarkeitComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.anpassbarkeitComboBox.FormattingEnabled = true;
            this.anpassbarkeitComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "niedrig",
            "moderat",
            "hoch"});
            this.anpassbarkeitComboBox.Location = new System.Drawing.Point(269, 12);
            this.anpassbarkeitComboBox.Name = "anpassbarkeitComboBox";
            this.anpassbarkeitComboBox.Size = new System.Drawing.Size(121, 21);
            this.anpassbarkeitComboBox.TabIndex = 2;
            this.anpassbarkeitComboBox.SelectedIndexChanged += new System.EventHandler(this.anpassbarkeitComboBox_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(45, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(205, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Nutzungsdauer der Integrationsarchitektur";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(99, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(151, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Anpassbarkeit der Transaktion";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.intensitaetComboBox);
            this.groupBox20.Controls.Add(this.groesseComboBox);
            this.groupBox20.Controls.Add(this.periodizitaetComboBox);
            this.groupBox20.Controls.Add(this.label14);
            this.groupBox20.Controls.Add(this.label13);
            this.groupBox20.Controls.Add(this.label10);
            this.groupBox20.Location = new System.Drawing.Point(6, 19);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(396, 102);
            this.groupBox20.TabIndex = 0;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Leistungsanforderungen";
            // 
            // intensitaetComboBox
            // 
            this.intensitaetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.intensitaetComboBox.FormattingEnabled = true;
            this.intensitaetComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "niedrig",
            "moderat",
            "hoch"});
            this.intensitaetComboBox.Location = new System.Drawing.Point(269, 75);
            this.intensitaetComboBox.Name = "intensitaetComboBox";
            this.intensitaetComboBox.Size = new System.Drawing.Size(121, 21);
            this.intensitaetComboBox.TabIndex = 5;
            this.intensitaetComboBox.SelectedIndexChanged += new System.EventHandler(this.intensitaetComboBox_SelectedIndexChanged);
            // 
            // groesseComboBox
            // 
            this.groesseComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groesseComboBox.FormattingEnabled = true;
            this.groesseComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "niedrig",
            "moderat",
            "hoch"});
            this.groesseComboBox.Location = new System.Drawing.Point(269, 46);
            this.groesseComboBox.Name = "groesseComboBox";
            this.groesseComboBox.Size = new System.Drawing.Size(121, 21);
            this.groesseComboBox.TabIndex = 4;
            this.groesseComboBox.SelectedIndexChanged += new System.EventHandler(this.groesseComboBox_SelectedIndexChanged);
            // 
            // periodizitaetComboBox
            // 
            this.periodizitaetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.periodizitaetComboBox.FormattingEnabled = true;
            this.periodizitaetComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "niedrig",
            "moderat",
            "hoch"});
            this.periodizitaetComboBox.Location = new System.Drawing.Point(269, 19);
            this.periodizitaetComboBox.Name = "periodizitaetComboBox";
            this.periodizitaetComboBox.Size = new System.Drawing.Size(121, 21);
            this.periodizitaetComboBox.TabIndex = 3;
            this.periodizitaetComboBox.SelectedIndexChanged += new System.EventHandler(this.periodizitaetComboBox_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(238, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Intensität bei der Durchführung einer Transaktion";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(47, 49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(203, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Größe einer zu übertragenden AO-Instanz";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(154, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Periodizität des GP";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(6, 19);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(110, 523);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox13.TabIndex = 0;
            this.pictureBox13.TabStop = false;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.einfachheitCheckBox);
            this.groupBox18.Controls.Add(this.knowHowCheckBox);
            this.groupBox18.Controls.Add(this.interoperabilityCheckBox);
            this.groupBox18.Location = new System.Drawing.Point(125, 454);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(565, 88);
            this.groupBox18.TabIndex = 2;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Merkmale auf Ebene der Ressourcenebene";
            // 
            // einfachheitCheckBox
            // 
            this.einfachheitCheckBox.AutoSize = true;
            this.einfachheitCheckBox.Location = new System.Drawing.Point(6, 66);
            this.einfachheitCheckBox.Name = "einfachheitCheckBox";
            this.einfachheitCheckBox.Size = new System.Drawing.Size(177, 17);
            this.einfachheitCheckBox.TabIndex = 2;
            this.einfachheitCheckBox.Text = "Einfachheit der Implementierung";
            this.einfachheitCheckBox.UseVisualStyleBackColor = true;
            this.einfachheitCheckBox.CheckedChanged += new System.EventHandler(this.einfachheitCheckBox_CheckedChanged);
            // 
            // knowHowCheckBox
            // 
            this.knowHowCheckBox.AutoSize = true;
            this.knowHowCheckBox.Location = new System.Drawing.Point(6, 43);
            this.knowHowCheckBox.Name = "knowHowCheckBox";
            this.knowHowCheckBox.Size = new System.Drawing.Size(282, 17);
            this.knowHowCheckBox.TabIndex = 1;
            this.knowHowCheckBox.Text = "Know-How zur Realisierung der Integrationsarchitektur";
            this.knowHowCheckBox.UseVisualStyleBackColor = true;
            this.knowHowCheckBox.CheckedChanged += new System.EventHandler(this.knowHowCheckBox_CheckedChanged);
            // 
            // interoperabilityCheckBox
            // 
            this.interoperabilityCheckBox.AutoSize = true;
            this.interoperabilityCheckBox.Location = new System.Drawing.Point(6, 19);
            this.interoperabilityCheckBox.Name = "interoperabilityCheckBox";
            this.interoperabilityCheckBox.Size = new System.Drawing.Size(177, 17);
            this.interoperabilityCheckBox.TabIndex = 0;
            this.interoperabilityCheckBox.Text = "Interoperabilität involvierter AwS";
            this.interoperabilityCheckBox.UseVisualStyleBackColor = true;
            this.interoperabilityCheckBox.CheckedChanged += new System.EventHandler(this.interoperabilityCheckBox_CheckedChanged);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.kostenCheckBox);
            this.groupBox17.Controls.Add(this.datenschutzCheckBox);
            this.groupBox17.Controls.Add(this.rechtlRahmenbedCheckBox);
            this.groupBox17.Controls.Add(this.normenCheckBox);
            this.groupBox17.Controls.Add(this.interdependenzenCheckBox);
            this.groupBox17.Controls.Add(this.vorgabeCheckBox);
            this.groupBox17.Controls.Add(this.strategieCheckBox);
            this.groupBox17.Location = new System.Drawing.Point(125, 19);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(565, 194);
            this.groupBox17.TabIndex = 1;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Merkmale auf Ebene des Unternehmensplans";
            // 
            // kostenCheckBox
            // 
            this.kostenCheckBox.AutoSize = true;
            this.kostenCheckBox.Location = new System.Drawing.Point(6, 161);
            this.kostenCheckBox.Name = "kostenCheckBox";
            this.kostenCheckBox.Size = new System.Drawing.Size(268, 17);
            this.kostenCheckBox.TabIndex = 6;
            this.kostenCheckBox.Text = "Kosten der Implementierung sowie Kostenvorgaben";
            this.kostenCheckBox.UseVisualStyleBackColor = true;
            this.kostenCheckBox.CheckedChanged += new System.EventHandler(this.kostenCheckBox_CheckedChanged);
            // 
            // datenschutzCheckBox
            // 
            this.datenschutzCheckBox.AutoSize = true;
            this.datenschutzCheckBox.Location = new System.Drawing.Point(6, 137);
            this.datenschutzCheckBox.Name = "datenschutzCheckBox";
            this.datenschutzCheckBox.Size = new System.Drawing.Size(526, 17);
            this.datenschutzCheckBox.TabIndex = 5;
            this.datenschutzCheckBox.Text = "Bestimmungen zu Datenschutz und Datensicherheit (Zugriffsberechtigungen auf AwS m" +
    "it sensiblen Daten)";
            this.datenschutzCheckBox.UseVisualStyleBackColor = true;
            this.datenschutzCheckBox.CheckedChanged += new System.EventHandler(this.datenschutzCheckBox_CheckedChanged);
            // 
            // rechtlRahmenbedCheckBox
            // 
            this.rechtlRahmenbedCheckBox.AutoSize = true;
            this.rechtlRahmenbedCheckBox.Location = new System.Drawing.Point(6, 113);
            this.rechtlRahmenbedCheckBox.Name = "rechtlRahmenbedCheckBox";
            this.rechtlRahmenbedCheckBox.Size = new System.Drawing.Size(291, 17);
            this.rechtlRahmenbedCheckBox.TabIndex = 4;
            this.rechtlRahmenbedCheckBox.Text = "rechtliche Rahmenbedingungen, wie Gesetzesvorgaben";
            this.rechtlRahmenbedCheckBox.UseVisualStyleBackColor = true;
            this.rechtlRahmenbedCheckBox.CheckedChanged += new System.EventHandler(this.rechtlRahmenbedCheckBox_CheckedChanged);
            // 
            // normenCheckBox
            // 
            this.normenCheckBox.AutoSize = true;
            this.normenCheckBox.Location = new System.Drawing.Point(6, 89);
            this.normenCheckBox.Name = "normenCheckBox";
            this.normenCheckBox.Size = new System.Drawing.Size(137, 17);
            this.normenCheckBox.TabIndex = 3;
            this.normenCheckBox.Text = "Einhaltung von Normen";
            this.normenCheckBox.UseVisualStyleBackColor = true;
            this.normenCheckBox.CheckedChanged += new System.EventHandler(this.normenCheckBox_CheckedChanged);
            // 
            // interdependenzenCheckBox
            // 
            this.interdependenzenCheckBox.AutoSize = true;
            this.interdependenzenCheckBox.Location = new System.Drawing.Point(6, 65);
            this.interdependenzenCheckBox.Name = "interdependenzenCheckBox";
            this.interdependenzenCheckBox.Size = new System.Drawing.Size(392, 17);
            this.interdependenzenCheckBox.TabIndex = 2;
            this.interdependenzenCheckBox.Text = "Interdependenzen mit anderen laufenden Projekten (Multiprojektmanagement)";
            this.interdependenzenCheckBox.UseVisualStyleBackColor = true;
            this.interdependenzenCheckBox.CheckedChanged += new System.EventHandler(this.interdependenzenCheckBox_CheckedChanged);
            // 
            // vorgabeCheckBox
            // 
            this.vorgabeCheckBox.AutoSize = true;
            this.vorgabeCheckBox.Location = new System.Drawing.Point(6, 42);
            this.vorgabeCheckBox.Name = "vorgabeCheckBox";
            this.vorgabeCheckBox.Size = new System.Drawing.Size(217, 17);
            this.vorgabeCheckBox.TabIndex = 1;
            this.vorgabeCheckBox.Text = "Vorgabe bei den zu verwendenden AwS";
            this.vorgabeCheckBox.UseVisualStyleBackColor = true;
            this.vorgabeCheckBox.CheckedChanged += new System.EventHandler(this.vorgabeCheckBox_CheckedChanged);
            // 
            // strategieCheckBox
            // 
            this.strategieCheckBox.AutoSize = true;
            this.strategieCheckBox.Location = new System.Drawing.Point(6, 19);
            this.strategieCheckBox.Name = "strategieCheckBox";
            this.strategieCheckBox.Size = new System.Drawing.Size(507, 17);
            this.strategieCheckBox.TabIndex = 0;
            this.strategieCheckBox.Text = "Strategie bei Adoption und Einsatz von Technologien und die zur Auswahl stehenden" +
    " Basismaschinen";
            this.strategieCheckBox.UseVisualStyleBackColor = true;
            this.strategieCheckBox.CheckedChanged += new System.EventHandler(this.strategieCheckBox_CheckedChanged);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label12);
            this.groupBox15.Controls.Add(this.label11);
            this.groupBox15.Controls.Add(this.sollAutomatisierungComboBox);
            this.groupBox15.Controls.Add(this.istAutomatisierungComboBox);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.ForeColor = System.Drawing.Color.DarkRed;
            this.groupBox15.Location = new System.Drawing.Point(6, 605);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(239, 71);
            this.groupBox15.TabIndex = 4;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Automatisierung";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Soll-Automat.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ist-Automat.";
            // 
            // sollAutomatisierungComboBox
            // 
            this.sollAutomatisierungComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sollAutomatisierungComboBox.FormattingEnabled = true;
            this.sollAutomatisierungComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "zu automatisieren",
            "nicht zu automatisieren"});
            this.sollAutomatisierungComboBox.Location = new System.Drawing.Point(114, 40);
            this.sollAutomatisierungComboBox.Name = "sollAutomatisierungComboBox";
            this.sollAutomatisierungComboBox.Size = new System.Drawing.Size(121, 21);
            this.sollAutomatisierungComboBox.TabIndex = 1;
            this.sollAutomatisierungComboBox.SelectedIndexChanged += new System.EventHandler(this.sollAutomatisierungComboBox_SelectedIndexChanged);
            // 
            // istAutomatisierungComboBox
            // 
            this.istAutomatisierungComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.istAutomatisierungComboBox.FormattingEnabled = true;
            this.istAutomatisierungComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "automatisiert",
            "nicht automatisiert"});
            this.istAutomatisierungComboBox.Location = new System.Drawing.Point(114, 13);
            this.istAutomatisierungComboBox.Name = "istAutomatisierungComboBox";
            this.istAutomatisierungComboBox.Size = new System.Drawing.Size(121, 21);
            this.istAutomatisierungComboBox.TabIndex = 0;
            this.istAutomatisierungComboBox.SelectedIndexChanged += new System.EventHandler(this.istAutomatisierungComboBox_SelectedIndexChanged);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.aoTextBox);
            this.groupBox14.Controls.Add(this.addAObutton);
            this.groupBox14.Controls.Add(this.delteAObutton);
            this.groupBox14.Controls.Add(this.aoListBox);
            this.groupBox14.Location = new System.Drawing.Point(6, 341);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(239, 258);
            this.groupBox14.TabIndex = 3;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Überlappende Aufgabenobjekte";
            // 
            // aoTextBox
            // 
            this.aoTextBox.Location = new System.Drawing.Point(6, 231);
            this.aoTextBox.Name = "aoTextBox";
            this.aoTextBox.Size = new System.Drawing.Size(129, 20);
            this.aoTextBox.TabIndex = 3;
            // 
            // addAObutton
            // 
            this.addAObutton.Image = ((System.Drawing.Image)(resources.GetObject("addAObutton.Image")));
            this.addAObutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addAObutton.Location = new System.Drawing.Point(141, 229);
            this.addAObutton.Name = "addAObutton";
            this.addAObutton.Size = new System.Drawing.Size(92, 23);
            this.addAObutton.TabIndex = 2;
            this.addAObutton.Text = "Hinzufügen";
            this.addAObutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addAObutton.UseVisualStyleBackColor = true;
            this.addAObutton.Click += new System.EventHandler(this.addAObutton_Click);
            // 
            // delteAObutton
            // 
            this.delteAObutton.Image = ((System.Drawing.Image)(resources.GetObject("delteAObutton.Image")));
            this.delteAObutton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delteAObutton.Location = new System.Drawing.Point(141, 193);
            this.delteAObutton.Name = "delteAObutton";
            this.delteAObutton.Size = new System.Drawing.Size(92, 23);
            this.delteAObutton.TabIndex = 1;
            this.delteAObutton.Text = "Löschen";
            this.delteAObutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.delteAObutton.UseVisualStyleBackColor = true;
            this.delteAObutton.Click += new System.EventHandler(this.delteAObutton_Click);
            // 
            // aoListBox
            // 
            this.aoListBox.FormattingEnabled = true;
            this.aoListBox.Location = new System.Drawing.Point(6, 14);
            this.aoListBox.Name = "aoListBox";
            this.aoListBox.Size = new System.Drawing.Size(227, 173);
            this.aoListBox.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.lvCheckBox);
            this.groupBox13.Controls.Add(this.identityAOcheckBox);
            this.groupBox13.Controls.Add(this.gleichheitAOcheckBox);
            this.groupBox13.Controls.Add(this.reihenfolgebezCheckBox);
            this.groupBox13.Location = new System.Drawing.Point(720, 7);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(227, 115);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Aufgabenintegrationsmuster (AIM)";
            // 
            // lvCheckBox
            // 
            this.lvCheckBox.AutoSize = true;
            this.lvCheckBox.Location = new System.Drawing.Point(6, 89);
            this.lvCheckBox.Name = "lvCheckBox";
            this.lvCheckBox.Size = new System.Drawing.Size(207, 17);
            this.lvCheckBox.TabIndex = 5;
            this.lvCheckBox.Text = "Überlappende (Teil-)Lösungsverfahren";
            this.lvCheckBox.UseVisualStyleBackColor = true;
            this.lvCheckBox.CheckedChanged += new System.EventHandler(this.lvCheckBox_CheckedChanged);
            // 
            // identityAOcheckBox
            // 
            this.identityAOcheckBox.AutoSize = true;
            this.identityAOcheckBox.Location = new System.Drawing.Point(6, 66);
            this.identityAOcheckBox.Name = "identityAOcheckBox";
            this.identityAOcheckBox.Size = new System.Drawing.Size(177, 17);
            this.identityAOcheckBox.TabIndex = 4;
            this.identityAOcheckBox.Text = "Partielle Identität (AO-Instanzen)";
            this.identityAOcheckBox.UseVisualStyleBackColor = true;
            this.identityAOcheckBox.CheckedChanged += new System.EventHandler(this.identityAOcheckBox_CheckedChanged);
            // 
            // gleichheitAOcheckBox
            // 
            this.gleichheitAOcheckBox.AutoSize = true;
            this.gleichheitAOcheckBox.Location = new System.Drawing.Point(6, 42);
            this.gleichheitAOcheckBox.Name = "gleichheitAOcheckBox";
            this.gleichheitAOcheckBox.Size = new System.Drawing.Size(170, 17);
            this.gleichheitAOcheckBox.TabIndex = 3;
            this.gleichheitAOcheckBox.Text = "Partielle Gleichheit (AO-Typen)";
            this.gleichheitAOcheckBox.UseVisualStyleBackColor = true;
            this.gleichheitAOcheckBox.CheckedChanged += new System.EventHandler(this.gleichheitAOcheckBox_CheckedChanged);
            // 
            // reihenfolgebezCheckBox
            // 
            this.reihenfolgebezCheckBox.AutoSize = true;
            this.reihenfolgebezCheckBox.Location = new System.Drawing.Point(6, 19);
            this.reihenfolgebezCheckBox.Name = "reihenfolgebezCheckBox";
            this.reihenfolgebezCheckBox.Size = new System.Drawing.Size(132, 17);
            this.reihenfolgebezCheckBox.TabIndex = 1;
            this.reihenfolgebezCheckBox.Text = "Reihenfolgebeziehung";
            this.reihenfolgebezCheckBox.UseVisualStyleBackColor = true;
            this.reihenfolgebezCheckBox.CheckedChanged += new System.EventHandler(this.reihenfolgebezCheckBox_CheckedChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label25);
            this.groupBox12.Controls.Add(this.receivingAwSlabel);
            this.groupBox12.Controls.Add(this.label23);
            this.groupBox12.Controls.Add(this.receivingPATlabel);
            this.groupBox12.Controls.Add(this.sendingAwSlabel);
            this.groupBox12.Controls.Add(this.label22);
            this.groupBox12.Controls.Add(this.sendingPATlabel);
            this.groupBox12.Controls.Add(this.label18);
            this.groupBox12.Controls.Add(this.receivingBOlabel);
            this.groupBox12.Controls.Add(this.sendingBOlabel);
            this.groupBox12.Controls.Add(this.label9);
            this.groupBox12.Controls.Add(this.label8);
            this.groupBox12.Location = new System.Drawing.Point(251, 7);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(463, 115);
            this.groupBox12.TabIndex = 1;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Der Transaktion zugeordnete betriebliche Objekte";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(130, 79);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "AwS";
            // 
            // receivingAwSlabel
            // 
            this.receivingAwSlabel.AutoSize = true;
            this.receivingAwSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivingAwSlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivingAwSlabel.Location = new System.Drawing.Point(165, 79);
            this.receivingAwSlabel.Name = "receivingAwSlabel";
            this.receivingAwSlabel.Size = new System.Drawing.Size(47, 13);
            this.receivingAwSlabel.TabIndex = 12;
            this.receivingAwSlabel.Text = "Sender";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(112, 92);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 11;
            this.label23.Text = "pers. AT";
            // 
            // receivingPATlabel
            // 
            this.receivingPATlabel.AutoSize = true;
            this.receivingPATlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivingPATlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivingPATlabel.Location = new System.Drawing.Point(165, 92);
            this.receivingPATlabel.Name = "receivingPATlabel";
            this.receivingPATlabel.Size = new System.Drawing.Size(47, 13);
            this.receivingPATlabel.TabIndex = 10;
            this.receivingPATlabel.Text = "Sender";
            // 
            // sendingAwSlabel
            // 
            this.sendingAwSlabel.AutoSize = true;
            this.sendingAwSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendingAwSlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendingAwSlabel.Location = new System.Drawing.Point(165, 29);
            this.sendingAwSlabel.Name = "sendingAwSlabel";
            this.sendingAwSlabel.Size = new System.Drawing.Size(47, 13);
            this.sendingAwSlabel.TabIndex = 9;
            this.sendingAwSlabel.Text = "Sender";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(112, 42);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "pers. AT";
            // 
            // sendingPATlabel
            // 
            this.sendingPATlabel.AutoSize = true;
            this.sendingPATlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendingPATlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendingPATlabel.Location = new System.Drawing.Point(165, 42);
            this.sendingPATlabel.Name = "sendingPATlabel";
            this.sendingPATlabel.Size = new System.Drawing.Size(47, 13);
            this.sendingPATlabel.TabIndex = 7;
            this.sendingPATlabel.Text = "Sender";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(130, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "AwS";
            // 
            // receivingBOlabel
            // 
            this.receivingBOlabel.AutoSize = true;
            this.receivingBOlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivingBOlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivingBOlabel.Location = new System.Drawing.Point(165, 66);
            this.receivingBOlabel.Name = "receivingBOlabel";
            this.receivingBOlabel.Size = new System.Drawing.Size(67, 13);
            this.receivingBOlabel.TabIndex = 5;
            this.receivingBOlabel.Text = "Empfänger";
            // 
            // sendingBOlabel
            // 
            this.sendingBOlabel.AutoSize = true;
            this.sendingBOlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendingBOlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendingBOlabel.Location = new System.Drawing.Point(165, 16);
            this.sendingBOlabel.Name = "sendingBOlabel";
            this.sendingBOlabel.Size = new System.Drawing.Size(47, 13);
            this.sendingBOlabel.TabIndex = 4;
            this.sendingBOlabel.Text = "Sender";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(153, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = ">empfangendes betr. Obj.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(27, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "sendendes betr. Obj.<";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.interAwSTAsListBox);
            this.groupBox11.Location = new System.Drawing.Point(6, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(239, 328);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Inter-AwS-Transaktionen";
            // 
            // interAwSTAsListBox
            // 
            this.interAwSTAsListBox.FormattingEnabled = true;
            this.interAwSTAsListBox.Location = new System.Drawing.Point(6, 19);
            this.interAwSTAsListBox.Name = "interAwSTAsListBox";
            this.interAwSTAsListBox.Size = new System.Drawing.Size(227, 303);
            this.interAwSTAsListBox.TabIndex = 0;
            this.interAwSTAsListBox.SelectedIndexChanged += new System.EventHandler(this.interAwSTAsListBox_SelectedIndexChanged);
            // 
            // sollkonzeptIAStabPage
            // 
            this.sollkonzeptIAStabPage.Controls.Add(this.pictureBox3);
            this.sollkonzeptIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.sollkonzeptIAStabPage.Name = "sollkonzeptIAStabPage";
            this.sollkonzeptIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.sollkonzeptIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.sollkonzeptIAStabPage.TabIndex = 2;
            this.sollkonzeptIAStabPage.Text = "(3.3) Sollkonzept (IAS)";
            this.sollkonzeptIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(3, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(950, 680);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // istZustandIAStabPage
            // 
            this.istZustandIAStabPage.Controls.Add(this.pictureBox4);
            this.istZustandIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.istZustandIAStabPage.Name = "istZustandIAStabPage";
            this.istZustandIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.istZustandIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.istZustandIAStabPage.TabIndex = 3;
            this.istZustandIAStabPage.Text = "(4.1) Ist-Zustand (IAS)";
            this.istZustandIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(950, 680);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // deltaTAsTabPage
            // 
            this.deltaTAsTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.deltaTAsTabPage.Controls.Add(this.groupBox28);
            this.deltaTAsTabPage.Controls.Add(this.groupBox24);
            this.deltaTAsTabPage.Controls.Add(this.pictureBox15);
            this.deltaTAsTabPage.Controls.Add(this.pictureBox14);
            this.deltaTAsTabPage.Controls.Add(this.groupBox23);
            this.deltaTAsTabPage.Controls.Add(this.groupBox22);
            this.deltaTAsTabPage.Controls.Add(this.groupBox10);
            this.deltaTAsTabPage.Location = new System.Drawing.Point(4, 22);
            this.deltaTAsTabPage.Name = "deltaTAsTabPage";
            this.deltaTAsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.deltaTAsTabPage.Size = new System.Drawing.Size(956, 686);
            this.deltaTAsTabPage.TabIndex = 13;
            this.deltaTAsTabPage.Text = "(4.2) Delta Transaktionen";
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.groupBox36);
            this.groupBox28.Controls.Add(this.groupBox34);
            this.groupBox28.Controls.Add(this.groupBox35);
            this.groupBox28.Controls.Add(this.groupBox33);
            this.groupBox28.Controls.Add(this.groupBox32);
            this.groupBox28.Controls.Add(this.groupBox31);
            this.groupBox28.Controls.Add(this.groupBox30);
            this.groupBox28.Controls.Add(this.hinweiseTextBox);
            this.groupBox28.Location = new System.Drawing.Point(657, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(293, 674);
            this.groupBox28.TabIndex = 5;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Hinweise bei der Gestaltung der Integration";
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.ueberlappendeLVsTextBox);
            this.groupBox36.Location = new System.Drawing.Point(6, 286);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(281, 69);
            this.groupBox36.TabIndex = 7;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Auswirkung auf partiell gleiche Lösungsverfahren";
            // 
            // ueberlappendeLVsTextBox
            // 
            this.ueberlappendeLVsTextBox.Location = new System.Drawing.Point(6, 19);
            this.ueberlappendeLVsTextBox.Multiline = true;
            this.ueberlappendeLVsTextBox.Name = "ueberlappendeLVsTextBox";
            this.ueberlappendeLVsTextBox.ReadOnly = true;
            this.ueberlappendeLVsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ueberlappendeLVsTextBox.Size = new System.Drawing.Size(269, 42);
            this.ueberlappendeLVsTextBox.TabIndex = 0;
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.homogAOInstanzTextBox);
            this.groupBox34.Location = new System.Drawing.Point(6, 211);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(281, 69);
            this.groupBox34.TabIndex = 6;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Homogenisierung partiell identischer AO-Instanzen";
            // 
            // homogAOInstanzTextBox
            // 
            this.homogAOInstanzTextBox.Location = new System.Drawing.Point(6, 19);
            this.homogAOInstanzTextBox.Multiline = true;
            this.homogAOInstanzTextBox.Name = "homogAOInstanzTextBox";
            this.homogAOInstanzTextBox.ReadOnly = true;
            this.homogAOInstanzTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.homogAOInstanzTextBox.Size = new System.Drawing.Size(269, 42);
            this.homogAOInstanzTextBox.TabIndex = 0;
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.homogAOTypenTextBox);
            this.groupBox35.Location = new System.Drawing.Point(6, 136);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(281, 69);
            this.groupBox35.TabIndex = 5;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Homogenisierung partiell gleicher AO-Typen";
            // 
            // homogAOTypenTextBox
            // 
            this.homogAOTypenTextBox.Location = new System.Drawing.Point(6, 19);
            this.homogAOTypenTextBox.Multiline = true;
            this.homogAOTypenTextBox.Name = "homogAOTypenTextBox";
            this.homogAOTypenTextBox.ReadOnly = true;
            this.homogAOTypenTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.homogAOTypenTextBox.Size = new System.Drawing.Size(269, 42);
            this.homogAOTypenTextBox.TabIndex = 0;
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this.restriktionenTextBox);
            this.groupBox33.Location = new System.Drawing.Point(6, 372);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(281, 100);
            this.groupBox33.TabIndex = 4;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Restriktionen / Voraussetzungen";
            // 
            // restriktionenTextBox
            // 
            this.restriktionenTextBox.Location = new System.Drawing.Point(6, 19);
            this.restriktionenTextBox.Multiline = true;
            this.restriktionenTextBox.Name = "restriktionenTextBox";
            this.restriktionenTextBox.ReadOnly = true;
            this.restriktionenTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.restriktionenTextBox.Size = new System.Drawing.Size(269, 75);
            this.restriktionenTextBox.TabIndex = 0;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.empfehlungTextBox);
            this.groupBox32.Location = new System.Drawing.Point(6, 574);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(281, 100);
            this.groupBox32.TabIndex = 3;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Empehlung / Anwendungseinsatz";
            // 
            // empfehlungTextBox
            // 
            this.empfehlungTextBox.Location = new System.Drawing.Point(6, 19);
            this.empfehlungTextBox.Multiline = true;
            this.empfehlungTextBox.Name = "empfehlungTextBox";
            this.empfehlungTextBox.ReadOnly = true;
            this.empfehlungTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.empfehlungTextBox.Size = new System.Drawing.Size(269, 75);
            this.empfehlungTextBox.TabIndex = 0;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.mmk2TextBox);
            this.groupBox31.Location = new System.Drawing.Point(149, 478);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(137, 90);
            this.groupBox31.TabIndex = 2;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Auswirkungen auf MMK2";
            // 
            // mmk2TextBox
            // 
            this.mmk2TextBox.Location = new System.Drawing.Point(6, 26);
            this.mmk2TextBox.Multiline = true;
            this.mmk2TextBox.Name = "mmk2TextBox";
            this.mmk2TextBox.ReadOnly = true;
            this.mmk2TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mmk2TextBox.Size = new System.Drawing.Size(125, 58);
            this.mmk2TextBox.TabIndex = 1;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.mmk1TextBox);
            this.groupBox30.Location = new System.Drawing.Point(6, 478);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(137, 90);
            this.groupBox30.TabIndex = 1;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Auswirkungen auf MMK1";
            // 
            // mmk1TextBox
            // 
            this.mmk1TextBox.Location = new System.Drawing.Point(6, 26);
            this.mmk1TextBox.Multiline = true;
            this.mmk1TextBox.Name = "mmk1TextBox";
            this.mmk1TextBox.ReadOnly = true;
            this.mmk1TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mmk1TextBox.Size = new System.Drawing.Size(125, 58);
            this.mmk1TextBox.TabIndex = 0;
            // 
            // hinweiseTextBox
            // 
            this.hinweiseTextBox.Location = new System.Drawing.Point(6, 16);
            this.hinweiseTextBox.Multiline = true;
            this.hinweiseTextBox.Name = "hinweiseTextBox";
            this.hinweiseTextBox.ReadOnly = true;
            this.hinweiseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.hinweiseTextBox.Size = new System.Drawing.Size(281, 102);
            this.hinweiseTextBox.TabIndex = 0;
            this.hinweiseTextBox.Text = "Auf Konsequenzen bei der Wahl des Integrationskonzepts hinweisen. Auch den Zugrif" +
    "f des pATs auf das erforderliche AwS zur Gewinnung der Daten als Option als Hinw" +
    "eis einblenden";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.sendAwSlabel);
            this.groupBox24.Controls.Add(this.label30);
            this.groupBox24.Controls.Add(this.sendPATlabel);
            this.groupBox24.Controls.Add(this.label28);
            this.groupBox24.Controls.Add(this.receivPATlabel);
            this.groupBox24.Controls.Add(this.label26);
            this.groupBox24.Controls.Add(this.receivAwSlabel);
            this.groupBox24.Controls.Add(this.label21);
            this.groupBox24.Controls.Add(this.receivBOlabel);
            this.groupBox24.Controls.Add(this.sendBOlabel);
            this.groupBox24.Controls.Add(this.label19);
            this.groupBox24.Controls.Add(this.label20);
            this.groupBox24.Location = new System.Drawing.Point(338, 6);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(312, 118);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Involvierte betriebliche Objekte";
            // 
            // sendAwSlabel
            // 
            this.sendAwSlabel.AutoSize = true;
            this.sendAwSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendAwSlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendAwSlabel.Location = new System.Drawing.Point(165, 29);
            this.sendAwSlabel.Name = "sendAwSlabel";
            this.sendAwSlabel.Size = new System.Drawing.Size(67, 13);
            this.sendAwSlabel.TabIndex = 17;
            this.sendAwSlabel.Text = "Empfänger";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(130, 29);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 13);
            this.label30.TabIndex = 16;
            this.label30.Text = "AwS";
            // 
            // sendPATlabel
            // 
            this.sendPATlabel.AutoSize = true;
            this.sendPATlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendPATlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendPATlabel.Location = new System.Drawing.Point(165, 42);
            this.sendPATlabel.Name = "sendPATlabel";
            this.sendPATlabel.Size = new System.Drawing.Size(67, 13);
            this.sendPATlabel.TabIndex = 15;
            this.sendPATlabel.Text = "Empfänger";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(112, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "pers. AT";
            // 
            // receivPATlabel
            // 
            this.receivPATlabel.AutoSize = true;
            this.receivPATlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivPATlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivPATlabel.Location = new System.Drawing.Point(165, 95);
            this.receivPATlabel.Name = "receivPATlabel";
            this.receivPATlabel.Size = new System.Drawing.Size(67, 13);
            this.receivPATlabel.TabIndex = 13;
            this.receivPATlabel.Text = "Empfänger";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(112, 95);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 13);
            this.label26.TabIndex = 12;
            this.label26.Text = "pers. AT";
            // 
            // receivAwSlabel
            // 
            this.receivAwSlabel.AutoSize = true;
            this.receivAwSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivAwSlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivAwSlabel.Location = new System.Drawing.Point(165, 82);
            this.receivAwSlabel.Name = "receivAwSlabel";
            this.receivAwSlabel.Size = new System.Drawing.Size(67, 13);
            this.receivAwSlabel.TabIndex = 11;
            this.receivAwSlabel.Text = "Empfänger";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(130, 82);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 13);
            this.label21.TabIndex = 10;
            this.label21.Text = "AwS";
            // 
            // receivBOlabel
            // 
            this.receivBOlabel.AutoSize = true;
            this.receivBOlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.receivBOlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.receivBOlabel.Location = new System.Drawing.Point(165, 69);
            this.receivBOlabel.Name = "receivBOlabel";
            this.receivBOlabel.Size = new System.Drawing.Size(67, 13);
            this.receivBOlabel.TabIndex = 9;
            this.receivBOlabel.Text = "Empfänger";
            // 
            // sendBOlabel
            // 
            this.sendBOlabel.AutoSize = true;
            this.sendBOlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sendBOlabel.ForeColor = System.Drawing.Color.DarkBlue;
            this.sendBOlabel.Location = new System.Drawing.Point(165, 16);
            this.sendBOlabel.Name = "sendBOlabel";
            this.sendBOlabel.Size = new System.Drawing.Size(47, 13);
            this.sendBOlabel.TabIndex = 8;
            this.sendBOlabel.Text = "Sender";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(153, 13);
            this.label19.TabIndex = 7;
            this.label19.Text = ">empfangendes betr. Obj.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(27, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(132, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "sendendes betr. Obj.<";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(299, 52);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(33, 26);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 4;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(299, 239);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(33, 26);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 3;
            this.pictureBox14.TabStop = false;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.ueberlappendeAOlistBox);
            this.groupBox23.Location = new System.Drawing.Point(338, 130);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(312, 244);
            this.groupBox23.TabIndex = 2;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Überlappende Aufgabenobjekte";
            // 
            // ueberlappendeAOlistBox
            // 
            this.ueberlappendeAOlistBox.FormattingEnabled = true;
            this.ueberlappendeAOlistBox.Location = new System.Drawing.Point(6, 19);
            this.ueberlappendeAOlistBox.Name = "ueberlappendeAOlistBox";
            this.ueberlappendeAOlistBox.Size = new System.Drawing.Size(300, 212);
            this.ueberlappendeAOlistBox.TabIndex = 0;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox29);
            this.groupBox22.Controls.Add(this.integrationskonzeptEmpfehlungTextBox);
            this.groupBox22.Controls.Add(this.groupBox27);
            this.groupBox22.Controls.Add(this.groupBox26);
            this.groupBox22.Controls.Add(this.groupBox25);
            this.groupBox22.Location = new System.Drawing.Point(6, 380);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(644, 300);
            this.groupBox22.TabIndex = 1;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Unterstützung bei der Wahl des zu verwendenden Integrationskonzepts";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.integrationskonzeptComboBox);
            this.groupBox29.Location = new System.Drawing.Point(471, 219);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(173, 75);
            this.groupBox29.TabIndex = 10;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Wahl Integrationskonzept";
            // 
            // integrationskonzeptComboBox
            // 
            this.integrationskonzeptComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.integrationskonzeptComboBox.FormattingEnabled = true;
            this.integrationskonzeptComboBox.Items.AddRange(new object[] {
            "nicht gesetzt",
            "aufgabenträgerorientierte Funktionsintegration",
            "datenflussorientierte Funktionsintegration",
            "Datenintegration",
            "Objektintegration"});
            this.integrationskonzeptComboBox.Location = new System.Drawing.Point(6, 19);
            this.integrationskonzeptComboBox.Name = "integrationskonzeptComboBox";
            this.integrationskonzeptComboBox.Size = new System.Drawing.Size(161, 21);
            this.integrationskonzeptComboBox.TabIndex = 0;
            this.integrationskonzeptComboBox.SelectedIndexChanged += new System.EventHandler(this.integrationskonzeptComboBox_SelectedIndexChanged);
            // 
            // integrationskonzeptEmpfehlungTextBox
            // 
            this.integrationskonzeptEmpfehlungTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.integrationskonzeptEmpfehlungTextBox.ForeColor = System.Drawing.Color.DarkRed;
            this.integrationskonzeptEmpfehlungTextBox.Location = new System.Drawing.Point(6, 219);
            this.integrationskonzeptEmpfehlungTextBox.Multiline = true;
            this.integrationskonzeptEmpfehlungTextBox.Name = "integrationskonzeptEmpfehlungTextBox";
            this.integrationskonzeptEmpfehlungTextBox.ReadOnly = true;
            this.integrationskonzeptEmpfehlungTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.integrationskonzeptEmpfehlungTextBox.Size = new System.Drawing.Size(459, 75);
            this.integrationskonzeptEmpfehlungTextBox.TabIndex = 9;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.integritaetCheckBox);
            this.groupBox27.Controls.Add(this.kommunikationsstrukturCheckBox);
            this.groupBox27.Controls.Add(this.vorgangssteuerungCheckBox);
            this.groupBox27.Controls.Add(this.funktionsredundanzCheckBox);
            this.groupBox27.Controls.Add(this.datenredundanzCheckBox);
            this.groupBox27.Location = new System.Drawing.Point(471, 35);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(167, 178);
            this.groupBox27.TabIndex = 8;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Beherrschung der Integrationsmerkmale durch AwS-Unterstützung";
            // 
            // integritaetCheckBox
            // 
            this.integritaetCheckBox.AutoSize = true;
            this.integritaetCheckBox.Location = new System.Drawing.Point(6, 157);
            this.integritaetCheckBox.Name = "integritaetCheckBox";
            this.integritaetCheckBox.Size = new System.Drawing.Size(67, 17);
            this.integritaetCheckBox.TabIndex = 4;
            this.integritaetCheckBox.Text = "Integrität";
            this.integritaetCheckBox.UseVisualStyleBackColor = true;
            this.integritaetCheckBox.CheckedChanged += new System.EventHandler(this.integritaetCheckBox_CheckedChanged);
            // 
            // kommunikationsstrukturCheckBox
            // 
            this.kommunikationsstrukturCheckBox.AutoSize = true;
            this.kommunikationsstrukturCheckBox.Location = new System.Drawing.Point(6, 134);
            this.kommunikationsstrukturCheckBox.Name = "kommunikationsstrukturCheckBox";
            this.kommunikationsstrukturCheckBox.Size = new System.Drawing.Size(138, 17);
            this.kommunikationsstrukturCheckBox.TabIndex = 3;
            this.kommunikationsstrukturCheckBox.Text = "Kommunikationsstruktur";
            this.kommunikationsstrukturCheckBox.UseVisualStyleBackColor = true;
            this.kommunikationsstrukturCheckBox.CheckedChanged += new System.EventHandler(this.kommunikationsstrukturCheckBox_CheckedChanged);
            // 
            // vorgangssteuerungCheckBox
            // 
            this.vorgangssteuerungCheckBox.AutoSize = true;
            this.vorgangssteuerungCheckBox.Location = new System.Drawing.Point(6, 111);
            this.vorgangssteuerungCheckBox.Name = "vorgangssteuerungCheckBox";
            this.vorgangssteuerungCheckBox.Size = new System.Drawing.Size(118, 17);
            this.vorgangssteuerungCheckBox.TabIndex = 2;
            this.vorgangssteuerungCheckBox.Text = "Vorgangssteuerung";
            this.vorgangssteuerungCheckBox.UseVisualStyleBackColor = true;
            this.vorgangssteuerungCheckBox.CheckedChanged += new System.EventHandler(this.vorgangssteuerungCheckBox_CheckedChanged);
            // 
            // funktionsredundanzCheckBox
            // 
            this.funktionsredundanzCheckBox.AutoSize = true;
            this.funktionsredundanzCheckBox.Location = new System.Drawing.Point(6, 88);
            this.funktionsredundanzCheckBox.Name = "funktionsredundanzCheckBox";
            this.funktionsredundanzCheckBox.Size = new System.Drawing.Size(122, 17);
            this.funktionsredundanzCheckBox.TabIndex = 1;
            this.funktionsredundanzCheckBox.Text = "Funktionsredundanz";
            this.funktionsredundanzCheckBox.UseVisualStyleBackColor = true;
            this.funktionsredundanzCheckBox.CheckedChanged += new System.EventHandler(this.funktionsredundanzCheckBox_CheckedChanged);
            // 
            // datenredundanzCheckBox
            // 
            this.datenredundanzCheckBox.AutoSize = true;
            this.datenredundanzCheckBox.Location = new System.Drawing.Point(6, 65);
            this.datenredundanzCheckBox.Name = "datenredundanzCheckBox";
            this.datenredundanzCheckBox.Size = new System.Drawing.Size(105, 17);
            this.datenredundanzCheckBox.TabIndex = 0;
            this.datenredundanzCheckBox.Text = "Datenredundanz";
            this.datenredundanzCheckBox.UseVisualStyleBackColor = true;
            this.datenredundanzCheckBox.CheckedChanged += new System.EventHandler(this.datenredundanzCheckBox_CheckedChanged);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label43);
            this.groupBox26.Controls.Add(this.label42);
            this.groupBox26.Controls.Add(this.label41);
            this.groupBox26.Controls.Add(this.label40);
            this.groupBox26.Controls.Add(this.label39);
            this.groupBox26.Controls.Add(this.label38);
            this.groupBox26.Controls.Add(this.label37);
            this.groupBox26.Controls.Add(this.label36);
            this.groupBox26.Controls.Add(this.label35);
            this.groupBox26.Controls.Add(this.label33);
            this.groupBox26.Controls.Add(this.label34);
            this.groupBox26.Controls.Add(this.label32);
            this.groupBox26.Controls.Add(this.label31);
            this.groupBox26.Controls.Add(this.label29);
            this.groupBox26.Controls.Add(this.label17);
            this.groupBox26.Controls.Add(this.label24);
            this.groupBox26.Controls.Add(this.label27);
            this.groupBox26.Location = new System.Drawing.Point(236, 19);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(229, 194);
            this.groupBox26.TabIndex = 7;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Integrationskonzepte mit masch. Beteiligung";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(38, 128);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(16, 13);
            this.label43.TabIndex = 16;
            this.label43.Text = "◐";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(39, 151);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(14, 13);
            this.label42.TabIndex = 15;
            this.label42.Text = "●";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(114, 174);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(14, 13);
            this.label41.TabIndex = 14;
            this.label41.Text = "●";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(181, 174);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(14, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "●";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(181, 128);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(14, 13);
            this.label39.TabIndex = 12;
            this.label39.Text = "●";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(181, 151);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(14, 13);
            this.label38.TabIndex = 11;
            this.label38.Text = "●";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(174, 105);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(29, 13);
            this.label37.TabIndex = 10;
            this.label37.Text = "X | ●";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(174, 82);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 13);
            this.label36.TabIndex = 9;
            this.label36.Text = "X | ●";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(107, 82);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 13);
            this.label35.TabIndex = 8;
            this.label35.Text = "X | ●";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(181, 59);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(14, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "X";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(114, 59);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(14, 13);
            this.label34.TabIndex = 6;
            this.label34.Text = "X";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(181, 36);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(14, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "X";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(114, 36);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(14, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "X";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(39, 36);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(14, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "X";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Funktionsint.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(92, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Datenint.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(157, 16);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Objektint.";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.lvOverlappingCheckBox);
            this.groupBox25.Controls.Add(this.aoIdentityCheckBox);
            this.groupBox25.Controls.Add(this.aoGleichheitCheckBox);
            this.groupBox25.Controls.Add(this.reihenfolgeCheckBox);
            this.groupBox25.Location = new System.Drawing.Point(6, 35);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(224, 178);
            this.groupBox25.TabIndex = 6;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Aufgabenintegrationsmuster";
            // 
            // lvOverlappingCheckBox
            // 
            this.lvOverlappingCheckBox.AutoSize = true;
            this.lvOverlappingCheckBox.Enabled = false;
            this.lvOverlappingCheckBox.Location = new System.Drawing.Point(6, 88);
            this.lvOverlappingCheckBox.Name = "lvOverlappingCheckBox";
            this.lvOverlappingCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lvOverlappingCheckBox.Size = new System.Drawing.Size(207, 17);
            this.lvOverlappingCheckBox.TabIndex = 3;
            this.lvOverlappingCheckBox.Text = "Überlappende (Teil-)Lösungsverfahren";
            this.lvOverlappingCheckBox.UseVisualStyleBackColor = true;
            // 
            // aoIdentityCheckBox
            // 
            this.aoIdentityCheckBox.AutoSize = true;
            this.aoIdentityCheckBox.Enabled = false;
            this.aoIdentityCheckBox.Location = new System.Drawing.Point(36, 65);
            this.aoIdentityCheckBox.Name = "aoIdentityCheckBox";
            this.aoIdentityCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.aoIdentityCheckBox.Size = new System.Drawing.Size(177, 17);
            this.aoIdentityCheckBox.TabIndex = 2;
            this.aoIdentityCheckBox.Text = "Partielle Identität (AO-Instanzen)";
            this.aoIdentityCheckBox.UseVisualStyleBackColor = true;
            // 
            // aoGleichheitCheckBox
            // 
            this.aoGleichheitCheckBox.AutoSize = true;
            this.aoGleichheitCheckBox.Enabled = false;
            this.aoGleichheitCheckBox.Location = new System.Drawing.Point(43, 42);
            this.aoGleichheitCheckBox.Name = "aoGleichheitCheckBox";
            this.aoGleichheitCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.aoGleichheitCheckBox.Size = new System.Drawing.Size(170, 17);
            this.aoGleichheitCheckBox.TabIndex = 1;
            this.aoGleichheitCheckBox.Text = "Partielle Gleichheit (AO-Typen)";
            this.aoGleichheitCheckBox.UseVisualStyleBackColor = true;
            // 
            // reihenfolgeCheckBox
            // 
            this.reihenfolgeCheckBox.AutoSize = true;
            this.reihenfolgeCheckBox.Enabled = false;
            this.reihenfolgeCheckBox.Location = new System.Drawing.Point(81, 19);
            this.reihenfolgeCheckBox.Name = "reihenfolgeCheckBox";
            this.reihenfolgeCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.reihenfolgeCheckBox.Size = new System.Drawing.Size(132, 17);
            this.reihenfolgeCheckBox.TabIndex = 0;
            this.reihenfolgeCheckBox.Text = "Reihenfolgebeziehung";
            this.reihenfolgeCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.deltaTAsListBox);
            this.groupBox10.Location = new System.Drawing.Point(6, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(287, 368);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Anzeige der Transaktionen, für die ein Delta besteht";
            // 
            // deltaTAsListBox
            // 
            this.deltaTAsListBox.ForeColor = System.Drawing.Color.Red;
            this.deltaTAsListBox.FormattingEnabled = true;
            this.deltaTAsListBox.Location = new System.Drawing.Point(6, 13);
            this.deltaTAsListBox.Name = "deltaTAsListBox";
            this.deltaTAsListBox.Size = new System.Drawing.Size(272, 342);
            this.deltaTAsListBox.TabIndex = 0;
            this.deltaTAsListBox.SelectedIndexChanged += new System.EventHandler(this.deltaTAsListBox_SelectedIndexChanged);
            // 
            // zielkonzeptIAStabPage
            // 
            this.zielkonzeptIAStabPage.Controls.Add(this.pictureBox5);
            this.zielkonzeptIAStabPage.Location = new System.Drawing.Point(4, 22);
            this.zielkonzeptIAStabPage.Name = "zielkonzeptIAStabPage";
            this.zielkonzeptIAStabPage.Padding = new System.Windows.Forms.Padding(3);
            this.zielkonzeptIAStabPage.Size = new System.Drawing.Size(956, 686);
            this.zielkonzeptIAStabPage.TabIndex = 4;
            this.zielkonzeptIAStabPage.Text = "(5.1) Zielkonzept (IAS)";
            this.zielkonzeptIAStabPage.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(3, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(950, 680);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // BetrieblicheObjekteView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 740);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BetrieblicheObjekteView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dialog zum Bearbeiten betrieblicher Objekte";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.somGpModellIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tasBetrObjTabPage.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.zuordnungAtIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.zuordnungATtabPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.awsGrenzenIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.awsGrenzenTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.sollkonzeptTabPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.sollkonzeptIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.istZustandIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.deltaTAsTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.zielkonzeptIAStabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button hinzufuegenBetrObjButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox vorhandeneBOlistBox;
        private System.Windows.Forms.Button openBetrObjButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button deleteBetrObjButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage somGpModellIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage sollkonzeptIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TabPage istZustandIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TabPage zielkonzeptIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.TabPage zuordnungATtabPage;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button pATzuordnenButton;
        private System.Windows.Forms.TextBox namePATtextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox nameAwStextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button awSzuordnenButton;
        private System.Windows.Forms.TabPage tasBetrObjTabPage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox taTextBox;
        private System.Windows.Forms.Button addTAbutton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button deleteTAbutton;
        private System.Windows.Forms.ListBox tasListBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox sendingComboBox;
        private System.Windows.Forms.ComboBox receivingComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage awsGrenzenTabPage;
        private System.Windows.Forms.TabPage awsGrenzenIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListBox unterstuetzendeBOlistBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox awsListBox;
        private System.Windows.Forms.TabPage deltaTAsTabPage;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ListBox deltaTAsListBox;
        private System.Windows.Forms.TabPage sollkonzeptTabPage;
        private System.Windows.Forms.TabPage zuordnungAtIAStabPage;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ListBox interAwSTAsListBox;
        private System.Windows.Forms.Label receivingBOlabel;
        private System.Windows.Forms.Label sendingBOlabel;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ListBox aoListBox;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox lvCheckBox;
        private System.Windows.Forms.CheckBox identityAOcheckBox;
        private System.Windows.Forms.CheckBox gleichheitAOcheckBox;
        private System.Windows.Forms.CheckBox reihenfolgebezCheckBox;
        private System.Windows.Forms.TextBox aoTextBox;
        private System.Windows.Forms.Button addAObutton;
        private System.Windows.Forms.Button delteAObutton;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox sollAutomatisierungComboBox;
        private System.Windows.Forms.ComboBox istAutomatisierungComboBox;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.ComboBox groesseComboBox;
        private System.Windows.Forms.ComboBox periodizitaetComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox nutzungsdauerComboBox;
        private System.Windows.Forms.ComboBox anpassbarkeitComboBox;
        private System.Windows.Forms.ComboBox intensitaetComboBox;
        private System.Windows.Forms.TextBox handlungsempfehlungTextBox;
        private System.Windows.Forms.CheckBox strategieCheckBox;
        private System.Windows.Forms.CheckBox einfachheitCheckBox;
        private System.Windows.Forms.CheckBox knowHowCheckBox;
        private System.Windows.Forms.CheckBox interoperabilityCheckBox;
        private System.Windows.Forms.CheckBox kostenCheckBox;
        private System.Windows.Forms.CheckBox datenschutzCheckBox;
        private System.Windows.Forms.CheckBox rechtlRahmenbedCheckBox;
        private System.Windows.Forms.CheckBox normenCheckBox;
        private System.Windows.Forms.CheckBox interdependenzenCheckBox;
        private System.Windows.Forms.CheckBox vorgabeCheckBox;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.ListBox ueberlappendeAOlistBox;
        private System.Windows.Forms.Label receivBOlabel;
        private System.Windows.Forms.Label sendBOlabel;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label receivingAwSlabel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label receivingPATlabel;
        private System.Windows.Forms.Label sendingAwSlabel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label sendingPATlabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label sendAwSlabel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label sendPATlabel;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label receivPATlabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label receivAwSlabel;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.CheckBox reihenfolgeCheckBox;
        private System.Windows.Forms.CheckBox lvOverlappingCheckBox;
        private System.Windows.Forms.CheckBox aoIdentityCheckBox;
        private System.Windows.Forms.CheckBox aoGleichheitCheckBox;
        private System.Windows.Forms.CheckBox integritaetCheckBox;
        private System.Windows.Forms.CheckBox kommunikationsstrukturCheckBox;
        private System.Windows.Forms.CheckBox vorgangssteuerungCheckBox;
        private System.Windows.Forms.CheckBox funktionsredundanzCheckBox;
        private System.Windows.Forms.CheckBox datenredundanzCheckBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox integrationskonzeptEmpfehlungTextBox;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.TextBox hinweiseTextBox;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.ComboBox integrationskonzeptComboBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.TextBox mmk2TextBox;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.TextBox mmk1TextBox;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.TextBox empfehlungTextBox;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.TextBox homogAOInstanzTextBox;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.TextBox homogAOTypenTextBox;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.TextBox restriktionenTextBox;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.TextBox ueberlappendeLVsTextBox;
    }
}