﻿namespace Methodenunterstuetzung.Views
{
    partial class BetrieblichesObjektDetailView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BetrieblichesObjektDetailView));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.addNewRowButton = new System.Windows.Forms.Button();
            this.deleteRowButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // songsDataGridView
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "songsDataGridView";
            this.dataGridView1.Size = new System.Drawing.Size(1175, 679);
            this.dataGridView1.TabIndex = 0;
            // 
            // addNewRowButton
            // 
            this.addNewRowButton.Image = ((System.Drawing.Image)(resources.GetObject("addNewRowButton.Image")));
            this.addNewRowButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.addNewRowButton.Location = new System.Drawing.Point(3, 3);
            this.addNewRowButton.Name = "addNewRowButton";
            this.addNewRowButton.Size = new System.Drawing.Size(95, 23);
            this.addNewRowButton.TabIndex = 1;
            this.addNewRowButton.Text = "Hinzufügen";
            this.addNewRowButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addNewRowButton.UseVisualStyleBackColor = true;
            this.addNewRowButton.Click += new System.EventHandler(this.addNewRowButton_Click);
            // 
            // deleteRowButton
            // 
            this.deleteRowButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteRowButton.Image")));
            this.deleteRowButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.deleteRowButton.Location = new System.Drawing.Point(104, 3);
            this.deleteRowButton.Name = "deleteRowButton";
            this.deleteRowButton.Size = new System.Drawing.Size(95, 23);
            this.deleteRowButton.TabIndex = 2;
            this.deleteRowButton.Text = "Löschen";
            this.deleteRowButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.deleteRowButton.UseVisualStyleBackColor = true;
            this.deleteRowButton.Click += new System.EventHandler(this.deleteRowButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.addNewRowButton);
            this.panel1.Controls.Add(this.deleteRowButton);
            this.panel1.Location = new System.Drawing.Point(984, 697);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 31);
            this.panel1.TabIndex = 3;
            // 
            // BetrieblichesObjektDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 740);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BetrieblichesObjektDetail";
            this.Text = "Details zu einem betrieblichen Objekt";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button addNewRowButton;
        private System.Windows.Forms.Button deleteRowButton;
        private System.Windows.Forms.Panel panel1;
    }
}