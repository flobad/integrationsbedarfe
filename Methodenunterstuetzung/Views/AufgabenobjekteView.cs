﻿using Methodenunterstuetzung.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung.Views
{
    public partial class AufgabenobjekteView : Form
    {
        public AufgabenobjekteView()
        {
            InitializeComponent();

            //vorhandeneAOsListBox;
            //zugeordneteTAsListBox;
            //zugeordBetrObjListBox;
            //fillListBoxes();
            using (var ctx = new DissEntityDataModel())
            {
                // Query for all blogs with names starting with B
                var aos = from b in ctx.TaskObjects
                          where b.ZugeordneterMandant.MandantName == Settings.Default.Mandant
                          //where b.MandantName.StartsWith("B")
                          select b;
                int i = 0;
                foreach (TaskObject ao in aos)
                {
                    vorhandeneAOsListBox.Items.Add(ao.TaskObjectName);
                    if (i == 0)
                    {
                        vorhandeneAOsListBox.SelectedItem = ao.TaskObjectName;
                    }
                    i++;
                }
            }
        }

        private void deleteAObutton_Click(object sender, EventArgs e)
        {
            if (vorhandeneAOsListBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneAOsListBox.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    TaskObject ao = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == vorhandeneAOsListBox.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.TaskObjects.Remove(ao);
                    ctx.SaveChanges();
                }
                vorhandeneAOsListBox.Items.Remove(vorhandeneAOsListBox.SelectedItem);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein betriebliches Objekt ausgewählt!";
            }
        }

        private void vorhandeneAOsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillListBoxes();
        }

        private void fillListBoxes()
        {
            if (vorhandeneAOsListBox.SelectedItem != null && !string.IsNullOrEmpty(vorhandeneAOsListBox.SelectedItem.ToString()))
            {
                zugeordneteTAsListBox.Items.Clear();
                zugeordBetrObjListBox.Items.Clear();
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    TaskObject ao = ctx.TaskObjects
                        .Where(b => b.TaskObjectName == vorhandeneAOsListBox.SelectedItem.ToString())
                        .FirstOrDefault();

                    int i = 0;
                    foreach (Transaction transaktion in ao.ZugeordneteTransaktionen)
                    {
                        if (!zugeordneteTAsListBox.Items.Contains(transaktion.TransactionName))
                        {
                            zugeordneteTAsListBox.Items.Add(transaktion.TransactionName);

                            // Fülle die Liste mit den zugeordneten betrieblichen Objekten
                            if (!zugeordBetrObjListBox.Items.Contains(transaktion.ReceivingObject.BusinessObjectName))
                            {
                                zugeordBetrObjListBox.Items.Add(transaktion.ReceivingObject.BusinessObjectName);
                            }
                            if (!zugeordBetrObjListBox.Items.Contains(transaktion.SendingObject.BusinessObjectName))
                            {
                                zugeordBetrObjListBox.Items.Add(transaktion.SendingObject.BusinessObjectName);
                            }
                        }

                        if (i == 0)
                        {
                        }
                    }
                    toolStripStatusLabel1.Text = ctx.SaveChanges().ToString();
                }
            }
        }
    }
}
