﻿using Methodenunterstuetzung.ConceptualObjects;
using Methodenunterstuetzung.DataAccessObjects;
using Methodenunterstuetzung.Properties;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung.Views
{
    public partial class MandantView : Form
    {
        public MandantView()
        {
            InitializeComponent();

            using (var ctx = new DissEntityDataModel())
            {
                // Query for the Blog named ADO.NET Blog
                //var mandant = ctx.Mandanten
                //                .Where(b => b.MandantName == textBox1.Text)
                //                .FirstOrDefault();

                // Query for all blogs with names starting with B
                var mandanten = from b in ctx.Mandanten
                                //where b.MandantName.StartsWith("B")
                                select b;
                foreach (Mandant mand in mandanten)
                {
                    listBox1.Items.Add(mand.MandantName);
                }
            }

            //if (listBox1.Items.Count == 0)
            //{
            //    ArrayList Mandanten = new MandantDAO().GetAllMandanten();
            //    foreach (Mandant mandant in Mandanten)
            //    {
            //        listBox1.Items.Add(mandant.MandantName);
            //    }
            //}

            toolStripStatusLabel1.Text = "Momentan ist Fallstudie " + Settings.Default.Mandant + " ausgewählt.";
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null && !string.IsNullOrEmpty(listBox1.SelectedItem.ToString()))
            {
                Settings.Default.Mandant = listBox1.SelectedItem.ToString();
                Settings.Default.Save();

                toolStripStatusLabel1.Text = "Es wurde die Fallstudie " + Settings.Default.Mandant + " ausgewählt.";

            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Mandant ausgewählt!";
            }

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null && !string.IsNullOrEmpty(listBox1.SelectedItem.ToString()))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    // Query for the Blog named ADO.NET Blog
                    Mandant mandant = ctx.Mandanten
                        .Where(b => b.MandantName == listBox1.SelectedItem.ToString())
                        .FirstOrDefault();

                    ctx.Mandanten.Remove(mandant);
                    ctx.SaveChanges();
                }

                //toolStripStatusLabel1.Text = new MandantDAO().DeleteMandantByName(listBox1.SelectedItem.ToString());
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Mandant ausgewählt!";
            }

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                using (var ctx = new DissEntityDataModel())
                {
                    Mandant mandant = new Mandant() { MandantName = textBox1.Text };
                    ctx.Mandanten.Add(mandant);
                    ctx.SaveChanges();
                }
                //toolStripStatusLabel1.Text = new MandantDAO().CreateNewMandant(new Mandant(textBox1.Text));
                listBox1.Items.Add(textBox1.Text);
            }
            else
            {
                toolStripStatusLabel1.Text = "Es wurde kein Mandant hinzugefügt!";
            }
            textBox1.Clear();
        }
    }
}
