﻿using Methodenunterstuetzung.Properties;
using Methodenunterstuetzung.Resources;
using Methodenunterstuetzung.TaskObjects;
using Methodenunterstuetzung.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Methodenunterstuetzung
{
    /// <summary>
    /// Singleton class
    /// </summary>
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            this.Initialized_Items();

            toolStripStatusLabel1.Text = "Momentan ist Fallstudie " + Settings.Default.Mandant + " ausgewählt.";
        }

        private void boToolStripButton_Click(object sender, EventArgs e)
        {
            BetrieblicheObjekteView bo = new BetrieblicheObjekteView();
            bo.MdiParent = this;
            bo.Show();

            // a flag to store if the child form is opened or not
            //bool found = false;

            //// get all of the MDI children in an array
            //Form[] charr = this.MdiChildren;

            //if (charr.Length == 0)      // no child form is opened
            //{
            //    BetrieblicheObjekte myPatients = new BetrieblicheObjekte();
            //    myPatients.MdiParent = this;
            //    // The StartPosition property is essential
            //    // for the location property to work
            //    myPatients.StartPosition = FormStartPosition.Manual;
            //    myPatients.Location = new Point(0, 0);
            //    myPatients.Show();
            //}
            //else      // child forms are opened
            //{

            //    foreach (Form chform in charr)
            //    {
            //        Console.WriteLine(chform.Name);

            //        if (chform.Name == "BetrieblicheObjekte")
            //        // one instance of the form is already opened
            //        {
            //            chform.Activate();
            //            found = true;
            //            break;   // exit loop
            //        }
            //        else
            //        {
            //            found = false;      // make sure flag is set to false if the form is not found
            //        }
            //    }

            //    if (found == false)
            //    {
            //        BetrieblicheObjekte myPatients = new BetrieblicheObjekte();
            //        myPatients.MdiParent = this;
            //        // The StartPosition property is essential
            //        // for the location property to work
            //        myPatients.StartPosition = FormStartPosition.Manual;
            //        myPatients.Location = new Point(0, 0);
            //        myPatients.Show();
            //    }
            //}
        }

        //private void awsToolStripButton_Click(object sender, EventArgs e)
        //{
        //    Form1 bo = new Form1();
        //    bo.MdiParent = this;
        //    bo.Show();

        //}

        private void initialisiereDatenbankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ReturnValue = InitializationTasks.GetInstance().InitializeDatabases();
            //MessageBox.Show(ReturnValue, "Info", MessageBoxButton.OK);
            toolStripStatusLabel1.Text = ReturnValue;
            this.Initialized_Items();
        }

        private void Initialized_Items()
        {
            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Database file is initialized, deactivate the corresponding button.
                initialisiereDatenbankToolStripMenuItem.Enabled = false;
                boToolStripButton.Enabled = true;
                //awsToolStripButton.Enabled = true;
                löscheDatenbankToolStripMenuItem.Enabled = true;
            }
            else if (!File.Exists(Constants.DATABASE_FILE))
            {
                initialisiereDatenbankToolStripMenuItem.Enabled = true;
                boToolStripButton.Enabled = false;
                //awsToolStripButton.Enabled = false;
                // Database file is not initialized, deactivate the corresponding button.
                löscheDatenbankToolStripMenuItem.Enabled = false;
            }
        }

        private void löscheDatenbankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ReturnValue = CleanupTasks.GetInstance().DeleteDatabases();
            //MessageBox.Show(ReturnValue, "Info", MessageBoxButton.OK);
            toolStripStatusLabel1.Text = ReturnValue;
            this.Initialized_Items();
        }

        private void mandantToolStripButton_Click(object sender, EventArgs e)
        {
            MandantView mandant = new MandantView();
            mandant.MdiParent = this;
            mandant.Show();
        }

        private void pATtoolStripButton_Click(object sender, EventArgs e)
        {
            AufgabentraegerView pAT = new AufgabentraegerView();
            pAT.MdiParent = this;
            pAT.Show();
        }

        private void awsToolStripButton_Click(object sender, EventArgs e)
        {
            AufgabentraegerView pAT = new AufgabentraegerView();
            pAT.MdiParent = this;
            pAT.Show();
        }

        private void hilfeToolStripButton_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.MdiParent = this;
            ab.Show();
        }

        private void aufgabenobjekteToolStripButton_Click(object sender, EventArgs e)
        {
            AufgabenobjekteView av = new AufgabenobjekteView();
            av.MdiParent = this;
            av.Show();
        }
    }
}
