﻿using Db4objects.Db4o;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Methodenunterstuetzung.Resources;
using Methodenunterstuetzung.ConceptualObjects;

namespace Methodenunterstuetzung.TaskObjects
{
    class InitializationTasks
    {
        /// <summary>
        /// Implement the singleton pattern
        /// </summary>
        private InitializationTasks() { }

        /// <summary>
        /// Instance for the singleton pattern.
        /// </summary>
        private static InitializationTasks Instance;

        /// <summary>
        /// Get the instance of the class.
        /// </summary>
        /// <returns></returns>
        public static InitializationTasks GetInstance()
        {
            if (Instance == null)
            {
                Instance = new InitializationTasks();
            }
            return Instance;
        }

        /// <summary>
        /// Task for initializing the database(s).
        /// </summary>
        /// <returns></returns>
        public string InitializeDatabases()
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Save the data
                    //db.Store(new BusinessObject("Anpassbarkeit"));

                    // Load the data
                    IList<BusinessObject> result = db.Query<BusinessObject>(typeof(BusinessObject));
                    returnValue = "You have successfully initialized " + result.Count.ToString() + " items!";
                    db.Close();
                }
            }
            else if (File.Exists(Constants.DATABASE_FILE))
            {
                returnValue = "The database(s) has been already initialized!";
            }
            return returnValue;
        }

    }
}
