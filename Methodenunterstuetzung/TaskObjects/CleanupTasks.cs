﻿using Methodenunterstuetzung.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.TaskObjects
{
    class CleanupTasks
    {
        /// <summary>
        /// Implement the singleton pattern
        /// </summary>
        private CleanupTasks() { }

        /// <summary>
        /// Instance for the singleton pattern.
        /// </summary>
        private static CleanupTasks instance;

        /// <summary>
        /// Get the instance of the class.
        /// </summary>
        /// <returns></returns>
        public static CleanupTasks GetInstance()
        {
            if (instance == null)
            {
                instance = new CleanupTasks();
            }
            return instance;
        }

        /// <summary>
        /// Task for deleting the database(s).
        /// </summary>
        /// <returns></returns>
        public string DeleteDatabases()
        {
            string returnValue = "";

            if (File.Exists(Constants.DATABASE_FILE))
            {
                File.Delete(Constants.DATABASE_FILE);
                returnValue = "You have successfully deleted the database(s)!";
            }
            else if (!File.Exists(Constants.DATABASE_FILE))
            {
                returnValue = "You can not delete the database(s), because they are currently not created!";
            }
            return returnValue;
        }

    }
}
