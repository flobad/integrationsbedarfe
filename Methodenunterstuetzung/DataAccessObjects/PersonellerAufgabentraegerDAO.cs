﻿using Db4objects.Db4o;
using Methodenunterstuetzung.ConceptualObjects;
using Methodenunterstuetzung.Resources;
using Methodenunterstuetzung.TaskObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.DataAccessObjects
{
    public class PersonellerAufgabentraegerDAO
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public string CreateNewPersonellerAufgabentraeger(PersonellerAufgabentraeger entry)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Save the data
                    db.Store(entry);

                    // Load the data
                    IList<PersonellerAufgabentraeger> result = db.Query<PersonellerAufgabentraeger>(typeof(PersonellerAufgabentraeger));
                    returnValue = "You have successfully added the item " + entry.AufgabentraegerName.ToString() + ". There are now " + result.Count.ToString() + " items stored.";
                    db.Close();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Retrieve all characteristics in the database.
        /// </summary>
        /// <returns></returns>
        public ArrayList GetAllPersonelleAufgabentraeger()
        {
            ArrayList result = new ArrayList();

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                //returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Load the data
                    result.AddRange(db.Query<PersonellerAufgabentraeger>(typeof(PersonellerAufgabentraeger)).ToList());
                    db.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieve characteristics by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PersonellerAufgabentraeger GetPersonellerAufgabentraegerByName(string name)
        {
            ArrayList result = new ArrayList();

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                //returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Load the data
                    result.AddRange(db.QueryByExample(new PersonellerAufgabentraeger(name)));
                    db.Close();
                }
            }

            if (result.Count >= 1)
            {
                return (PersonellerAufgabentraeger)result[0];
            }
            return null;
        }

        /// <summary>
        /// Updates the characteristics retrieved by name.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public string UpdatePersonellerAufgabentraegerByName(PersonellerAufgabentraeger entry)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    int counter = 0;

                    // Load the data
                    IObjectSet result = db.QueryByExample(new PersonellerAufgabentraeger(entry.AufgabentraegerName));
                    foreach (var item in result)
                    {
                        PersonellerAufgabentraeger found = (PersonellerAufgabentraeger)item;
                        // TODO
                        db.Store(found);
                        counter++;
                    }
                    db.Close();

                    returnValue = "Updated " + counter + " items";
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Deletes characteristics by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string DeletePersonellerAufgabentraegerByName(string name)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    int counter = 0;

                    // Load the data
                    IObjectSet result = db.QueryByExample(new PersonellerAufgabentraeger(name));
                    // Delete all retrieved items
                    foreach (var item in result)
                    {
                        db.Delete((PersonellerAufgabentraeger)item);
                        counter++;
                    }
                    db.Close();

                    returnValue = "Deleted " + counter + " items";
                }
            }

            return returnValue;
        }

    }
}
