﻿using Db4objects.Db4o;
using Methodenunterstuetzung.ConceptualObjects;
using Methodenunterstuetzung.Resources;
using Methodenunterstuetzung.TaskObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.DataAccessObjects
{
    public class BusinessObjectDAO
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public string CreateNewBusinessObject(BusinessObject entry)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Save the data
                    db.Store(entry);

                    // Load the data
                    IList<BusinessObject> result = db.Query<BusinessObject>(typeof(BusinessObject));
                    returnValue = "You have successfully added the item " + entry.BusinessObjectName.ToString() + ". There are now " + result.Count.ToString() + " items stored.";
                    db.Close();
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Retrieve all characteristics in the database.
        /// </summary>
        /// <returns></returns>
        public ArrayList GetAllBusinessObjects()
        {
            ArrayList result = new ArrayList();

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                //returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Load the data
                    result.AddRange(db.Query<BusinessObject>(typeof(BusinessObject)).ToList());
                    db.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieve characteristics by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BusinessObject GetBusinessObjectByName(string name)
        {
            ArrayList result = new ArrayList();

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                //returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    // Load the data
                    result.AddRange(db.QueryByExample(new BusinessObject(name)));
                    db.Close();
                }
            }

            if (result.Count >= 1)
            {
                return (BusinessObject)result[0];
            }
            return null;
        }

        /// <summary>
        /// Updates the characteristics retrieved by name.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        public string UpdateBusinessObjectByName(BusinessObject entry)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    int counter = 0;

                    //db.Store(entry);
                    //db.Commit();

                    // Load the data
                    IObjectSet result = db.QueryByExample(new BusinessObject(entry.BusinessObjectName));
                    foreach (var item in result)
                    {
                        BusinessObject found = (BusinessObject)item;
                        // TODO
                        found = entry;
                        db.Store(found);
                        db.Commit();
                        counter++;
                    }
                    db.Close();

                    returnValue = "Updated " + counter + " items";
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Deletes characteristics by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string DeleteBusinessObjectByName(string name)
        {
            string returnValue = "";

            if (!File.Exists(Constants.DATABASE_FILE))
            {
                // Database(s) do not exist! Initialize the database(s) first!
                returnValue = InitializationTasks.GetInstance().InitializeDatabases();
            }

            if (File.Exists(Constants.DATABASE_FILE))
            {
                // Use db4o as database
                using (IObjectContainer db = Db4oEmbedded.OpenFile(Constants.DATABASE_FILE))
                {
                    int counter = 0;

                    // Load the data
                    IObjectSet result = db.QueryByExample(new BusinessObject(name));
                    // Delete all retrieved items
                    foreach (var item in result)
                    {
                        db.Delete((BusinessObject)item);
                        counter++;
                    }
                    db.Close();

                    returnValue = "Deleted " + counter + " items";
                }
            }

            return returnValue;
        }
    }
}
