﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class Task
    {
        /// <summary>
        /// Bezeichnung der Aufgabe
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// Lösungsverfahren der Aufgabe definiert über Aktionen
        /// </summary>
        public List<Action> Actions { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad für die Aktionensteuerung
        /// </summary>
        public enum RequiredAutomationActionControl { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad für die Aktionensteuerung
        /// </summary>
        public enum ActualAutomationActionControl { automated, notAutomated };

        /// <summary>
        /// Benötigter Automatisierungsgrad für die Vorgangsauslösung
        /// </summary>
        public enum RequiredAutomationProcedureTrigger { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad für die Vorgangsauslösung
        /// </summary>
        public enum ActualAutomationProcedureTrigger { automated, notAutomated };
    }
}
