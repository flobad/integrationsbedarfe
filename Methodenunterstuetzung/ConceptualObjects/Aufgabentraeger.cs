﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class Aufgabentraeger
    {
        public string AufgabentraegerName { get; set; }
        public List<BusinessObject> UnterstuetzendeBOs { get; set; }
        public Mandant ZugeordneterMandant { get; set; }
    }
}
