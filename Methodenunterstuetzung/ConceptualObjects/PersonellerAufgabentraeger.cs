﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class PersonellerAufgabentraeger : Aufgabentraeger
    {
        public PersonellerAufgabentraeger() { }
        public PersonellerAufgabentraeger(string name) { this.AufgabentraegerName = name; }
    }
}
