﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class Action
    {
        /// <summary>
        /// Beschreibung der Aktion
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public enum RequiredAutomation { toAutomate, partlyToAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public enum ActualAutomation { notNot, partNot, fullNot, partPart, fullPart, fullFull };
        
        /// <summary>
        /// Aufgabenobjekte
        /// </summary>
        public List<TaskObject> TaskObjects { get; set; }

        /// <summary>
        /// Für die Aktion zugeordnete Transaktion
        /// </summary>
        public Transaction Transaction { get; set; }


    }
}
