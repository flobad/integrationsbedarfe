﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class Mandant
    {
        /// <summary>
        /// Default minimal constructor for characteristic.
        /// </summary>
        public Mandant() { }

        /// <summary>
        /// Public constructor for a business object.
        /// </summary>
        /// <param name="name"></param>
        public Mandant(string name) { this.MandantName = name; }

        /// <summary>
        /// Bezeichnung des betrieblichen Objekts
        /// </summary>
        public string MandantName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<BusinessObject> BusinessObjects { get; set; }

    }
}
