﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class Transaction
    {
        /// <summary>
        /// Bezeichnung der Transaktion
        /// </summary>
        public string TransactionName { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public enum RequiredAutomation { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public enum ActualAutomation { automated, notAutomated };

        /// <summary>
        /// Sendendes betriebliches Objekt
        /// </summary>
        public BusinessObject SendingObject { get; set; }

        /// <summary>
        /// Empfangendes betriebliches Objekt
        /// </summary>
        public BusinessObject ReceivingObject { get; set; }

        public List<TaskObject> UeberlappendeAOs { get; set; }
    }
}
