﻿namespace Methodenunterstuetzung.ConceptualObjects
{
    public class TaskObject
    {
        /// <summary>
        /// Bezeichnung des Aufgabenobjekts
        /// </summary>
        public string TaskObjectName { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public enum RequiredAutomation { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public enum ActualAutomation { automated, notAutomated };

        public Mandant ZugeordneterMandant { get; set; }
    }
}