﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methodenunterstuetzung.ConceptualObjects
{
    public class BusinessObject
    {
        /// <summary>
        /// Default minimal constructor for characteristic.
        /// </summary>
        public BusinessObject() { }

        /// <summary>
        /// Public constructor for a business object.
        /// </summary>
        /// <param name="name"></param>
        public BusinessObject(string name) { this.BusinessObjectName = name; }

        /// <summary>
        /// Bezeichnung des betrieblichen Objekts
        /// </summary>
        public string BusinessObjectName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Task> Tasks { get; set; }

        public Anwendungssystem ZugeordnetesAwS { get; set; }
        public PersonellerAufgabentraeger ZugeordneteStelle { get; set; }
        public Mandant ZugeordneterMandant { get; set; }
        public List<Transaction> ZugeordneteTAs { get; set; }
    }
}
