﻿namespace Methodenunterstuetzung
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class DissEntityDataModel : DbContext
    {
        // Der Kontext wurde für die Verwendung einer DissEntityDataModel-Verbindungszeichenfolge aus der 
        // Konfigurationsdatei ('App.config' oder 'Web.config') der Anwendung konfiguriert. Diese Verbindungszeichenfolge hat standardmäßig die 
        // Datenbank 'Methodenunterstuetzung.DissEntityDataModel' auf der LocalDb-Instanz als Ziel. 
        // 
        // Wenn Sie eine andere Datenbank und/oder einen anderen Anbieter als Ziel verwenden möchten, ändern Sie die DissEntityDataModel-Zeichenfolge 
        // in der Anwendungskonfigurationsdatei.
        public DissEntityDataModel()
            : base("name=DissEntityDataModel")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DissEntityDataModel, Migrations.Configuration>("DissEntityDataModel"));
        }

        // Fügen Sie ein 'DbSet' für jeden Entitätstyp hinzu, den Sie in das Modell einschließen möchten. Weitere Informationen 
        // zum Konfigurieren und Verwenden eines Code First-Modells finden Sie unter 'http://go.microsoft.com/fwlink/?LinkId=390109'.

        //public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<Mandant> Mandanten { get; set; }
        public virtual DbSet<BusinessObject> BusinessObjects { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskObject> TaskObjects { get; set; }
        public virtual DbSet<Anwendungssystem> Anwendungssysteme { get; set; }
        public virtual DbSet<PersonellerAufgabentraeger> PersonelleAufgabentraeger { get; set; }
        public virtual DbSet<Action> Actions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    public class Mandant
    {
        /// <summary>
        /// Default minimal constructor for characteristic.
        /// </summary>
        public Mandant() { }

        /// <summary>
        /// Public constructor for a business object.
        /// </summary>
        /// <param name="name"></param>
        public Mandant(string name) { this.MandantName = name; }

        /// <summary>
        /// Bezeichnung des betrieblichen Objekts
        /// </summary>
        [Key]
        public string MandantName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<BusinessObject> BusinessObjects { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }

    public class BusinessObject
    {
        /// <summary>
        /// Default minimal constructor for characteristic.
        /// </summary>
        public BusinessObject() { }

        /// <summary>
        /// Public constructor for a business object.
        /// </summary>
        /// <param name="name"></param>
        public BusinessObject(string name) { this.BusinessObjectName = name; }

        /// <summary>
        /// Bezeichnung des betrieblichen Objekts
        /// </summary>
        [Required] // [Key]
        public string BusinessObjectName { get; set; }

        [Key]
        public int BusinessObjectKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<Task> Tasks { get; set; }

        public virtual Anwendungssystem ZugeordnetesAwS { get; set; }
        public virtual PersonellerAufgabentraeger ZugeordneteStelle { get; set; }
        public virtual Mandant ZugeordneterMandant { get; set; }
        public virtual ICollection<Transaction> ZugeordneteTAs { get; set; }
    }

    public class Transaction
    {
        /// <summary>
        /// Bezeichnung der Transaktion
        /// </summary>
        [Required] // [Key]
        public string TransactionName { get; set; }

        [Key]
        public int TransactionKey { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public TransactionAutomationDegrees RequiredAutomation { get; set; }

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public TransactionAutomationDegrees ActualAutomation { get; set; }

        /// <summary>
        /// Sendendes betriebliches Objekt
        /// </summary>
        public virtual BusinessObject SendingObject { get; set; }

        /// <summary>
        /// Empfangendes betriebliches Objekt
        /// </summary>
        public virtual BusinessObject ReceivingObject { get; set; }

        public virtual Mandant ZugeordneterMandant { get; set; }

        public virtual ICollection<TaskObject> UeberlappendeAOs { get; set; }

        // Identifizierte Aufgabenintegrationsmuster
        //public virtual TaskObject IdentifizierteAufgabenintegrationsmuster { get; set; }
        public bool Reihenfolgebeziehung { get; set; }
        public bool PartielleGleichheit { get; set; }
        public bool PartielleIdentitaet { get; set; }
        public bool UeberlappendeLoesungsverfahren { get; set; }


        // Unternehmensplan: Strategie; Vorgabe; Interdependenzen; Normen; rechtl. Rahmenbedingungen; Datenschutz; Kosten
        public bool Strategie { get; set; }
        public bool Vorgabe { get; set; }
        public bool Interdependenzen { get; set; }
        public bool Normen { get; set; }
        public bool Rahmenbedingungen { get; set; }
        public bool Datenschutz { get; set; }
        public bool Kosten { get; set; }

        // Geschäfsprozessmodell: Merkmale
        public MerkmaleAuspraegungen Periodizitaet { get; set; }
        public MerkmaleAuspraegungen Groesse { get; set; }
        public MerkmaleAuspraegungen Intensitaet { get; set; }
        public MerkmaleAuspraegungen Anpassbarkeit { get; set; }
        public NutzungsdauerAuspraegungen Nutzungsdauer { get; set; }

        // Interoperabilität; Know-how; Einfachheit
        public bool Interoberability { get; set; }
        public bool KnowHow { get; set; }
        public bool Einfachheit { get; set; }

        // Integrationsziele
        public bool Datenredundanz { get; set; }
        public bool Funktionsredundanz { get; set; }
        public bool Vorgangssteuerung { get; set; }
        public bool Kommunikationsstrutkur { get; set; }
        public bool Integritaet { get; set; }

        public Integrationskonzept Integrationskonzept { get; set; }
    }

    public enum Integrationskonzept
    {
        nicht_gesetzt, aufgabentraegerorientiert, datenflussorientiert, datenintegration, objektintegration
    }

    public enum MerkmaleAuspraegungen
    {
        nicht_gesetzt, niedrig, moderat, hoch
    }

    public enum NutzungsdauerAuspraegungen
    {
        nicht_gesetzt, kurzfristig, mittelfristig, langfristig
    }

    public enum TransactionAutomationDegrees
    {
        nicht_gesetzt, Automate, NotAutomate
    }

    public class Task
    {
        /// <summary>
        /// Bezeichnung der Aufgabe
        /// </summary>
        [Required]
        public string TaskName { get; set; }

        [Key]
        public int TaskKey { get; set; }

        /// <summary>
        /// Lösungsverfahren der Aufgabe definiert über Aktionen
        /// </summary>
        public virtual ICollection<Action> Actions { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad für die Aktionensteuerung
        /// </summary>
        public enum RequiredAutomationActionControl { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad für die Aktionensteuerung
        /// </summary>
        public enum ActualAutomationActionControl { automated, notAutomated };

        /// <summary>
        /// Benötigter Automatisierungsgrad für die Vorgangsauslösung
        /// </summary>
        public enum RequiredAutomationProcedureTrigger { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad für die Vorgangsauslösung
        /// </summary>
        public enum ActualAutomationProcedureTrigger { automated, notAutomated };
    }

    public class TaskObject
    {
        /// <summary>
        /// Bezeichnung des Aufgabenobjekts
        /// </summary>
        [Required]
        public string TaskObjectName { get; set; }

        [Key]
        public int TaskObjectKey { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        //public enum RequiredAutomation { toAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        //public enum ActualAutomation { automated, notAutomated };

        public virtual Mandant ZugeordneterMandant { get; set; }
        public virtual ICollection<Transaction> ZugeordneteTransaktionen { get; set; }
    }

    public class Aufgabentraeger
    {
        [Required]
        public string AufgabentraegerName { get; set; }
        [Key]
        public int AufgabentraegerKey { get; set; }
        public virtual ICollection<BusinessObject> UnterstuetzendeBOs { get; set; }
        public virtual Mandant ZugeordneterMandant { get; set; }
    }

    public class Anwendungssystem : Aufgabentraeger
    {
    }

    public class PersonellerAufgabentraeger : Aufgabentraeger
    {
        public PersonellerAufgabentraeger() { }
        public PersonellerAufgabentraeger(string name) { this.AufgabentraegerName = name; }
    }

    public class Action
    {
        /// <summary>
        /// Beschreibung der Aktion
        /// </summary>
        [Required]
        public string ActionName { get; set; }

        [Key]
        public int ActionKey { get; set; }

        /// <summary>
        /// Benötigter Automatisierungsgrad
        /// </summary>
        public enum RequiredAutomation { toAutomate, partlyToAutomate, notToAutomate };

        /// <summary>
        /// Gegenwärtiger Automatisierungsgrad
        /// </summary>
        public enum ActualAutomation { notNot, partNot, fullNot, partPart, fullPart, fullFull };

        /// <summary>
        /// Aufgabenobjekte
        /// </summary>
        public virtual ICollection<TaskObject> TaskObjects { get; set; }

        /// <summary>
        /// Für die Aktion zugeordnete Transaktion
        /// </summary>
        public virtual Transaction Transaction { get; set; }
    }

}