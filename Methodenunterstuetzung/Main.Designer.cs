﻿namespace Methodenunterstuetzung
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.windowsMenu = new System.Windows.Forms.MenuStrip();
            this.dateiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.neuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernunterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.druckenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seitenansichtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.beendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rückgängigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wiederholenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ausschneidenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kopierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.einfügenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.alleauswählenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extrasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.anpassenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extrasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.initialisiereDatenbankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löscheDatenbankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inhaltToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suchenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.neuToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.öffnenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.speichernToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.druckenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.ausschneidenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.kopierenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.einfügenToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.hilfeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mandantToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.boToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.awsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pATtoolStripButton = new System.Windows.Forms.ToolStripButton();
            this.aufgabenobjekteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip.SuspendLayout();
            this.windowsMenu.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip.Location = new System.Drawing.Point(0, 846);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1565, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // windowsMenu
            // 
            this.windowsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem1,
            this.bearbeitenToolStripMenuItem1,
            this.extrasToolStripMenuItem1,
            this.extrasToolStripMenuItem,
            this.hilfeToolStripMenuItem1});
            this.windowsMenu.Location = new System.Drawing.Point(0, 0);
            this.windowsMenu.Name = "windowsMenu";
            this.windowsMenu.Size = new System.Drawing.Size(1565, 24);
            this.windowsMenu.TabIndex = 4;
            this.windowsMenu.Text = "menuStrip1";
            // 
            // dateiToolStripMenuItem1
            // 
            this.dateiToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuToolStripMenuItem,
            this.öffnenToolStripMenuItem,
            this.toolStripSeparator3,
            this.speichernToolStripMenuItem,
            this.speichernunterToolStripMenuItem,
            this.toolStripSeparator4,
            this.druckenToolStripMenuItem,
            this.seitenansichtToolStripMenuItem,
            this.toolStripSeparator5,
            this.beendenToolStripMenuItem});
            this.dateiToolStripMenuItem1.Enabled = false;
            this.dateiToolStripMenuItem1.Name = "dateiToolStripMenuItem1";
            this.dateiToolStripMenuItem1.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem1.Text = "&Datei";
            // 
            // neuToolStripMenuItem
            // 
            this.neuToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("neuToolStripMenuItem.Image")));
            this.neuToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.neuToolStripMenuItem.Name = "neuToolStripMenuItem";
            this.neuToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.neuToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.neuToolStripMenuItem.Text = "&Neu";
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("öffnenToolStripMenuItem.Image")));
            this.öffnenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.öffnenToolStripMenuItem.Text = "Ö&ffnen";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(165, 6);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("speichernToolStripMenuItem.Image")));
            this.speichernToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.speichernToolStripMenuItem.Text = "&Speichern";
            // 
            // speichernunterToolStripMenuItem
            // 
            this.speichernunterToolStripMenuItem.Name = "speichernunterToolStripMenuItem";
            this.speichernunterToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.speichernunterToolStripMenuItem.Text = "Speichern &unter";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(165, 6);
            // 
            // druckenToolStripMenuItem
            // 
            this.druckenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("druckenToolStripMenuItem.Image")));
            this.druckenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.druckenToolStripMenuItem.Name = "druckenToolStripMenuItem";
            this.druckenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.druckenToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.druckenToolStripMenuItem.Text = "&Drucken";
            // 
            // seitenansichtToolStripMenuItem
            // 
            this.seitenansichtToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("seitenansichtToolStripMenuItem.Image")));
            this.seitenansichtToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.seitenansichtToolStripMenuItem.Name = "seitenansichtToolStripMenuItem";
            this.seitenansichtToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.seitenansichtToolStripMenuItem.Text = "&Seitenansicht";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(165, 6);
            // 
            // beendenToolStripMenuItem
            // 
            this.beendenToolStripMenuItem.Name = "beendenToolStripMenuItem";
            this.beendenToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.beendenToolStripMenuItem.Text = "&Beenden";
            // 
            // bearbeitenToolStripMenuItem1
            // 
            this.bearbeitenToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rückgängigToolStripMenuItem,
            this.wiederholenToolStripMenuItem,
            this.toolStripSeparator6,
            this.ausschneidenToolStripMenuItem,
            this.kopierenToolStripMenuItem,
            this.einfügenToolStripMenuItem,
            this.toolStripSeparator7,
            this.alleauswählenToolStripMenuItem});
            this.bearbeitenToolStripMenuItem1.Enabled = false;
            this.bearbeitenToolStripMenuItem1.Name = "bearbeitenToolStripMenuItem1";
            this.bearbeitenToolStripMenuItem1.Size = new System.Drawing.Size(75, 20);
            this.bearbeitenToolStripMenuItem1.Text = "&Bearbeiten";
            // 
            // rückgängigToolStripMenuItem
            // 
            this.rückgängigToolStripMenuItem.Name = "rückgängigToolStripMenuItem";
            this.rückgängigToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.rückgängigToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.rückgängigToolStripMenuItem.Text = "&Rückgängig";
            // 
            // wiederholenToolStripMenuItem
            // 
            this.wiederholenToolStripMenuItem.Name = "wiederholenToolStripMenuItem";
            this.wiederholenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.wiederholenToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.wiederholenToolStripMenuItem.Text = "Wiede&rholen";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(188, 6);
            // 
            // ausschneidenToolStripMenuItem
            // 
            this.ausschneidenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ausschneidenToolStripMenuItem.Image")));
            this.ausschneidenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ausschneidenToolStripMenuItem.Name = "ausschneidenToolStripMenuItem";
            this.ausschneidenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.ausschneidenToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.ausschneidenToolStripMenuItem.Text = "&Ausschneiden";
            // 
            // kopierenToolStripMenuItem
            // 
            this.kopierenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("kopierenToolStripMenuItem.Image")));
            this.kopierenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.kopierenToolStripMenuItem.Name = "kopierenToolStripMenuItem";
            this.kopierenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.kopierenToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.kopierenToolStripMenuItem.Text = "&Kopieren";
            // 
            // einfügenToolStripMenuItem
            // 
            this.einfügenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("einfügenToolStripMenuItem.Image")));
            this.einfügenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.einfügenToolStripMenuItem.Name = "einfügenToolStripMenuItem";
            this.einfügenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.einfügenToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.einfügenToolStripMenuItem.Text = "&Einfügen";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(188, 6);
            // 
            // alleauswählenToolStripMenuItem
            // 
            this.alleauswählenToolStripMenuItem.Name = "alleauswählenToolStripMenuItem";
            this.alleauswählenToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.alleauswählenToolStripMenuItem.Text = "&Alle auswählen";
            // 
            // extrasToolStripMenuItem1
            // 
            this.extrasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.anpassenToolStripMenuItem,
            this.optionenToolStripMenuItem});
            this.extrasToolStripMenuItem1.Enabled = false;
            this.extrasToolStripMenuItem1.Name = "extrasToolStripMenuItem1";
            this.extrasToolStripMenuItem1.Size = new System.Drawing.Size(49, 20);
            this.extrasToolStripMenuItem1.Text = "E&xtras";
            // 
            // anpassenToolStripMenuItem
            // 
            this.anpassenToolStripMenuItem.Name = "anpassenToolStripMenuItem";
            this.anpassenToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.anpassenToolStripMenuItem.Text = "&Anpassen";
            // 
            // optionenToolStripMenuItem
            // 
            this.optionenToolStripMenuItem.Name = "optionenToolStripMenuItem";
            this.optionenToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.optionenToolStripMenuItem.Text = "&Optionen";
            // 
            // extrasToolStripMenuItem
            // 
            this.extrasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.initialisiereDatenbankToolStripMenuItem,
            this.löscheDatenbankToolStripMenuItem});
            this.extrasToolStripMenuItem.Enabled = false;
            this.extrasToolStripMenuItem.Name = "extrasToolStripMenuItem";
            this.extrasToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.extrasToolStripMenuItem.Text = "Daten";
            // 
            // initialisiereDatenbankToolStripMenuItem
            // 
            this.initialisiereDatenbankToolStripMenuItem.Name = "initialisiereDatenbankToolStripMenuItem";
            this.initialisiereDatenbankToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.initialisiereDatenbankToolStripMenuItem.Text = "Initialisiere Datenbank";
            this.initialisiereDatenbankToolStripMenuItem.Click += new System.EventHandler(this.initialisiereDatenbankToolStripMenuItem_Click);
            // 
            // löscheDatenbankToolStripMenuItem
            // 
            this.löscheDatenbankToolStripMenuItem.Name = "löscheDatenbankToolStripMenuItem";
            this.löscheDatenbankToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.löscheDatenbankToolStripMenuItem.Text = "Lösche Datenbank";
            this.löscheDatenbankToolStripMenuItem.Click += new System.EventHandler(this.löscheDatenbankToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem1
            // 
            this.hilfeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inhaltToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.suchenToolStripMenuItem,
            this.toolStripSeparator8,
            this.infoToolStripMenuItem});
            this.hilfeToolStripMenuItem1.Enabled = false;
            this.hilfeToolStripMenuItem1.Name = "hilfeToolStripMenuItem1";
            this.hilfeToolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem1.Text = "&Hilfe";
            // 
            // inhaltToolStripMenuItem
            // 
            this.inhaltToolStripMenuItem.Name = "inhaltToolStripMenuItem";
            this.inhaltToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.inhaltToolStripMenuItem.Text = "I&nhalt";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // suchenToolStripMenuItem
            // 
            this.suchenToolStripMenuItem.Name = "suchenToolStripMenuItem";
            this.suchenToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.suchenToolStripMenuItem.Text = "&Suchen";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(110, 6);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.infoToolStripMenuItem.Text = "Inf&o...";
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boToolStripButton,
            this.toolStripSeparator2,
            this.mandantToolStripButton,
            this.awsToolStripButton,
            this.pATtoolStripButton,
            this.aufgabenobjekteToolStripButton,
            this.toolStripSeparator9,
            this.neuToolStripButton,
            this.öffnenToolStripButton,
            this.speichernToolStripButton,
            this.druckenToolStripButton,
            this.toolStripSeparator,
            this.ausschneidenToolStripButton,
            this.kopierenToolStripButton,
            this.einfügenToolStripButton,
            this.toolStripSeparator1,
            this.hilfeToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1565, 25);
            this.toolStrip.TabIndex = 5;
            this.toolStrip.Text = "toolStrip1";
            // 
            // neuToolStripButton
            // 
            this.neuToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.neuToolStripButton.Enabled = false;
            this.neuToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("neuToolStripButton.Image")));
            this.neuToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.neuToolStripButton.Name = "neuToolStripButton";
            this.neuToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.neuToolStripButton.Text = "&Neu";
            // 
            // öffnenToolStripButton
            // 
            this.öffnenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.öffnenToolStripButton.Enabled = false;
            this.öffnenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("öffnenToolStripButton.Image")));
            this.öffnenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.öffnenToolStripButton.Name = "öffnenToolStripButton";
            this.öffnenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.öffnenToolStripButton.Text = "Ö&ffnen";
            // 
            // speichernToolStripButton
            // 
            this.speichernToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.speichernToolStripButton.Enabled = false;
            this.speichernToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("speichernToolStripButton.Image")));
            this.speichernToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.speichernToolStripButton.Name = "speichernToolStripButton";
            this.speichernToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.speichernToolStripButton.Text = "&Speichern";
            // 
            // druckenToolStripButton
            // 
            this.druckenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.druckenToolStripButton.Enabled = false;
            this.druckenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("druckenToolStripButton.Image")));
            this.druckenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.druckenToolStripButton.Name = "druckenToolStripButton";
            this.druckenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.druckenToolStripButton.Text = "&Drucken";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // ausschneidenToolStripButton
            // 
            this.ausschneidenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ausschneidenToolStripButton.Enabled = false;
            this.ausschneidenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ausschneidenToolStripButton.Image")));
            this.ausschneidenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ausschneidenToolStripButton.Name = "ausschneidenToolStripButton";
            this.ausschneidenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.ausschneidenToolStripButton.Text = "&Ausschneiden";
            // 
            // kopierenToolStripButton
            // 
            this.kopierenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.kopierenToolStripButton.Enabled = false;
            this.kopierenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("kopierenToolStripButton.Image")));
            this.kopierenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.kopierenToolStripButton.Name = "kopierenToolStripButton";
            this.kopierenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.kopierenToolStripButton.Text = "&Kopieren";
            // 
            // einfügenToolStripButton
            // 
            this.einfügenToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.einfügenToolStripButton.Enabled = false;
            this.einfügenToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("einfügenToolStripButton.Image")));
            this.einfügenToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.einfügenToolStripButton.Name = "einfügenToolStripButton";
            this.einfügenToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.einfügenToolStripButton.Text = "&Einfügen";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // hilfeToolStripButton
            // 
            this.hilfeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.hilfeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("hilfeToolStripButton.Image")));
            this.hilfeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.hilfeToolStripButton.Name = "hilfeToolStripButton";
            this.hilfeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.hilfeToolStripButton.Text = "Hi&lfe";
            this.hilfeToolStripButton.Click += new System.EventHandler(this.hilfeToolStripButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // mandantToolStripButton
            // 
            this.mandantToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.mandantToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("mandantToolStripButton.Image")));
            this.mandantToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.mandantToolStripButton.Name = "mandantToolStripButton";
            this.mandantToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.mandantToolStripButton.Text = "toolStripButton1";
            this.mandantToolStripButton.ToolTipText = "Öffnet den Dialog zum Bearbeiten von Mandanten";
            this.mandantToolStripButton.Click += new System.EventHandler(this.mandantToolStripButton_Click);
            // 
            // boToolStripButton
            // 
            this.boToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.boToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("boToolStripButton.Image")));
            this.boToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boToolStripButton.Name = "boToolStripButton";
            this.boToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.boToolStripButton.Text = "toolStripButton1";
            this.boToolStripButton.ToolTipText = "Öffnet den Dialog zum Bearbeiten betrieblicher Objekte";
            this.boToolStripButton.Click += new System.EventHandler(this.boToolStripButton_Click);
            // 
            // awsToolStripButton
            // 
            this.awsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.awsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("awsToolStripButton.Image")));
            this.awsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.awsToolStripButton.Name = "awsToolStripButton";
            this.awsToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.awsToolStripButton.Text = "toolStripButton1";
            this.awsToolStripButton.ToolTipText = "Öffnet den Dialog zum Bearbeiten der Aufgabenträger";
            this.awsToolStripButton.Click += new System.EventHandler(this.awsToolStripButton_Click);
            // 
            // pATtoolStripButton
            // 
            this.pATtoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pATtoolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("pATtoolStripButton.Image")));
            this.pATtoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pATtoolStripButton.Name = "pATtoolStripButton";
            this.pATtoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pATtoolStripButton.Text = "toolStripButton1";
            this.pATtoolStripButton.ToolTipText = "Öffnet den Dialog zum Bearbeiten der Aufgabenträger";
            this.pATtoolStripButton.Click += new System.EventHandler(this.pATtoolStripButton_Click);
            // 
            // aufgabenobjekteToolStripButton
            // 
            this.aufgabenobjekteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.aufgabenobjekteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("aufgabenobjekteToolStripButton.Image")));
            this.aufgabenobjekteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.aufgabenobjekteToolStripButton.Name = "aufgabenobjekteToolStripButton";
            this.aufgabenobjekteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.aufgabenobjekteToolStripButton.Text = "toolStripButton1";
            this.aufgabenobjekteToolStripButton.ToolTipText = "Öffnet den Dialog zum Bearbeiten der Aufgabenobjekte";
            this.aufgabenobjekteToolStripButton.Click += new System.EventHandler(this.aufgabenobjekteToolStripButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1565, 868);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.windowsMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.windowsMenu;
            this.Name = "Main";
            this.Text = "Main";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.windowsMenu.ResumeLayout(false);
            this.windowsMenu.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.MenuStrip windowsMenu;
        private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton boToolStripButton;
        private System.Windows.Forms.ToolStripButton awsToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem initialisiereDatenbankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löscheDatenbankToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripButton mandantToolStripButton;
        private System.Windows.Forms.ToolStripButton pATtoolStripButton;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem neuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernunterToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem druckenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seitenansichtToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem beendenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem rückgängigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wiederholenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem ausschneidenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem einfügenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem alleauswählenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extrasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem anpassenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hilfeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inhaltToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suchenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton neuToolStripButton;
        private System.Windows.Forms.ToolStripButton öffnenToolStripButton;
        private System.Windows.Forms.ToolStripButton speichernToolStripButton;
        private System.Windows.Forms.ToolStripButton druckenToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton ausschneidenToolStripButton;
        private System.Windows.Forms.ToolStripButton kopierenToolStripButton;
        private System.Windows.Forms.ToolStripButton einfügenToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton hilfeToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton aufgabenobjekteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    }
}