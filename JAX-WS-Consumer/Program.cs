﻿using JAX_WS_Consumer.CounterServiceReference;
using JAX_WS_Consumer.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace JAX_WS_Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            // Aufruf des über WildFly zur Verfügung gestellten WebServices

            // (1) Füge die Service Referenz hinzu; Visual Studio generiert aus der WSDL die erforderlichen Stubs
            // (2) Erstelle ein Objekt mit dem Aufruf des Client-Stubs
            CounterServiceClient proxy = new CounterServiceClient();
            try
            {
                // (3) Rufe den WS auf
                Console.WriteLine(proxy.getHits());
            }
            catch (EndpointNotFoundException enfe)
            {
                Console.WriteLine("Der Server ist nicht erreichbar ({0}).", enfe.ToString());
            }

            // Aufruf des über IIS zur Verfügung gestellten WebServices
            Service1Client svc = null;
            bool success = false;
            try
            {
                svc = new Service1Client();

                var request = new CompositeType
                {
                    BoolValue = true,
                    StringValue = "line"
                };
                var result = svc.GetDataUsingDataContract(request);

                Console.WriteLine("The result is:");
                Console.WriteLine(
                    "BoolValue={0}, StringValue={1}",
                    result.BoolValue,
                    result.StringValue);
                Console.Write("ENTER to continue:");
            }
            finally
            {
                if (!success && svc != null)
                {
                    svc.Abort();
                }
            }


            Console.ReadLine();
        }
    }
}
